\section{Accepted and rejected programs}
\label{sec:program_classes}

In order to express a program with ISAv2 or ISAv3, \gls{CFG} extraction has to be performed at compilation (or conversion) time. In our new \glspl{ISA}, the new program will reflect the quality of the prior \gls{CFG} extraction.
But any subsequent \gls{CFG} extraction becomes trivial (\emph{cf.} algorithms~\ref{alg:structural} and \ref{alg:tracking} in the appendix) and equivalent to the prior one: the structure of our new program reflects it.
Additionally, if the prior \gls{CFG} extraction is not precise, the new program becomes extremely inefficient (both in size and execution time) due to the presence of big dispatchers.
For all these elements, we can say that a program without indirect jumps structurally mirrors its \acrlong{CFG}.

But generally, the possibility of expressing a program in our new \glspl{ISA} is equivalent to the possibility of extracting a complete \gls{CFG} at compilation time (no invalid edge is ever taken at runtime).

\paragraph{Accepted programs}

As seen in \autoref{sec:cfg}, there are programs that cannot be expressed efficiently in our ISAv2 or ISAv3.
Here is how to write the snippet shown in \autoref{lst:opaque} in an \gls{ISA} without indirect jumps.
If the program has size $n$ instructions, we can write the new version by first computing the destination address as in the initial version, then dispatching on this address. As shown in \autoref{tab:disp_count}, the new program will have at least size $3\cdot n$ instructions with ISAv2 and ISAv3 ($n$ for the initial version, $2\cdot n$ to be able to branch to all initial instructions).
We still do not know the secret value that provokes a different branching destination or what that new destination is. But in this new version, the structure of the program makes explicit all possible branches.

All programs can therefore be converted to the new \glspl{ISA}, if and only if their size is finite and the memory layout is known (we consider only jumps to valid instruction addresses). But not all conversions are efficient.
Equivalently for a new program, it can be expressed in the new \glspl{ISA} if all its instructions are known at compile time.

In particular, virtual method tables can be written with ISAv2 and ISAv3. These tables are used in Object-Oriented languages to deal with dynamic dispatch: depending on the caller’s type, a different function (at a dedicated address) is called. Virtual method tables can be replaced with dispatchers since all destinations are known at compile time.

\paragraph{Rejected programs}

They are programming patterns where extracting a complete \gls{CFG} is impossible at compile time. An example would be an application able to dynamically load plugins for additional functionality.
When the application is compiled, the plugin instructions are not known. If we forbid indirect jumps, the application cannot divert the control flow to the plugin.
Similarly, the kernel cannot launch a new application not known when we compile it. We must recompile our kernel for each new application added.
In fact, the question is how to execute a program after compilation, since the new program was not known before.

From a security standpoint, these are dangerous patterns. For example, executing a plugin program is equivalent to execute unknown instructions from the application context.

\paragraph{Indirect jumps are needed}

Launching a new unknown application from the kernel seems to be more acceptable than launching a plugin from an application. This is because in the former case, we change the execution context: the application has fewer privileges than the kernel.

Finally, these examples give the solution to our problem. Indirect jumps are absolutely required to get non-trivial functionalities.
But an indirect jump is also equivalent to switching to a new security domain. As a consequence in a secure \gls{ISA}, if we want to remove indirect jumps as in our new \glspl{ISA}, we must also introduce hardware security domains. What is instruction and what is data is tied to the security domain, so that switching domain allows transferring data into instruction memory.
With this new feature, an indirect jump can be authorized when switching to a new context. The new jump would not be directly to a particular instruction, but to the new security domain entry point, able to verify the validity of the new control flow. 

With this mechanism, an application can launch a plugin, but in a new security domain. Compilation is possible, but launching a new application is done in a new security domain.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
