\section{Semantics}
\label{sec:semantics}

The key point that forbid easy and precise \gls{CFG} extraction is the presence of indirect jumps whose destinations are not statically predictable.
Indirect jumps are mostly present in two forms: forward edges (\verb|jump x0|) where we jump to the address present in a register, and backward edges (\verb|return|) where we jump to the address following the last procedure call.

A radical solution to our problem is therefore to forbid indirect jumps (both forward and backward).
Yet it is possible to handle indirect backward jumps in a statically predictable way, allowing as a consequence a more versatile \gls{ISA}.

In this section, we will describe and test $3$ \glspl{ISA}:
\begin{itemize}
\item ISAv1 allows indirect jumps.
\item ISAv2: similar to ISAv1 without indirect jumps.
\item ISAv3: building on ISAv2, a restricted indirect jump is authorized in the form of a call/return semantic with a dedicated return stack, also called ``call stack''.
\end{itemize}

% In the following listing we express our \glspl{ISA} using Rust syntax.
% Our implementations are done in this programming language since it supports sum types, that nicely fit an \gls{ISA} description.
The source code for our implementation and our results can be found on the git repository at \url{https://gitlab.com/Artefaritaj/simple_risc}.

Here is a short description of the instructions:
\begin{itemize}
\item Integer arithmetic instructions: perform an arithmetic operation on two
  operands (in registers) and place the result in a
  register. Operations are:
  $+, -, \oplus, \wedge, \vee, \cdot, /, \mod, <<, >>$.
\item Conditional branches: evaluate the condition among
  $=, \neq, \leq, <, \geq, >$ between two registers. If the condition
  is valid, jump to the specified address if not follow the usual
  control flow.
\item Direct jump: goto the specified address for the next instruction.
\item Call: direct jump to specified address and store the value of \gls{PC} in a register (x14 for ISAv1) or a dedicated stack (ISAv3).
\item Return: jump to the address popped from the dedicated return stack (ISAv3 only).
\item Indirect jump: go to the address present in the given register (ISAv1 only).
\item Direct load/direct store: load or store a data in memory to/from a
  register.
\item Indirect load/indirect store: load or store a data in memory at
  address in a register to/from another register.
\item Load immediate: set the value of the given register (source of
  constants).
\item Register move: copy a register into another.
\item Non-deterministic: load a non-deterministic value into a
  register. The semantic does not specify the source: it can be either
  user input, result of a true random number generator, etc.
\item Halt: terminate the computation.
\end{itemize}

The \verb|push| and \verb|pop| instructions are also used and are pseudo-instructions developed as several machine instructions to do stack operations.

In this paper, we will change the \gls{ISA} semantic by adding or
removing instructions in order to enforce security properties at the
\gls{ISA} level.


\subsection{Benchmarks}

After defining our three \glspl{ISA}, we want to verify that our new proposals achieve our goal of easy and precise static \gls{CFG} extraction. At the same time, we measure the performance differences for two benchmarks:
% We run two benchmarks for the $3$ \glspl{ISA}:
\begin{itemize}
\item An \gls{AES} encryption is a simple program with an interesting instruction mix. The execution requires a lot of memory accesses and many arithmetic operations (\emph{cf.} \autoref{tab:inst_mix}).
\item A theoretical analysis of a dispatcher pattern. Dispatchers are the main argument to keep indirect jumps: we will analyze the penalty incurred by ISAv2 and ISAv3.
\end{itemize}

\subsubsection{\gls{AES}}

\paragraph{Implementation}
This benchmark is an \gls{AES} encryption (symmetric cryptography algorithm), encoded by hand in an assembly language.
The instruction mix for ISAv1, ISAv2 and ISAv3 can be seen on \autoref{tab:inst_mix}.

\begin{table*}[h]
  \centering
  \caption{Instruction mix for one AES run for each ISA. *A return instruction is an indirect jump.\label{tab:inst_mix}}
  \begin{tabular}{| l | c | c | c | c | c | c |}
    \hline & \multicolumn{2}{c|}{ISAv1} & \multicolumn{2}{c|}{ISAv2} & \multicolumn{2}{c|}{ISAv3}\\
    \hline Type & Count & Ratio & Count & Ratio & Count & Ratio\\
    \hline Arithmetic & 10011 & 0.44 & 9978 & 0.41 & 9825 & 0.44\\
    LoadImmediate & 5232 & 0.23 & 6767 & 0.27 & 5218 & 0.23\\
    IndirectLoad & 2650 & 0.12 & 2628 & 0.11 & 2554 & 0.11\\
    IndirectStore & 2464 & 0.11 & 2441 & 0.10 & 2368 & 0.11\\
    DirectJump & 668 & 0.03 & 719 & 0.03 & 668 & 0.03\\
    IndirectJump* & 616 & 0.03 & X & X & 616 & 0.03\\
    ConditionalBranch & 601 & 0.03 & 1703 & 0.07  & 601 & 0.03\\
    RegisterMove & 370 & 0.02 & 370 & 0.02 & 370 & 0.02\\
    NonDeterministic & 2 & 0.00 & 2 & 0.00 & 2 & 0.00\\
    Halt & 1 & 0.00 & 1 & 0.00 & 1 & 0.00\\
    \hline
    Total & 22615 & 1.00 & 24609 & 1.00 & 22223 & 1.00\\
    \hline
  \end{tabular}
  
\end{table*}

The (naive) implementation is done in our own assembly language corresponding to the $3$ \glspl{ISA}. This assembly language is also used for listings \ref{lst:xtimesv1}, \ref{lst:pattern1}, \ref{lst:pattern2}, \ref{lst:pattern3} and \ref{lst:subleq_vm}. Instructions map quite directly to their defining semantic.
The \makeatletter @ \makeatother symbol is used as an optional hint that we are using the value at the address present in the register. \# is used to denote literal values. Finally, ! is used to denote global symbols (by default, symbols are defined only in their file).

\begin{lstlisting}[caption=Xtimes assembly implementation (AES subfonction) with ISAv1.,label=lst:xtimesv1]
//x1 byte value to modify
!xtimes:
    // save context
    push    x2, x4

    // x1=x1*2
    load    x4, #0x1
    sla     x1, x1, x4

    // test modulo
    load    x4, #0x100
    and     x2, x1, x4
    beq     end, x2, zero

sub_modulo:
    load    x4, #0x11B
    xor     x1, x1, x4

end:
    // restore context
    pop     x4, x2
    //return
    jump    x14
\end{lstlisting}

\paragraph{Results}
For the three \glspl{ISA}, we  evaluate their performance and the size of the \gls{CFG} that can be extracted without annotation, shown on \autoref{tab:aes}.
For the ISAv2, all the \verb|return| instructions have been replaced by dispatchers (\emph{cf.} \autoref{sec:dispatcher}), another possibility would be to inline everything but this latter solution cannot be scaled to any program.

The performance is evaluated as the mean and the standard deviation for $10,000$ \gls{AES} executions.
The \gls{CFG} extraction is done with $2$ different algorithms:
\begin{itemize}
\item Structural: the successors to an instruction are all the ones that can be reached according to the instruction semantic. The \gls{CFG} is deduced from the structure of the program, not the data content. In particular, the successors to an indirect jump are all the instructions in the program. The extraction algorithm is described in \autoref{alg:structural}.
\item Tracking: it is possible to extract precisely the \gls{CFG} with the ISAv3 call/stack semantic. In addition to the structural information, we track the return stack and compute all edges for all stack states. In other words, if the same procedure is called from $2$ different locations, the \verb|return| instruction will jump to $2$ different addresses that can be determined if we track the whole return stack in our \gls{CFG} analysis. The extraction algorithm is described in \autoref{alg:tracking} and \autoref{alg:stack_hash}.
\end{itemize}

It is often possible to have a better \gls{CFG} extraction algorithm in the presence of indirect jumps, for example Kinder \textit{et al.} in \cite{kinder2009} propose a (complex) solution to the problem. Balakrishnan \textit{et al.}~\cite{balakrishnan_2010} propose quite a complete toolsuite for static analysis in general and \gls{CFG} extraction in particular.
Yet these solutions are, of course, non-general: they cannot deal with the snippet presented in \autoref{sec:cfg}. The lesson being that the presented snippet should be rejected in the context of secure execution.



Our \gls{CFG} extraction algorithms are simpler than the one proposed by Kinder \textit{et al.}: only a simple control flow analysis is performed. A data-flow analysis may improve a bit the accuracy of the extracted \gls{CFG} if some branch conditions are always true or always false. But such a case would represent a failure of the compiler: it should have detected this invariant and simplified the program accordingly.

\input{tex/algorithms.tex}

On \autoref{tab:aes}, we show the implementations performances and the \gls{CFG} nodes and edges coverage.
The \gls{CFG} nodes (respectively edges) coverage is the ratio of nodes (resp. edges) in the \gls{CFG} that are touched during one execution. E.g. an edges coverage of $6.4\%$ means that only $6.4\%$ of the edges have effectively been followed during one execution: the \gls{CFG} is imprecise in this case. In our case, it means that numerous instruction transitions are considered valid where they should not.

\begin{table*}[h]
  \caption{Evaluating \glspl{ISA} performances and \gls{CFG} coverage for an \gls{AES} implementation}
  \label{tab:aes}
  \begin{tabular}{l | c || c | c || c | c | c | c |}
    & CFG extraction & Mean duration & Std deviation & CFG nodes count & nodes coverage & CFG edges & edges coverage \\
    \hline ISAv1 & structural & 22635.0 & 24.1 & 778 & $99.6\%$ & \textcolor{red}{12437} & \textcolor{red}{$6.4\%$} \\
    ISAv2 & structural & 24605.2 & 24.1 & 816 & $99.1\%$ & 836 & $99.0\%$ \\
    ISAv3 & structural & 22230.7 & 23.8 & 747 & $99.7\%$ & \textcolor{red}{11942} & \textcolor{red}{$6.4\%$} \\
    ISAv3 & tracking & 22230.8 & 24.0 & 745 & $100\%$ & 764 & $100\%$ \\
  \end{tabular}
\end{table*}

On the performance side, we observe that ISAv2 is slower. Indeed, for all returns from procedures we have a small dispatcher to jump back to the correct location.
ISAv1 is a bit slower than ISAv3 since the stack handling (push and pop) is explicit instead of implicit in ISAv3.

For \gls{CFG} extraction, we note that the structural extraction is imprecise for ISAv1 and ISAv3. In the latter case, the problem comes from the \verb|return| instructions: the algorithm cannot infer where they jump and assume that all instructions are potential successors. In these two cases, we can say that static \gls{CFG} extraction is impractical.
But for ISAv2 with structural extraction and ISAv3 with tracking extraction, the \gls{CFG} is extremely precise.
For ISAv2 the coverage is not perfect since, because of the semantic of the \gls{ISA}, unreachable \verb|halt| instructions must be added to deal with error cases.

This benchmark showcases the advantages for our ISAv2 and ISAv3 proposals: the \gls{CFG} is extractable with a $9\%$ loss of performance for ISAv2 or a $2\%$ gain of performances for ISAv3.

\subsubsection{Dispatcher}
\label{sec:dispatcher}

A dispatcher is an instruction pattern where a particular control flow is chosen depending on a data value. This pattern is used to call functions in a library, as a result of a system call or as a way to implement runtime polymorphism in object-oriented languages.
This pattern is particularly dangerous with respect to information security as we will see in \autoref{sec:limits}.
It often relies on indirect jumps for an efficient implementation, therefore we expect that our \gls{ISA} proposals will have varying dispatcher implementations.

\begin{lstlisting}[caption={ISAv1 dispatcher pattern},label=lst:pattern1]
  //x1 contains the address to branch to
  //(may be the result of pointer arithmetic)
  <@\textcolor{green}{call~~~x1}@>
dispatcher_end:
  //continue

  //...
procX:
  <@\textcolor{blue}{push~~~x14}@>  
  //...
  <@\textcolor{blue}{pop~~~~x14}@>
  <@\textcolor{blue}{jump~~~x14}@>
\end{lstlisting}

\begin{lstlisting}[caption={ISAv2 dispatcher pattern},label=lst:pattern2]
  //x1 contains the value that decides where to branch
  <@\textcolor{red}{load~~~x15, \#0}@>
  <@\textcolor{red}{beq~~~~proc1, x1, x15}@>
  <@\textcolor{red}{jump~~~proc2}@>

dispatcher_end:
  //continue

  //...
procX:
  //...
  <@\textcolor{blue}{jump~~~dispatcher\_end}@>
\end{lstlisting}

\begin{lstlisting}[caption={ISAv3 dispatcher pattern},label=lst:pattern3]
  //x1 contains the value that decides where to branch
  <@\textcolor{red}{load~~~x15, \#0}@>
  <@\textcolor{red}{beq~~~~call\_proc1, x1, x15}@>
  <@\textcolor{red}{jump~~~call\_proc2}@>
call_proc2:
  <@\textcolor{green}{call~~~proc2}@>
  <@\textcolor{green}{jump~~~dispatcher\_end}@>
call_proc1:
  <@\textcolor{green}{call~~~proc1}@>
  <@\textcolor{green}{jump~~~dispatcher\_end}@>
  //...
dispatcher_end:
  //continue

  //...
procX:  
  //...
  <@\textcolor{blue}{return}@>
\end{lstlisting}


For the 3 proposed patterns, we evaluate the number of instructions necessary for the dispatcher logic as a function of the number $p$ of procedures that must be reachable by the dispatcher, shown on \autoref{tab:disp_count}.

\begin{table*}
  \caption{Dispatcher instruction count (for $p > 3$)}
  \label{tab:disp_count}
  \begin{tabular}{l || c | c | c || c |}
    & Branch logic (red) & Call logic (green) & Return logic (blue) & Total \\
    \hline ISAv1 & $0$ & $1$ & $3 \cdot p$ & $3\cdot p + 1$ \\
    ISAv2 & $2\cdot p$ & $0$ & $p$ & $3\cdot p$ \\
    ISAv3 & $2\cdot p$ & $2\cdot p$ & $p$ & $5 \cdot p$ \\
  \end{tabular}
\end{table*}

\begin{table*}
  \centering
  \caption{Dispatcher latency}
  \label{tab:disp_latency}
  \begin{tabular}[h]{l || c | c | c || c |}
    & Branch latency (red) & Call latency (green) & Return latency (blue) & Total \\
    \hline ISAv1 & $0$ & $1$ & $3$ & $4$ \\
    ISAv2 & $2\cdot \ceil{\log_2(p)} + 1$ & $0$ & $1$ & $2 + 2 \cdot \ceil{\log_2(p)}$ \\
    ISAv3 & $2\cdot \ceil{\log_2(p)} + 1$ & $2$ & $1$ & $4 + 2 \cdot \ceil{\log_2(p)}$ \\
  \end{tabular}
\end{table*}

The branch latency, a measure of the time taken to call a particular procedure, is shown in \autoref{tab:disp_latency}. The value for ISAv2 and ISAv3 can be proven by recurrence. Indeed, for a given $p$ the branching can be done by using a branch (2 instructions) and the 2 patterns for $\ceil{\frac{p}{2}}$ and $\floor{\frac{p}{2}}$.
As a consequence $Latency(p) = 2 + Latency\left(\ceil{\frac{p}{2}} \right)$.
If we admit that $\forall l < p$, the relation $Latency(l) = 2\cdot \ceil{\log_2(l)} + 1$ holds (manually verified for the firsts $l$), then
\begin{eqnarray*}
  Latency(p) &=& 2 + 1 + 2\cdot \ceil{\log_2(\ceil{\frac{p}{2}})},  \\
  Latency(p) &=& 1 + 2\cdot \left(1 + \ceil{\log_2(p) - 1} \right), \\
  Latency(p) &=& 1 + 2\cdot \ceil{\log_2(p)}\quad . \\
\end{eqnarray*}

In the case where all procedures called by the dispatcher return to the same location, then ISAv2 is actually slightly better than ISAv3 for the two metrics (instruction count and latency).
Yet the true reason why indirect jumps are preeminent is because of the low dispatching latency, \textbf{independent from the number of \mbox{procedures} to call}.
Moreover, the high number of branches for ISAv2 and ISAv3 does not play well with speculative execution: dispatchers are highly penalized by the deletion of indirect jumps.
Indirect jumps allow a high fanout control flow: an instruction can have a lot of possible successors. Whereas with ISAv2 and ISAv3, an instruction has $2$ successors at most.

An additional problem with the \mbox{dispatchers} with ISAv2 and ISAv3 is that the latency of a procedure call depends on what procedure is called.
Depending on the application, this timing side-channel can be a vulnerability.

Therefore, the usage (or not) of indirect jumps is a trade-off between the performance of a high fanout control flow and the possibility (or ease) of knowing the \gls{CFG} statically.


\subsection{Branching}

In order to meet a better trade-off between performances and \gls{CFG} discoverability, a new instruction may be added: the \textit{n-fanout conditional branch}.

\begin{lstlisting}[caption={n-fanout conditional branch illustration},label=lst:nfanout_example]
  nbranch x1, #3
  jump    proc1
  jump    proc2
  jump    proc3
  jump    error_handling
\end{lstlisting}


The deciding register is expected to hold an integer value $i$ in $[\![0,n-1]\!]$ ($n$ being the constant litteral present in the instruction).
An implicit jump is performed to the address: $AddressOf(branch) + 1 + i$. If the register holds an illegal value, we jump to the error handling address at $AddressOf(branch) + 1 + n$.
A usage example can be found on \autoref{lst:nfanout_example}.

With this new instruction, the latency is now $O\left(\log_n(p)\right)$, where $n$ can be chosen equal to $p$ to minimize latency.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
