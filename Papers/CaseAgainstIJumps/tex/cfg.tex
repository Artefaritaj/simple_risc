\section{The machine, control flow graph and control flow integrity}
\label{sec:defs}

\subsection{Machine}
\label{sec:machine}

Our analyzes are performed on an abstract machine representative of
the real chips powering most computation today. The elementary
instructions that our machine is able to execute are inspired (and
simplified) from the RISC-V \gls{ISA}.
Apart from the common architecture described in this section, we will define $3$ different \glspl{ISA} in \autoref{sec:semantics} for this same machine.

Our machine is a 64-bit RISC processor: addresses, data memory words and registers are 64-bit integers.
% Our machine is defined by $3$ types: A is the address type, M is the
% memory value type and R is the register value type.  It is required
% that A and M can be converted from and to R.

The machine has a mutable state composed of registers and data memory.
The registers are:
\begin{itemize}
\item generic (read/write) registers $x1, \cdots, x16$.
\item \Gls{SP} (read/write) register for the data stack.
\item Constant zero and full (read-only) registers. Full is defined
  such that $x \oplus full = \neg x$ (full is the value with all its bits to $1$).
\item A special register (neither readable nor writable directly), the
  \gls{PC}, that contains the address of the next instruction.
\end{itemize}

The data memory is finite, as in a real machine, and is word-addressed.
Since the memory is finite, the machine is not Turing-complete. But as in real machines, it has no practical implications.

All registers and memory are initialized with a default zero value.
A program is a dictionary of instructions:
the address (\mbox{64-bit} unsigned integer) is used to uniquely select one instruction
in the dictionary.  As such, our architecture is a Harvard architecture,
instructions and data are separated.

The execution of our machine works as follows:
\begin{itemize}
\item First provide a program that is put in a dedicated (read-only) memory.
\item Set the \gls{PC} to the program entry address.
\item Then in an infinite loop:
\begin{itemize}
  \item Fetch the next instruction at the address in \gls{PC}
  \item Execute it by changing the state according to the instruction
    semantic. Or terminate the machine on an \verb|halt| instruction.
  \item If the \gls{PC} has not been changed by the instruction,
    increment the address.
  \end{itemize}
\end{itemize}

We will explore $3$ \glspl{ISA} presenting slight variations on how to handle the control flow in \autoref{sec:semantics}.

\subsection{Control flow graph}
\label{sec:cfg}

In our machine definition, we have seen that instructions are stored linearly in memory, as a dictionary where the addresses are the entry keys.
Yet at runtime, the control flow is not linear. We want to be able to go forward or back.
As a result, in our machine, the control flow is represented as a graph (potentially with cycles): the \acrfull{CFG}.
In it, nodes are instructions and edges are the legal transitions between instructions.
For example, a conditional branch has two possible successors: the node has two outgoing edges in the \gls{CFG}.

All invalid program addresses, in our machine, are considered aliases for the program entry address (simplification hypothesis for our analysis).

A \gls{CFG} is always defined for any \gls{ISA} since the complete digraph (\textit{i.e.} where all nodes are connected with all the others) is a valid \gls{CFG}: meaning that all legal control flows follow connected paths in the graph.
Therefore, any valid \gls{CFG} is an overapproximation.
% A branch condition may never be possible (\textit{e.g.} zero $=$ full) but the \gls{CFG}, \textit{in our definition}, still takes into account the two instruction successors.
The best \gls{CFG} is the smallest that is still valid for all possible program executions.

\Gls{CFG} extraction is useful on several counts. If \gls{CFG} extraction is easy and precise, it allows to obtain more efficient static analyzis tools (\textit{e.g.} dead code elimination can be performed safely and precisely).
Additionally, it allows powerful \gls{CFI} schemes such as \acrfull{ISR}~\cite{ruan_de_clercq_sofia:_2016}.


On another hand, it renders obfuscation harder (but possible as seen in \autoref{sec:limits}): it would be more difficult for malware packers to hide their payload.

The question that naturally arises is: \textbf{can the best \gls{CFG} be \mbox{extracted} from the instructions in general?}

Beyond the fact that the difficulty of precise \gls{CFG} extraction is related to the reachability problem in programs (undecidable generally), we can simply prove that it is possible to hide the destination of an indirect jumps by using cryptographic hash functions.

\paragraph{Trapdoor predicates}

\begin{definition}{\textbf{Trapdoor predicate}}
  Let $p: \mathbb{F}_2^n \rightarrow \mathbb{F}_2$ be a function working on bit vectors of size $n$ (for a large enough $n$). We call $p$ a trapdoor predicate if $\exists k \in \mathbb{F}_2^n$ such that:
  \begin{itemize}
  \item $\forall x \neq k \in \mathbb{F}_2^n$, $p(x) = 0$.
  \item $p(k) = 1$, where $k$ is a secret value. Meaning that knowing $p$, there is no better way to find $k$ than brute-forcing every possible input without additional information (the adversary has negligible advantage otherwise). 
  \end{itemize}
\end{definition}

Trapdoor predicates can be built from a cryptographic hash function (other constructions are possible, for example by exploiting the SAT problem). Let $h: \mathbb{F}_2^n \rightarrow \mathbb{F}_2^m$ be a cryptographic hash function.

Then we can define $p$ for a given secret value $k \in \mathbb{F}_2^n$ as:
\begin{equation*}
  p(x) = \left\{
      \begin{array}{ll}
        1 & \mbox{ if } h(x) = h(k) \\
        0 & \mbox{ in the other cases}
      \end{array}
      \right.
\end{equation*}

By construction, $p(k) = 1$ but $k$ cannot be deduced from the knowledge of $p$ (of course $h(k)$ is precomputed).

% We want to stress out the ``with trapdoor part'': opaque predicates (without trapdoor) are often used as a mean to prevent reverse engineering.
% But without the trapdoor, they are constant functions and are therefore quite easy to detect and remove~\cite{opaque_useful}.


\paragraph{How to use these predicates to hide the control flow}

Trapdoor predicates allow hiding the control flow in any given program as shown in \autoref{lst:opaque}.

\begin{lstlisting}[label=lst:opaque,caption=Hiding the control flow with a trapdoor predicate.]
  x     <- user input
  delta <- p(x)*(h(x + 1) xor constant)
  jump 0x1000 xor delta
\end{lstlisting}

In \autoref{lst:opaque}, for most input values $p(x)=0$, the program jump to the $0x1000$ value.
But if the attacker inputs the secret value $k$, the program jump to another secret address (hidden, equal to $0x1000 \oplus h(k + 1) \oplus constant$ where constant is chosen according to the desired destination).

In this program, without the knowledge of $k$, it is impossible (without brute-forcing) to
\begin{itemize}
\item find $x$ such that we jump to an address different than $0x1000$,
\item find the destination address when we jump to somewhere different than $0x1000$. We use $h(x + 1)$ since $h(k)$ may be reverse engineered from $p(x)$ program.
\end{itemize}

In conclusion,  because of the existence of trapdoor predicates, a precise \gls{CFG} is not extractable without additional information in general.
Specifically, the program is not enough: in \autoref{lst:opaque}, the \gls{CFG} extraction must assume that the jump instruction can reach the whole address space (where it can, in reality, jump to only two destinations).

Please note that we did not use opaque predicates for our demonstration since they are not theoretically sound as shown by Zobernig \textit{et al.}~\cite{opaque_useful}.

\subsection{Control flow integrity}
\label{sec:cfi}

A \gls{CFI} mechanism is responsible for ensuring that only a valid control flow is taken. For every instruction, it must verify that the transition to the next instruction is an edge in the \gls{CFG}.
\gls{CFI} has an extensive literature. A recent review has been published by Burow \textit{et al.}~\cite{burow_control-flow_2017}.
Most \gls{CFI} solutions try to verify that jumps can only reach legitimate addresses (forward edges). 
A special case is the \verb|return| instruction to return from a routine call (backward edges).

Abadi \textit{et al.}~\cite{abadi_control-flow_2009} show software \gls{CFI} implementations: they propose code snippets to replace dangerous instructions (indirect jumps) in order to guarantee \gls{CFI}.

Tice \textit{et al.}~\cite{tice_enforcing_nodate} demonstrate a software solution that leverages the compiler to automatically insert the appropriate protections at jump sites (forward edges only).
In particular, they tackle the problem caused by virtual method tables, necessary in some programming languages (\textit{e.g.} C++) to enforce runtime polymorphism: a dispatcher is necessary there.

Backward edges (\textit{e.g.} \verb|return| instruction) are traditionally protected with a shadow stack~\cite{frantzen_stackghost:_2001}: the call stack is duplicated.
On a \verb|return| instruction, the return address on both stacks are read then compared. If they differ, an alarm is triggered.
Another possibility explored by Davi \textit{et al.}~\cite{davi_hardware-assisted_2014,davi_hafix:_2015} is to add instructions to the ISA for the sole purpose of validating function calls and returns.
On any indirect function call, the processor switch to a particular state. The next instruction must be a special \verb|CFIBR label| instruction in order to continue execution.
The label is used to keep track of which functions are currently executing.

Another common solution proposed in the context of \gls{CFI} is the use of a shadow stack, a second stack that duplicates the return addresses of the main stack. Upon a function return, both return addresses from the two stacks are compared before the backward indirect jump.
This countermeasure is actually a duplication countermeasure, there is no added semantic in the program and there is no way to validate the return address against a truth value. As such it is only useful in some specific attack scenarios and is not a general solution for \gls{CFI}.

To resist stronger attacker models, hardware \gls{CFI} becomes necessary. SOFIA~\cite{ruan_de_clercq_sofia:_2016} proposes such a solution with an \gls{ISR} scheme: instructions are encrypted with a value representing the corresponding edge in the \gls{CFG}.
More precisely, instructions are encrypted at program startup using the previous and current \gls{PC} values as shown on \autoref{eq:sofia}.

\begin{equation}
  i' = E_k(PC_{prev} || PC || ...) \oplus i
  \label{eq:sofia}
\end{equation}

Effectively, the \gls{CFG} edge is encoded in the encrypted instruction and must be valid for decryption.
The difficulty arises when two predecessors (thus two edges) are possible for one instruction. Then a hack is proposed to overcome this difficulty.

Hiscock \textit{et al.}~\cite{hiscock} explore how to properly implement an \gls{ISR} scheme to ensure \gls{CFI} by discussing and proposing encryption strategies.

For all these propositions, the system must be able to check the control flow with respect to a predetermined \gls{CFG}.
Since trapdoor predicates do not allow this in general, in the next section we propose to modify the \gls{ISA} semantic to allow easy and precise \gls{CFG} extraction for all programs.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
