\section{Limits of control flow integrity}
\label{sec:limits}

The ability to precisely extract any \gls{CFG} limits the reach of virtualization as a defense mechanism: the virtual machine inner mechanisms cannot be hidden. But as demonstrated in this section, it is still possible to hide the control flow in the data domain.

\subsection{Virtual machines}

Guaranteeing \gls{CFI} is particularly important to mitigate \gls{ROP} attacks~\cite{Roemer:2012} where an attacker modifies the control flow from a valid program to obtain a malicious behavior.
The trouble is \gls{CFI} is not enough.
Indeed, the existence of emerging virtual machines, in particular \textit{weird machines}~\cite{weird_dullien}, limits the guarantees that \gls{CFI} can offer.
An illustration, valid with all our \glspl{ISA}, can be seen on \autoref{lst:subleq_vm}.

\begin{lstlisting}[caption={subleq virtual machine},label=lst:subleq_vm]
// data memory is filled with user-defined values

// initialization
// virtual program counter 
// VPC(x13) = 0
  load  x13, #0
 
//exec one subleq instruction
subleq:
//read operands from data memory
  move  x1, x13
  load  x15, #1
  add   x13, x13, x15
  move  x2, x13
  add   x13, x13, x15
  move  x3, x13
//increment for next instruction
//if no jump
  add   x13, x13, x15

//subleq execution  
  load  x4, @x1
  load  x5, @x2
  sub   x6, x5, x4
  store @x2, x6
  ble   ijump, x6, x0

//start next instruction
  jump  subleq  
  
//virtual indirect jump  
ijump:
  move  x13, x3
//start next instruction
  jump  subleq  
  
\end{lstlisting}

Subleq machines have been proven to be universal computers~\cite{urisc}.
Our subleq \gls{VM} has a simple control flow with only direct jumps (it can be written with any of the three \gls{ISA}).
Yet the instructions to the \gls{VM} are in fact in data memory and indirect jumps are performed in the data domain.
This program is an example where even if \gls{CFI} is guaranteed, we cannot enforce any security property as a result.
In other words, security properties such as \gls{CFI} are not automatically transposed in the virtual machine: if an attacker replace a word in data memory, she can hijack the \gls{VM} control flow.

A possible conclusion would be that since Harvard architectures can be converted to von Neumann architectures (and \mbox{reciprocally}) via \acrlongpl{VM},
then properly removing indirect jumps requires removing indirect (data) memory accesses.
Non-coincidentally, for safety-critical application the use of the dynamic heap allocation is forbidden (\textit{e.g.} the rule 20.4 of MISRA-C:2004)
since that generates lots of indirect memory accesses making the behavior of the program harder to statically predict.
But removing entirely indirect memory accesses is a lot more constraining.

To respect our design principle that the \gls{ISA} must intrinsically contain the semantic information about the control flow, pointers used for direct memory accesses must be augmented with metadata inside the \gls{ISA}.
The instructions below should replace indirect memory accesses to restrict their reach:

\begin{itemize}
\item \verb|BoundedIndirectLoad Rd, Rs, B1, B2|: load the value present in memory at address contained \verb|Rs| in register \verb|Rd| if the address is between the bounds \verb|B1| and \verb|B2| (being literal values).
\item \verb|BoundedIndirectWrite Rd, Rs, B1, B2|: same principle but store at address \verb|Rd| the value register \verb|Rs|.
\end{itemize}

This is a proposition present in CHERI~\cite{woodruff2014cheri}.
Unfortunately, these new instructions may mitigate the power of emerging \acrlongpl{VM} but do not allow to strictly enforce \gls{CFI}.

\subsection{Pointing our model simplifications}

The machines presented in sections~\ref{sec:defs} and \ref{sec:semantics} are simplified versions of actual machines.
In particular, real machines implement mechanisms where the control flow is not defined by instructions: interrupts and traps.
An interrupt is a change of control flow due to an external event: \textit{e.g.} when a new byte has arrived on the serial bus, an interrupt is raised that divert the control flow to a dedicated subroutine, \textit{whatever the previous instruction was}. A trap is a special interrupt caused by an instruction: the \verb|trap| instruction divert the control flow to an implementation-defined location.
Traps are notably used to transfer control from the user land to the kernel.

These mechanisms are not captured by our models but neither are they by most \gls{CFI} mechanisms presented in \autoref{sec:cfi}.
Modeling the security of these control transfers is a major hurdle that has yet to be achieved. For example, how can we ensure \gls{CFI} in the presence of the \textit{page-fault weird machine}~\cite{bangert2013page}?

Another limitation is our use of a Harvard architecture.
Even if it is well known that the distinction between von Neumann and Harvard architectures has little relevance theoretically, we have no ways to modify the instructions in our machine after the initial program write.
As such it is not possible to install new programs after the initial programming. Quite limiting for a general-purpose computer.
The reason being that any instruction modification has the potential to break \gls{CFI} and is harder to model.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
