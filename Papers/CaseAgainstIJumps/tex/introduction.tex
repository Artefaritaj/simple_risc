\section{Introduction}

In the wake of the publication of numerous microarchitectural attacks such as Spectre~\cite{kocher_spectre_nodate}, Meltdown~\cite{lipp_meltdown:_nodate} and their variants, the need for the secure execution of programs is blatant.
Secure execution in this context refers to the additional information security-related properties that must be guaranteed by the execution model.

One such property is the \gls{CFI}.
Informally, this property should guarantee that an attacker is not able to redirect the control flow, \textit{i.e.} the order of execution of the instructions in the program.
But as we will see in \autoref{sec:limits}, this property is actually quite difficult to properly enforce.
Moreover, guaranteeing \gls{CFI} requires the defense mechanism to obtain the valid control flow in the form of the \gls{CFG}.
The mechanism is then responsible for comparing the actual control flow with the valid paths in the \gls{CFG}.
Unfortunately, in the real computers that we use every day, extracting the \gls{CFG} from a binary is a problem impossible to do precisely in general as shown in \autoref{sec:cfg}.
It leaves few possibilities for the defender:
\begin{enumerate}
    \item \label{choice:escaping} Provides an escaping mechanism to the \gls{CFI} in order to deal with the holes in our knowledge (but therefore creating a weakness).
    \item \label{choice:enrich} Enrich the binary with the necessary metadata to complete the \gls{CFG}. In some cases, it requires annotation from the developer in the source language.
    \item \label{choice:restrict} Restrict the valid programs to the ones where the \gls{CFG} is easily extractable.
\end{enumerate}

If we are interested in providing \gls{CFI} at the hardware level, we must be able to run securely all the programs fed to the chip: solution ~\ref{choice:escaping} is not acceptable.
To comply with the solution~\ref{choice:enrich}, the metadata must either be filled manually by the developer or derived automatically by the compiler when possible.
A consequence is that to be executed on a secure processor enforcing \gls{CFI}, the \mbox{sequence} of instructions is not self-sufficient anymore.
In addition to the instructions, the processor needs the metadata defining a complete \gls{CFG}.
Some solutions such as HAFIX~\cite{davi_hafix:_2015} or CHERI~\cite{woodruff2014cheri} propose to enrich the \gls{ISA} semantic with new instructions, embedding the metadata in the instructions themselves. 

Instead, in this paper, we argue for the third choice: the impossibility to extract efficiently the \gls{CFG} is due to the presence of indirect jumps in the \gls{ISA}.
If we remove the indirect jumps, the binary now contains all the information to extract the \gls{CFG} without requiring metadata.

The problem caused by indirect jumps is well known, and numerous works try to circumvent it (see \cite{theiling,kinder2009} for \gls{CFG} extraction in the presence of indirect jumps).
Yet removing indirect jumps has multiple benefits: first it rejects a class of program (the ones that cannot be converted to direct jumps only) that is insecure by nature.
Then the \gls{CFG} extraction becomes trivial, the binary program holds straightforwardly the necessary information for the \gls{CFG} reconstruction.

A non-goal is to convert automatically programs written in an \gls{ISA} with indirect jumps to our new \gls{ISA} proposal without them.
\mbox{Indeed}, as we show in \autoref{sec:cfg} and \autoref{sec:program_classes}, such a \mbox{conversion} is not possible in general: there are some programs that cannot be converted efficiently to an \gls{ISA} without indirect jumps.
But this impossibility result is actually a feature. Our new \gls{ISA} rejects programs which contain jumps with an impredictable destination: unsecure programs if we want to enforce \gls{CFI}.



\paragraph{Motivation}

In this work, we want to provide an \gls{ISA} allowing easy and precise \gls{CFG} extraction for all valid programs.
As proven in \autoref{sec:cfg}, this is not possible if the \gls{ISA} has indirect jumps.
This property would allow to easily enforce \gls{CFI} but also to forbid some forms of obfuscation: a malware packer would have a hard time to hide its payload.
Additionally, it would improve the reach and the power of static analysis tools.


\paragraph{Contribution}

To allow easy and precise \gls{CFG} extraction, we \mbox{propose} to remove indirect jumps in the \gls{ISA} with two new \glspl{ISA} \mbox{proposals}.

In this paper, we start by defining our abstract machine in \autoref{sec:defs}, followed by the definitions of \gls{CFG} and \gls{CFI}.
In particular we show, in \autoref{sec:cfg}, that a particular program with an indirect jump can hide the jump destination.

The $3$ \glspl{ISA} with varying semantics to express the control flow are described in \autoref{sec:semantics}.
We compare the resulting \glspl{ISA} with a simple benchmark to evaluate the performances and the ability to extract a precise \gls{CFG} with basic \gls{CFG} extraction algorithms. 
Our \glspl{ISA} are very simple ones, we do not compare them to existing ones; instead we evaluate the impact of indirect jumps removal in this simple setup.

A theoretical comparison is also performed on a dispatcher program since it is the main pattern requiring an indirect jump for efficient computations.
We propose a new \gls{ISA} modification to \mbox{mitigate} the latency penalty.

We critically analyze the reach of our proposals in \autoref{sec:limits}.
We demonstrate in particular that the ability of extracting a \gls{CFG} is not transferable in a \acrlong{VM}.
There is a parallel between indirect jumps and indirect memory operations that would require to remove the latter operations in order to precisely and easily extract the \gls{CFG} in any \gls{VM}.
Alas, this would seriously limit the machine computational capability.

We discuss the classes of accepted or rejected programs in \autoref{sec:program_classes} to point the limitations of totally removing indirect jumps.

On these mixed results, we draw a conclusion in \autoref{sec:conclusion}

%Kinder et al

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
