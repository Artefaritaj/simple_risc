\section{Semantics}

\begin{frame}[fragile]
  \frametitle{The problem: CFG extraction}

  \begin{center}
  \textbf{A precise CFG cannot be extracted in general.}  
  \end{center}

  \begin{block}{Trapdoor predicate}
    Let $k$ be a secret key and $h$ a cryptographic hash function.

    Then we define the trapdoor predicate $p$ as:

    \begin{equation*}
  p(x) = \left\{
      \begin{array}{ll}
        1 & \mbox{ if } h(x) = h(k) \\
        0 & \mbox{ in the other cases}
      \end{array}
      \right.
    \end{equation*}
  \end{block}

  \begin{block}{Hiding the control flow}
    \vspace{-0.6cm}
    \begin{eqnarray*}
      x & \leftarrow & \text{user input}\\
      y & \leftarrow & p(x)\cdot \left( h(x+1) \oplus constant \right)\\
      jump && 0x1000 \oplus y
    \end{eqnarray*}
  \end{block}
  
\end{frame}

\begin{frame}
  \frametitle{Hiding the control flow}

  \begin{block}{Hiding snippet}
    \vspace{-0.6cm}
    \begin{eqnarray*}
      x & \leftarrow & \text{user input}\\
      y & \leftarrow & p(x)\cdot \left( h(x+1) \oplus constant \right)\\
      jump && 0x1000 \oplus y
    \end{eqnarray*}
  \end{block}

  \begin{block}{Properties}
    Without the knowledge of $k$, it is impossible to:
    \begin{itemize}
    \item find $x$ such that we jump to an address different than $0x1000$.
    \item find the destination address, if we jump to somewhere different than $0x1000$.
    \end{itemize}
  \end{block}
  
\end{frame}

\begin{frame}
  \frametitle{Removing indirect jumps}

  \begin{block}{Indirect jumps considered evil ?}
    Instructions with potentially high fanout.
    Convert data uncertainty into control flow uncertainty.
    % If the destination value is unpredictable, all instructions are possible destinations.
    % They make dataflow analysis a requirement for precise CFG extraction.
  \end{block}

  \begin{block}<2->{}
    \begin{center}
      \textbf{Why not just forbid them ?}
    \end{center}
  \end{block}

  \begin{alertblock}<3->{Common answers}
    \only<3>{
      \begin{itemize}
      \item Some programs become impossible to write ! (Virtual method tables for OO polymorphism\ldots)
      \item Programs become inefficient !
      \end{itemize}
    }
    \only<4>{
      \begin{itemize}
      \item Some program patterns become impossible to write $\rightarrow$ the unsecure ones !
      \item Dispatchers may be inefficient.
      \end{itemize}
    }
  \end{alertblock}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{The machine}

  \begin{block}{A same machine for $3$ ISAs variations}
    A 64-bit RISC Harvard machine (D and I separated):
    \begin{itemize}
    \item Generict registers (RW): \verb|x1|,$\cdots$, \verb|x16|.
    \item Stack pointer (RW): \verb|sp| for the data stack.
    \item Constant registers (R): \verb|zero| and \verb|full| ($x \oplus full = \neg x$).
    \item Program counter is a special register neither (directly) readable nor writable.
    \item $2$ finite address spaces (for instructions and data), word addressed.
    \end{itemize}
  \end{block}

  \begin{block}{Setup}
    The program is written in a read-only instruction memory before machine boot.
  \end{block}
  
\end{frame}

\begin{frame}
  \frametitle{Our ISAs}

    \begin{itemize}
    \item \textcolor{green}{ISAv1}: base ISA with indirect jumps.
    \item \textcolor{red}{ISAv2}: no indirect jumps at all.
    \item \textcolor{blue}{ISAv3}: backward indirect jumps allowed.
    \end{itemize}

  \begin{block}<2->{Instructions}
    \begin{itemize}
    \item Integer arithmetic (2 to 1): $+, -, \oplus, \wedge, \vee, \cdot, /, \mod, <<, >>$.
    \item Conditional branches: $=, \neq, \leq, <, \geq, >$. If condition valid, branch to offset.
    \item Direct load/store: load or store a data in memory to/from a register.
    \item Indirect load/store:  load or store a data in memory at address in a register to/from another register.
    \item Load immediate: set the value of the given register (source of constants).
    \item Register move: copy a register into another.
  %   \item Non deterministic: load a non-deterministic value into a register. The semantic does not specify the source: it can be either user input, result of a true random number generator, etc.
  %   \item Halt: terminate the computation.
     
  %   \item Direct jump: goto address contained in the instruction.

  %   \item Call: direct jump to specified address and store the value of \gls{PC} in a register (x14 for ISAv1) or a dedicated stack (ISAv3).
  %   \item Return: jump to the address poped from the dedicated return stack (ISAv3 only).
  %   \item Indirect jump: go to the address present in the given register (ISAv1 only).
    
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Our ISAs}

    \begin{itemize}
    \item \textcolor{green}{ISAv1}: base ISA with indirect jumps.
    \item \textcolor{red}{ISAv2}: no indirect jumps at all.
    \item \textcolor{blue}{ISAv3}: backward indirect jumps allowed.
    \end{itemize}

  \begin{block}{Instructions (continued)}
    \begin{itemize}
    \item Non deterministic: load a non-deterministic value into a register. The semantic does not specify the source (TRNG, user input, etc).
    \item Halt: terminate the computation.
    \item Direct jump: go to address contained in the instruction.

    \item \textcolor{green}{Ca}\textcolor{blue}{ll}: direct jump to specified address and store the value of PC in a register (x14 for ISAv1) or a dedicated stack (ISAv3).
    \item \textcolor{blue}{Return}: jump to the address popped from the dedicated return stack (ISAv3 only).
    \item \textcolor{green}{Indirect jump}: go to the address present in the given register (ISAv1 only).
    
    \end{itemize}
  \end{block}
\end{frame}

% \begin{frame}
%   \frametitle{Idioms}

%   % how we wrote our benchmarks
  
% \end{frame}

\begin{frame}
  \frametitle{Benchmark: AES}

  
  \begin{block}{}%{Why this benchmark ?}
    \vspace{-.2cm}
    Need for security. Mix of various instruction types. Indirect jumps in our sbox implementation and to call procedures.
  \end{block}

  \begin{center}
    
  \begin{tabular}{| l | c | c | c | c | c | c |}
    \hline & \multicolumn{2}{c|}{\textcolor{green}{ISAv1}} & \multicolumn{2}{c|}{\textcolor{red}{ISAv2}} & \multicolumn{2}{c|}{\textcolor{blue}{ISAv3}}\\
    \hline Type & Count & Ratio & Count & Ratio & Count & Ratio\\
    \hline Arithmetic & 10011 & 0.44 & 9978 & 0.41 & 9825 & 0.44\\
    LoadImmediate & 5232 & 0.23 & \textcolor{red}{6767} & 0.27 & 5218 & 0.23\\
    IndirectLoad & 2650 & 0.12 & 2628 & 0.11 & 2554 & 0.11\\
    IndirectStore & 2464 & 0.11 & 2441 & 0.10 & 2368 & 0.11\\
    DirectJump & 668 & 0.03 & 719 & 0.03 & 668 & 0.03\\
    IndirectJump & 616 & 0.03 & X & X & 616 & 0.03\\
    ConditionalBranch & 601 & 0.03 & \textcolor{red}{1703} & 0.07  & 601 & 0.03\\
    RegisterMove & 370 & 0.02 & 370 & 0.02 & 370 & 0.02\\
    NonDeterministic & 2 & 0.00 & 2 & 0.00 & 2 & 0.00\\
    Halt & 1 & 0.00 & 1 & 0.00 & 1 & 0.00\\
    \hline
    Total & 22615 & 1.00 & 24609 & 1.00 & 22223 & 1.00\\
    \hline
  \end{tabular}
  \end{center}  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Benchmark: AES}

  \begin{block}{CFG extraction algorithms}
    \begin{itemize}
    \item Structural (s): just follow the semantic content of the instruction opcodes (branches have two successors, etc.).
    \item Tracking (t): track the return stack in addition to the structural method to find \verb|Return| destinations (easy without forward indirect jumps).
    \end{itemize}
  \end{block}

  \vspace{1cm}

\begin{footnotesize}
  \begin{tabular}{ | l || c | c || c | c | c | c |}
    \hline
     & Mean dur & Std dev & CFG nodes & nodes cov & CFG edges & edges cov \\
    \hline \textcolor{green}{ISAv1} s& 22635 & 24.1 & 778 & $99.6\%$ & \textcolor{red}{12437} & \textcolor{red}{$6.4\%$} \\
    \textcolor{red}{ISAv2} s & 24605 & 24.1 & 816 & $99.1\%$ & 836 & $99.0\%$ \\
    \textcolor{blue}{ISAv3} s & 22231 & 23.8 & 747 & $99.7\%$ & \textcolor{red}{11942} & \textcolor{red}{$6.4\%$} \\
    \textcolor{blue}{ISAv3} t & 22231 & 24.0 & 745 & $100\%$ & 764 & $100\%$ \\
    \hline
  \end{tabular}
\end{footnotesize}
\end{frame}



\begin{frame}[fragile]
  \frametitle{Dispatchers}

  \begin{block}{The problem}
    Branch to $n$ different destinations depending on a data value in a register.
  \end{block}

  \vspace{-.2cm}
\begin{lstlisting}[caption={ISAv2 dispatcher pattern},label=lst:pattern2]
  //x1 contains the value that decides where to branch (x1 in [0, 3])
  <@\textcolor{red}{load~~~x15, \#0}@>
  <@\textcolor{red}{beq~~~~proc0, x1, x15}@>
  <@\textcolor{red}{load~~~x15, \#1}@>
  <@\textcolor{red}{beq~~~~proc1, x1, x15}@>
  <@\textcolor{red}{load~~~x15, \#2}@>
  <@\textcolor{red}{beq~~~~proc2, x1, x15}@>
  <@\textcolor{red}{load~~~x15, \#3}@>
  <@\textcolor{red}{beq~~~~proc3, x1, x15}@>
  <@\textcolor{red}{jump~~~error\_handler}@>
\end{lstlisting}
  
\end{frame}

\begin{frame}
  \frametitle{Dispatchers costs}

  \begin{block}{Instructions count}
  \begin{tabular}{P{1cm} || P{2.5cm} | P{1.7cm} | P{1.7cm} || P{2.5cm} |}
    & Branch & Call & Return & Total \\
    \hline ISAv1 & $0$ & $1$ & $3 \cdot p$ & $3\cdot p + 1$ \\
    ISAv2 & $2\cdot p$ & $0$ & $p$ & $3\cdot p$ \\
    ISAv3 & $2\cdot p$ & $2\cdot p$ & $p$ & $5 \cdot p$ \\
  \end{tabular}
  \end{block}
 
  \begin{block}{Latency}
  \begin{tabular}{P{1cm} || P{2.5cm} | P{1.7cm} | P{1.7cm} || P{2.5cm} |}
    & Branch & Call & Return & Total \\
    \hline ISAv1 & $0$ & $1$ & $3$ & $4$ \\
    ISAv2 & $2\cdot \ceil{\log_2(p)} + 1$ & $0$ & $1$ & $2 + 2 \cdot \ceil{\log_2(p)}$ \\
    ISAv3 & $2\cdot \ceil{\log_2(p)} + 1$ & $2$ & $1$ & $4 + 2 \cdot \ceil{\log_2(p)}$ \\
  \end{tabular}
  \end{block}
  
\end{frame}

\begin{frame}[fragile]
  \frametitle{Dispatchers, a conclusion}

  \begin{block}{Branch based dispatchers are not efficient}
    The dispatcher latency is high and depends on the number of destinations $\rightarrow$ timing leakage.
  \end{block}

  \begin{block}<2->{A possible solution: n-fanout conditional branches}
    \begin{lstlisting}
  nbranch x1, #4
  jump    proc0
  jump    proc1
  jump    proc2
  jump    proc3
  jump    error_handler
\end{lstlisting}
  \end{block}
\end{frame}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../main"
%%% End:
