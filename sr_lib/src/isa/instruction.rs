/*
 * File: instruction.rs
 * Project: isa
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 20th May 2019 4:41:05 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;
use crate::machine::*;
use crate::program::*;

use std::collections::{BTreeMap, BTreeSet};
use std::fmt;
use std::hash::Hash;


pub trait Instruction: fmt::Debug + Clone + Copy + Default + fmt::Display + Send + Sync + Hash + Eq {
    type A: Address;
    type M: CanMemoryData;
    type R: CanRegisterValue<Self::A, Self::M>;

    fn resolve(ui: UnresolvedInstruction, ledger: &BTreeMap<String, InstructionAddress<Self::A>>) -> Result<Self, ResolutionError>;
    fn develop(pi: PseudoInstruction) -> Vec<Self>;
    fn apply<RM, MEM>(&self, state: &mut RM, mem: &mut MEM, monitor: Option<&mut Monitor<Self>>) 
    -> Result<ApplicationEffect, MachineError<Self>> 
    where   RM: RegisterMachine<Self::A, Self::R>,
            MEM: MemoryReadWrite<DataAddress<Self::A>, Self::M>;
    fn instruction_type(&self) -> InstructionType;
    fn successors(&self) -> BTreeSet<Successor<Self::A>>;

    fn branch(cond: Condition, add: InstructionAddress<Self::A>, r1: ReadableRegister, r2: ReadableRegister) -> Self;
    fn jump(add: InstructionAddress<Self::A>) -> Self;
    fn call(add: InstructionAddress<Self::A>) -> Self;
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum InstructionType {
    Arithmetic,
    ConditionalBranch,
    DirectJump,
    IndirectJump,
    DirectLoad,
    IndirectLoad,
    DirectStore,
    IndirectStore,
    RegisterMove,
    NonDeterministic,
    LoadImmediate,
    Other
}