/*
 * File: mod.rs
 * Project: instructions
 * Created Date: Friday April 12th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 26th April 2019 10:50:32 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

pub mod instruction_v2;
pub mod instruction_v1;
pub mod instruction_v3;


pub use self::instruction_v1::InstructionV1;
pub use self::instruction_v2::InstructionV2;
pub use self::instruction_v3::InstructionV3;