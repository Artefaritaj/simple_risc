/*
 * File: instruction.rs
 * Project: isa
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 21st May 2019 3:20:14 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;
use crate::machine::*;
use crate::program::*;
use crate::program::static_analysis::*;

use std::collections::{BTreeMap, BTreeSet};
use std::fmt;
use std::marker::PhantomData;
use std::convert::{ TryFrom, TryInto };

//A = Address type
//AO = Address offset type
//M = Memory data type
//R = Register value type


#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum InstructionV3<A, M, R> 
where   A: Address,
        M: CanMemoryData,
        R: CanRegisterValue<A, M> {

    IntegerArithmetic(ArithmeticOperation, WritableRegister, ReadableRegister, ReadableRegister),//Destination, Source 1, Source 2
    ConditionalBranch(Condition, InstructionAddress<A>, ReadableRegister, ReadableRegister),//Source 1, Source2, jump to address if condition is satisfied
    DirectJump(InstructionAddress<A>),

    DirectLoad(WritableRegister, DataAddress<A>),
    DirectStore(DataAddress<A>, ReadableRegister),
    IndirectLoad(WritableRegister, ReadableRegister),
    IndirectStore(ReadableRegister, ReadableRegister), // address, value
    NonDeterministic(WritableRegister),
    LoadImmediate(WritableRegister, R), //required for harvard architecture
    RegisterMove(WritableRegister, ReadableRegister),
    // IndirectJump(ReadableRegister), //jump to address in register
    Call(InstructionAddress<A>), //call = jump + store address of next instruction in "return stack"
    Return,

    Halt(PhantomData<M>)
}

impl<A, M, R> Instruction for InstructionV3<A, M, R>
where   A: Address + TryFrom<R>,
        M: CanMemoryData + From<R>,
        R: CanRegisterValue<A, M>,
        rand::distributions::Standard: rand::distributions::Distribution<R> {

    type A = A;
    type M = M;
    type R = R;

    fn instruction_type(&self) -> InstructionType {
        match self {
            InstructionV3::IntegerArithmetic(..)    => InstructionType::Arithmetic,
            InstructionV3::ConditionalBranch(..)    => InstructionType::ConditionalBranch,
            InstructionV3::DirectJump(..)           => InstructionType::DirectJump,
            InstructionV3::DirectLoad(..)           => InstructionType::DirectLoad,
            InstructionV3::IndirectLoad(..)         => InstructionType::IndirectLoad,
            InstructionV3::DirectStore(..)          => InstructionType::DirectStore,
            InstructionV3::IndirectStore(..)        => InstructionType::IndirectStore,
            InstructionV3::NonDeterministic(..)     => InstructionType::NonDeterministic,
            InstructionV3::LoadImmediate(..)        => InstructionType::LoadImmediate,
            InstructionV3::RegisterMove(..)         => InstructionType::RegisterMove,
            InstructionV3::Call(..)                 => InstructionType::DirectJump,
            InstructionV3::Return                   => InstructionType::IndirectJump,
            InstructionV3::Halt(..)                 => InstructionType::Other
        }
    }

    fn develop(pi: PseudoInstruction) -> Vec<Self> {
        let mut result: Vec<Self> = Vec::new();

        match pi {
            PseudoInstruction::Push(regs) => {
                result.push(
                    InstructionV3::LoadImmediate(ReadWriteRegister::X15.into(), A::default().next().into()) //this is a trick to load the address increment in x15, the temp register
                );
                for reg in regs {
                    result.push(
                        InstructionV3::IndirectStore(
                                ReadWriteRegister::StackPointer.into(), 
                                reg.into()));
                    result.push(
                        InstructionV3::IntegerArithmetic(
                            ArithmeticOperation::Add, 
                            ReadWriteRegister::StackPointer.into(),
                            ReadWriteRegister::StackPointer.into(),
                            ReadWriteRegister::X15.into()));
                }
            },
            PseudoInstruction::Pop(regs) => {
                result.push(
                    InstructionV3::LoadImmediate(ReadWriteRegister::X15.into(), A::default().next().into()) //this is a trick to load the address increment in x15, the temp register
                );
                for reg in regs {
                    result.push(
                        InstructionV3::IntegerArithmetic(
                            ArithmeticOperation::Substract, 
                            ReadWriteRegister::StackPointer.into(),
                            ReadWriteRegister::StackPointer.into(),
                            ReadWriteRegister::X15.into()));
                    result.push(
                        InstructionV3::IndirectLoad(
                                reg.into(),
                                ReadWriteRegister::StackPointer.into()));
                }
            }
        }

        result
    }

    fn resolve(ui: UnresolvedInstruction, ledger: &BTreeMap<String, InstructionAddress<A>>) -> Result<Self, ResolutionError> {
        match ui {
            UnresolvedInstruction::UnresolvedConditionalBranch(cond, label, r1, r2) => Ok(InstructionV3::ConditionalBranch(cond, label.resolve_inst(ledger)?, r1, r2)),
            UnresolvedInstruction::UnresolvedCall(label) => Ok(InstructionV3::Call(label.resolve_inst(ledger)?)),
            UnresolvedInstruction::UnresolvedJump(label) => Ok(InstructionV3::DirectJump(label.resolve_inst(ledger)?)),
        }
    }

    fn apply<RM, MEM>(&self, state: &mut RM, mem: &mut MEM, monitor: Option<&mut Monitor<Self>>) 
    -> Result<ApplicationEffect, MachineError<Self>> 
    where   RM: RegisterMachine<Self::A, Self::R>,
            MEM: MemoryReadWrite<DataAddress<Self::A>, Self::M> {

        //apply instruction
        let mut effect = match self {
            InstructionV3::DirectLoad(reg, add)                                   => inst_ops::transfer::dload_app(state, mem, *reg, *add),
            InstructionV3::DirectStore(add, reg)                                  => inst_ops::transfer::dstore_app(state, mem, *reg, *add),
            InstructionV3::IndirectLoad(val, add)                                 => inst_ops::transfer::iload_app(state, mem, *val, *add),
            InstructionV3::IndirectStore(add, val)                                => inst_ops::transfer::istore_app(state, mem, *val, *add),
            InstructionV3::LoadImmediate(reg, imm)                                => inst_ops::immediate::load_immediate(state, *reg, *imm),
            InstructionV3::IntegerArithmetic(op, dest, source1, source2)          => inst_ops::arithmetic::apply_arithmetic_op(state, *op, *dest, *source1, *source2),
            InstructionV3::ConditionalBranch(cond, jump_to, source1, source2)     => inst_ops::branch::apply_branch(state, *cond, *jump_to, *source1, *source2),
            // InstructionV3::Call(add)                                              => inst_ops::call_ret::apply_call(state, *add),
            // InstructionV3::Return                                                 => inst_ops::call_ret::apply_ret(state),
            InstructionV3::DirectJump(add)                                              => inst_ops::jump::apply_direct_jump(state, *add),
            InstructionV3::RegisterMove(dest, source)                             => inst_ops::reg_move::apply_move(state, *dest, *source),
            InstructionV3::Halt(_)                                                => inst_ops::halt::apply_halt(),
            InstructionV3::NonDeterministic(reg)                                  => inst_ops::non_deterministic::apply_nd(state, *reg),
            InstructionV3::Call(add)                                              => inst_ops::call_ret::apply_call(state, *add),
            InstructionV3::Return                                                 => inst_ops::call_ret::apply_ret(state)
            // _ => Err(MachineError::UnimplementedInstruction(inst))
        }?;

        //monitor
        if let Some(monitor) = monitor {
            monitor.register_instruction(self);
        }

        //1) implicit Program Counter increment
        if effect.program_counter_set == false {//PC may have been set (e.g. in a jump)
            let mut pc: A = state.read_reg(Register::program_counter()).try_into()   .map_err(|_|MachineError::NoConvert)?;
            pc.inc();
            state.write_reg(Register::program_counter(), pc.into());

            effect.program_counter_set = true;
        }

        return Ok(effect);
    }

    fn successors(&self) -> BTreeSet<Successor<Self::A>> {
        match self {
            InstructionV3::ConditionalBranch(_, jump_to, _, _)          => btreeset![Successor::Next, Successor::DirectJump(*jump_to)],
            InstructionV3::Call(add)                                    => btreeset![Successor::Call(*add)],
            InstructionV3::Return                                       => btreeset![Successor::Return],
            InstructionV3::DirectJump(add)                              => btreeset![Successor::DirectJump(*add)],
            InstructionV3::Halt(_)                                      => btreeset![Successor::None],
            _ => btreeset![Successor::Next],
        }
    }

    fn branch(cond: Condition, add: InstructionAddress<Self::A>, r1: ReadableRegister, r2: ReadableRegister) -> Self {
        InstructionV3::ConditionalBranch(cond, add, r1, r2)
    }

    fn jump(add: InstructionAddress<Self::A>) -> Self {
        InstructionV3::DirectJump(add)
    }

    fn call(add: InstructionAddress<Self::A>) -> Self {
        // InstructionV3::Call(add)
        InstructionV3::DirectJump(add)
    }
}


impl<A, M, R> Default for InstructionV3<A, M, R>
where   A: Address,
        M: CanMemoryData,
        R: CanRegisterValue<A, M> {
    fn default() -> InstructionV3<A, M, R> {
        InstructionV3::Halt(PhantomData)
    }
}

impl<A, M, R> fmt::Display for InstructionV3<A, M, R>
where   A: Address + fmt::LowerHex,
        M: CanMemoryData,
        R: CanRegisterValue<A, M> + fmt::LowerHex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            InstructionV3::IntegerArithmetic(op, dest, s1, s2)    => write!(f, "{}\t{}, {}, {}", op, dest, s1, s2),
            InstructionV3::ConditionalBranch(cond, add, s1, s2)   => write!(f, "{}\t{:x}, {}, {}", cond, add, s1, s2),
            InstructionV3::DirectJump(add)                              => write!(f, "goto\t{:x}", add),

            InstructionV3::DirectLoad(dest, add)                  => write!(f, "load\t{}, {:x}", dest, add),
            InstructionV3::DirectStore(add, source)               => write!(f, "store\t{:x}, {}", add, source),
            InstructionV3::IndirectLoad(dest, source)             => write!(f, "load\t{}, {}", dest, source),
            InstructionV3::IndirectStore(add, val)                => write!(f, "store\t{}, {}", add, val),
            InstructionV3::NonDeterministic(reg)                  => write!(f, "np\t{}", reg),
            InstructionV3::LoadImmediate(reg, val)                => write!(f, "load\t{}, #0x{:x}", reg, val),

            InstructionV3::RegisterMove(dest, source)             => write!(f, "move\t{}, {}", dest, source),
            InstructionV3::Call(add)                              => write!(f, "call\t{:x}", add),
            InstructionV3::Return                                 => write!(f, "return"),
            InstructionV3::Halt(_)                                => write!(f, "halt"),
            
            // _ => write!(f, "display not implemeted")
        }
    }
}

impl From<InstructionV3<u64, u64, u64>> for InstructionV3<u64, DeterminismTracer<u64>, DeterminismTracer<u64>> {
    fn from(i: InstructionV3<u64, u64, u64>) -> InstructionV3<u64, DeterminismTracer<u64>, DeterminismTracer<u64>> {
        match i {
            InstructionV3::IntegerArithmetic(op, dest, s1, s2)    => InstructionV3::IntegerArithmetic(op, dest, s1, s2),
            InstructionV3::ConditionalBranch(cond, add, s1, s2)   => InstructionV3::ConditionalBranch(cond, add, s1, s2),
            InstructionV3::DirectJump(add)                        => InstructionV3::DirectJump(add),
            InstructionV3::DirectLoad(dest, add)                  => InstructionV3::DirectLoad(dest, add),
            InstructionV3::DirectStore(add, source)               => InstructionV3::DirectStore(add, source),
            InstructionV3::IndirectLoad(dest, source)             => InstructionV3::IndirectLoad(dest, source),
            InstructionV3::IndirectStore(add, val)                => InstructionV3::IndirectStore(add, val),
            InstructionV3::NonDeterministic(reg)                  => InstructionV3::NonDeterministic(reg),
            InstructionV3::LoadImmediate(reg, val)                => InstructionV3::LoadImmediate(reg, val.into()),

            InstructionV3::RegisterMove(dest, source)             => InstructionV3::RegisterMove(dest, source),
            InstructionV3::Call(add)                              => InstructionV3::Call(add),
            InstructionV3::Return                                 => InstructionV3::Return,
            InstructionV3::Halt(_)                                => InstructionV3::Halt(PhantomData),
            
            // _ => { unimplemented!() }
        }
    }
}