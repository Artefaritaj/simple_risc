/*
 * File: instruction.rs
 * Project: isa
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 21st May 2019 3:20:03 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;
use crate::machine::*;
use crate::program::*;
use crate::program::static_analysis::*;

use std::collections::{BTreeMap, BTreeSet};
use std::fmt;
use std::marker::PhantomData;
use std::convert::{ TryFrom, TryInto };

//A = Address type
//AO = Address offset type
//M = Memory data type
//R = Register value type


#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum InstructionV1<A, M, R> 
where   A: Address,
        M: CanMemoryData,
        R: CanRegisterValue<A, M> {

    IntegerArithmetic(ArithmeticOperation, WritableRegister, ReadableRegister, ReadableRegister),//Destination, Source 1, Source 2
    ConditionalBranch(Condition, InstructionAddress<A>, ReadableRegister, ReadableRegister),//Source 1, Source2, jump to address if condition is satisfied
    DirectJump(InstructionAddress<A>),

    DirectLoad(WritableRegister, DataAddress<A>),
    DirectStore(DataAddress<A>, ReadableRegister),
    IndirectLoad(WritableRegister, ReadableRegister),
    IndirectStore(ReadableRegister, ReadableRegister), // address, value
    NonDeterministic(WritableRegister),
    LoadImmediate(WritableRegister, R), //required for harvard architecture
    RegisterMove(WritableRegister, ReadableRegister),
    IndirectJump(ReadableRegister), //jump to address in register
    Call(InstructionAddress<A>), //call = jump + store address of next instruction in register x14 (Link register)
    // Return,

    Halt(PhantomData<M>)
}

impl<A, M, R> Instruction for InstructionV1<A, M, R>
where   A: Address + TryFrom<R>,
        M: CanMemoryData + From<R>,
        R: CanRegisterValue<A, M>,
        rand::distributions::Standard: rand::distributions::Distribution<R> {

    type A = A;
    type M = M;
    type R = R;

    fn instruction_type(&self) -> InstructionType {
        match self {
            InstructionV1::IntegerArithmetic(..)    => InstructionType::Arithmetic,
            InstructionV1::ConditionalBranch(..)    => InstructionType::ConditionalBranch,
            InstructionV1::DirectJump(..)           => InstructionType::DirectJump,
            InstructionV1::DirectLoad(..)           => InstructionType::DirectLoad,
            InstructionV1::IndirectLoad(..)         => InstructionType::IndirectLoad,
            InstructionV1::DirectStore(..)          => InstructionType::DirectStore,
            InstructionV1::IndirectStore(..)        => InstructionType::IndirectStore,
            InstructionV1::NonDeterministic(..)     => InstructionType::NonDeterministic,
            InstructionV1::LoadImmediate(..)        => InstructionType::LoadImmediate,
            InstructionV1::RegisterMove(..)         => InstructionType::RegisterMove,
            InstructionV1::IndirectJump(..)         => InstructionType::IndirectJump,
            InstructionV1::Call(..)                 => InstructionType::DirectJump,
            InstructionV1::Halt(..)                 => InstructionType::Other
        }
    }

    fn develop(pi: PseudoInstruction) -> Vec<Self> {
        let mut result: Vec<Self> = Vec::new();

        match pi {
            PseudoInstruction::Push(regs) => {
                result.push(
                    InstructionV1::LoadImmediate(ReadWriteRegister::X15.into(), A::default().next().into()) //this is a trick to load the address increment in x15, the temp register
                );
                for reg in regs {
                    result.push(
                        InstructionV1::IndirectStore(
                                ReadWriteRegister::StackPointer.into(), 
                                reg.into()));
                    result.push(
                        InstructionV1::IntegerArithmetic(
                            ArithmeticOperation::Add, 
                            ReadWriteRegister::StackPointer.into(),
                            ReadWriteRegister::StackPointer.into(),
                            ReadWriteRegister::X15.into()));
                }
            },
            PseudoInstruction::Pop(regs) => {
                result.push(
                    InstructionV1::LoadImmediate(ReadWriteRegister::X15.into(), A::default().next().into()) //this is a trick to load the address increment in x15, the temp register
                );
                for reg in regs {
                    result.push(
                        InstructionV1::IntegerArithmetic(
                            ArithmeticOperation::Substract, 
                            ReadWriteRegister::StackPointer.into(),
                            ReadWriteRegister::StackPointer.into(),
                            ReadWriteRegister::X15.into()));
                    result.push(
                        InstructionV1::IndirectLoad(
                                reg.into(),
                                ReadWriteRegister::StackPointer.into()));
                }
            }
        }

        result
    }

    fn resolve(ui: UnresolvedInstruction, ledger: &BTreeMap<String, InstructionAddress<A>>) -> Result<Self, ResolutionError> {
        match ui {
            UnresolvedInstruction::UnresolvedConditionalBranch(cond, label, r1, r2) => Ok(InstructionV1::ConditionalBranch(cond, label.resolve_inst(ledger)?, r1, r2)),
            UnresolvedInstruction::UnresolvedCall(label) => Ok(InstructionV1::Call(label.resolve_inst(ledger)?)),
            UnresolvedInstruction::UnresolvedJump(label) => Ok(InstructionV1::DirectJump(label.resolve_inst(ledger)?)),
        }
    }

    fn apply<RM, MEM>(&self, state: &mut RM, mem: &mut MEM, monitor: Option<&mut Monitor<Self>>) 
    -> Result<ApplicationEffect, MachineError<Self>> 
    where   RM: RegisterMachine<Self::A, Self::R>,
            MEM: MemoryReadWrite<DataAddress<Self::A>, Self::M> {


        //apply instruction
        let mut effect = match self {
            InstructionV1::DirectLoad(reg, add)                                   => inst_ops::transfer::dload_app(state, mem, *reg, *add),
            InstructionV1::DirectStore(add, reg)                                  => inst_ops::transfer::dstore_app(state, mem, *reg, *add),
            InstructionV1::IndirectLoad(val, add)                                 => inst_ops::transfer::iload_app(state, mem, *val, *add),
            InstructionV1::IndirectStore(add, val)                                => inst_ops::transfer::istore_app(state, mem, *val, *add),
            InstructionV1::LoadImmediate(reg, imm)                                => inst_ops::immediate::load_immediate(state, *reg, *imm),
            InstructionV1::IntegerArithmetic(op, dest, source1, source2)          => inst_ops::arithmetic::apply_arithmetic_op(state, *op, *dest, *source1, *source2),
            InstructionV1::ConditionalBranch(cond, jump_to, source1, source2)     => inst_ops::branch::apply_branch(state, *cond, *jump_to, *source1, *source2),
            InstructionV1::Call(add)                                              => inst_ops::call_ret::apply_call_x14(state, *add),
            // InstructionV1::Return                                                 => inst_ops::call_ret::apply_ret(state),
            InstructionV1::DirectJump(add)                                        => inst_ops::jump::apply_direct_jump(state, *add),
            InstructionV1::IndirectJump(addreg)                                   => inst_ops::jump::apply_indirect_jump(state, *addreg),
            InstructionV1::RegisterMove(dest, source)                             => inst_ops::reg_move::apply_move(state, *dest, *source),
            InstructionV1::Halt(_)                                                => inst_ops::halt::apply_halt(),
            InstructionV1::NonDeterministic(reg)                                  => inst_ops::non_deterministic::apply_nd(state, *reg),
            // _ => Err(MachineError::UnimplementedInstruction(inst))
        }?;

        //monitor
        if let Some(monitor) = monitor {
            monitor.register_instruction(self);
        }

        //1) implicit Program Counter increment
        if effect.program_counter_set == false {//PC may have been set (e.g. in a jump)
            let mut pc: A = state.read_reg(Register::program_counter()).try_into()   .map_err(|_|MachineError::NoConvert)?;
            pc.inc();
            state.write_reg(Register::program_counter(), pc.into());

            effect.program_counter_set = true;
        }

        return Ok(effect);
    }

    fn successors(&self) -> BTreeSet<Successor<Self::A>> {
        match self {
            InstructionV1::ConditionalBranch(_, jump_to, _, _)          => btreeset![Successor::Next, Successor::DirectJump(*jump_to)],
            InstructionV1::Call(add)                                    => btreeset![Successor::DirectJump(*add)],// /!\
            InstructionV1::IndirectJump(reg)                            => btreeset![Successor::Register(*reg)],
            // InstructionV1::Return                                                 => inst_ops::call_ret::apply_ret(state),
            InstructionV1::DirectJump(add)                              => btreeset![Successor::DirectJump(*add)],
            InstructionV1::Halt(_)                                      => btreeset![Successor::None],
            _ => btreeset![Successor::Next],
        }
    }

    fn branch(cond: Condition, add: InstructionAddress<Self::A>, r1: ReadableRegister, r2: ReadableRegister) -> Self {
        InstructionV1::ConditionalBranch(cond, add, r1, r2)
    }

    fn jump(add: InstructionAddress<Self::A>) -> Self {
        InstructionV1::DirectJump(add)
    }

    fn call(add: InstructionAddress<Self::A>) -> Self {
        InstructionV1::Call(add)
        // InstructionV1::DirectJump(add)
    }
}

// impl<A, M, R> From<InstructionV1<A, M, R>> for InstructionV1< DeterminismTracer<A>, DeterminismTracer<M>, DeterminismTracer<R>>
// where   A: Address + From<R>,
//         M: CanMemoryData + From<R>,
//         R: CanRegisterValue<A, M>,
//         DeterminismTracer<A>: From<DeterminismTracer<R>>,
//         DeterminismTracer<R>: From<DeterminismTracer<A>>,
//         DeterminismTracer<M>: From<DeterminismTracer<R>>,
//         DeterminismTracer<R>: From<DeterminismTracer<M>>,
//         // rand::distributions::Standard: rand::distributions::Distribution<R>
// {
//     fn from(i: InstructionV1<A, M, R>) -> InstructionV1< DeterminismTracer<A>, DeterminismTracer<M>, DeterminismTracer<R>> {
//         match i {
//             InstructionV1::DirectLoad(reg, add)                                   => InstructionV1::DirectLoad(reg.into(), DataAddress( add.0.into() )),
//             InstructionV1::DirectStore(add, reg)                                  => InstructionV1::DirectStore(DataAddress(add.0.into()), reg.into()),
//             InstructionV1::IndirectLoad(val, add)                                 => InstructionV1::IndirectLoad(val.into(), add.into()),
//             InstructionV1::IndirectStore(add, val)                                => InstructionV1::IndirectStore(add.into(), val.into()),
//             InstructionV1::LoadImmediate(reg, imm)                                => InstructionV1::LoadImmediate(reg.into(), imm.into()),
//             InstructionV1::IntegerArithmetic(op, dest, source1, source2)          => InstructionV1::IntegerArithmetic(op, dest.into(), source1.into(), source2.into()),
//             InstructionV1::ConditionalBranch(cond, jump_to, source1, source2)     => InstructionV1::ConditionalBranch(cond, jump_to.into(), source1.into(), source2.into()),
//             // InstructionV1::Call(add)                                              => inst_ops::call_ret::apply_call(state, *add),
//             // InstructionV1::Return                                                 => inst_ops::call_ret::apply_ret(state),
//             InstructionV1::Jump(add)                                              => InstructionV1::Jump(add.into()),
//             InstructionV1::RegisterMove(dest, source)                             => InstructionV1::RegisterMove(dest.into(), source.into()),
//             InstructionV1::Halt(_)                                                => InstructionV1::Halt(PhantomData),
//             InstructionV1::NonDeterministic(reg)                                  => InstructionV1::NonDeterministic(reg.into()),
//             // _ => Err(MachineError::UnimplementedInstruction(i))
//             // _ => unimplemented!()
//         }
//     }

// }

impl<A, M, R> Default for InstructionV1<A, M, R>
where   A: Address,
        M: CanMemoryData,
        R: CanRegisterValue<A, M> {
    fn default() -> InstructionV1<A, M, R> {
        InstructionV1::Halt(PhantomData)
    }
}

impl<A, M, R> fmt::Display for InstructionV1<A, M, R>
where   A: Address + fmt::LowerHex,
        M: CanMemoryData,
        R: CanRegisterValue<A, M> + fmt::LowerHex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            InstructionV1::IntegerArithmetic(op, dest, s1, s2)    => write!(f, "{}\t{}, {}, {}", op, dest, s1, s2),
            InstructionV1::ConditionalBranch(cond, add, s1, s2)   => write!(f, "{}\t{:x}, {}, {}", cond, add, s1, s2),
            InstructionV1::DirectJump(add)                        => write!(f, "goto\t{:x}", add),
            InstructionV1::IndirectJump(addreg)                   => write!(f, "jump\t{}", addreg),

            InstructionV1::DirectLoad(dest, add)                  => write!(f, "load\t{}, {:x}", dest, add),
            InstructionV1::DirectStore(add, source)               => write!(f, "store\t{:x}, {}", add, source),
            InstructionV1::IndirectLoad(dest, source)             => write!(f, "load\t{}, {}", dest, source),
            InstructionV1::IndirectStore(add, val)                => write!(f, "store\t{}, {}", add, val),
            InstructionV1::NonDeterministic(reg)                  => write!(f, "np\t{}", reg),
            InstructionV1::LoadImmediate(reg, val)                => write!(f, "load\t{}, #0x{:x}", reg, val),

            InstructionV1::RegisterMove(dest, source)             => write!(f, "move\t{}, {}", dest, source),
            InstructionV1::Call(add)                              => write!(f, "call\t{:x}", add),
            // InstructionV1::Return                                 => write!(f, "return"),
            InstructionV1::Halt(_)                                => write!(f, "halt"),
            
            // _ => write!(f, "display not implemeted")
        }
    }
}

impl From<InstructionV1<u64, u64, u64>> for InstructionV1<u64, DeterminismTracer<u64>, DeterminismTracer<u64>> {
    fn from(i: InstructionV1<u64, u64, u64>) -> InstructionV1<u64, DeterminismTracer<u64>, DeterminismTracer<u64>> {
        match i {
            InstructionV1::IntegerArithmetic(op, dest, s1, s2)    => InstructionV1::IntegerArithmetic(op, dest, s1, s2),
            InstructionV1::ConditionalBranch(cond, add, s1, s2)   => InstructionV1::ConditionalBranch(cond, add, s1, s2),
            InstructionV1::DirectJump(add)                        => InstructionV1::DirectJump(add),
            InstructionV1::DirectLoad(dest, add)                  => InstructionV1::DirectLoad(dest, add),
            InstructionV1::DirectStore(add, source)               => InstructionV1::DirectStore(add, source),
            InstructionV1::IndirectLoad(dest, source)             => InstructionV1::IndirectLoad(dest, source),
            InstructionV1::IndirectStore(add, val)                => InstructionV1::IndirectStore(add, val),
            InstructionV1::NonDeterministic(reg)                  => InstructionV1::NonDeterministic(reg),
            InstructionV1::LoadImmediate(reg, val)                => InstructionV1::LoadImmediate(reg, val.into()),

            InstructionV1::RegisterMove(dest, source)             => InstructionV1::RegisterMove(dest, source),
            InstructionV1::Call(add)                              => InstructionV1::Call(add),
            InstructionV1::IndirectJump(addreg)                   => InstructionV1::IndirectJump(addreg),
            InstructionV1::Halt(_)                                => InstructionV1::Halt(PhantomData),
            
            // _ => { unimplemented!() }
        }
    }
}