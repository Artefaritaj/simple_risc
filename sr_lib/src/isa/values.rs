/*
 * File: values.rs
 * Project: isa
 * Created Date: Friday March 22nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 19th April 2019 3:04:00 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//A = Address type
//M = Memory data type 
//R = Register value type

use std::fmt;
use std::hash::Hash;
use crate::isa::*;

pub trait CanRegisterValue<A, M>: Default + Copy + From<A> + From<M> + Sync + Send + fmt::Debug + ArithmeticCapable + ConditionCapable + fmt::LowerHex + Hash + Eq {}
impl<A, M, T: Default + Copy + From<A> + From<M> + Sync + Send + fmt::Debug + ArithmeticCapable + ConditionCapable + fmt::LowerHex + Hash + Eq> CanRegisterValue<A, M> for T {}

pub trait CanMemoryData: Copy + Default + Sync + Send + fmt::Debug + Hash + Eq {}
impl<T: Copy + Default + Sync + Send + fmt::Debug + Hash + Eq> CanMemoryData for T {}