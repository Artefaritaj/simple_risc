/*
 * File: errors.rs
 * Project: isa
 * Created Date: Tuesday March 26th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 28th March 2019 3:05:14 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use failure::Fail;

#[derive(Debug,Fail)]
pub enum ISAError {
    #[fail(display = "{} cannot be parsed as a register", _0)]
    RegisterParseError(String),

    #[fail(display = "{} cannot be parsed as a condition", _0)]
    ConditionParseError(String),

    #[fail(display = "{} cannot be parsed as an arithmetic operation", _0)]
    ArithmeticOperationParseError(String)
}

#[derive(Debug,Fail)]
pub enum ResolutionError {
    #[fail(display = "Label {} does not exist in ledger", _0)]
    LabelDoesNotExist(String),

    #[fail(display = "Label {} cannot be resolved", _0)]
    LabelCannotBeResolved(String),

    #[fail(display = "{} is not an instruction and cannot be resolved", _0)]
    NotAnInstruction(String)
}