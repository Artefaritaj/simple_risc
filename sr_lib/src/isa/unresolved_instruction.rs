/*
 * File: unresolved_instruction.rs
 * Project: isa
 * Created Date: Thursday March 28th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 15th April 2019 11:30:04 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum UnresolvedInstruction {
            UnresolvedConditionalBranch(Condition, Label, ReadableRegister, ReadableRegister),
            UnresolvedCall(Label),
            UnresolvedJump(Label),
}