/*
 * File: register.rs
 * Project: isa
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 15th April 2019 11:47:26 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//copied from RISC-V registers

use std::str::FromStr;
use std::fmt;

use crate::isa::errors::*;

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum Register {
    ReadWrite(ReadWriteRegister),
    Read(ReadOnlyRegister),
    Write(WriteOnlyRegister),
    Special(SpecialRegister)
}

impl Register {
    pub fn program_counter() -> Register {
        Register::Special(SpecialRegister::ProgramCounter)
    }

    pub fn stack_pointer() -> Register {
        Register::ReadWrite(ReadWriteRegister::StackPointer)
    }

    pub fn zero() -> Register {
        Register::Read(ReadOnlyRegister::Zero)
    }

    pub fn full() -> Register {
        Register::Read(ReadOnlyRegister::Full)
    }

    pub fn x1() -> Register {
        Register::ReadWrite(ReadWriteRegister::X1)
    }

    pub fn x2() -> Register {
        Register::ReadWrite(ReadWriteRegister::X2)
    }

    pub fn x3() -> Register {
        Register::ReadWrite(ReadWriteRegister::X3)
    }

    pub fn x4() -> Register {
        Register::ReadWrite(ReadWriteRegister::X4)
    }

    pub fn x5() -> Register {
        Register::ReadWrite(ReadWriteRegister::X5)
    }

    pub fn x6() -> Register {
        Register::ReadWrite(ReadWriteRegister::X6)
    }

    pub fn x7() -> Register {
        Register::ReadWrite(ReadWriteRegister::X7)
    }

    pub fn x8() -> Register {
        Register::ReadWrite(ReadWriteRegister::X8)
    }

    pub fn x9() -> Register {
        Register::ReadWrite(ReadWriteRegister::X9)
    }

    pub fn x10() -> Register {
        Register::ReadWrite(ReadWriteRegister::X10)
    }

    pub fn x11() -> Register {
        Register::ReadWrite(ReadWriteRegister::X11)
    }

    pub fn x12() -> Register {
        Register::ReadWrite(ReadWriteRegister::X12)
    }

    pub fn x13() -> Register {
        Register::ReadWrite(ReadWriteRegister::X13)
    }

    pub fn x14() -> Register {
        Register::ReadWrite(ReadWriteRegister::X14)
    }

    pub fn x15() -> Register {
        Register::ReadWrite(ReadWriteRegister::X15)
    }

    pub fn x16() -> Register {
        Register::ReadWrite(ReadWriteRegister::X16)
    }
}

impl From<ReadWriteRegister> for Register {
    fn from(rw_reg: ReadWriteRegister) -> Register {
        Register::ReadWrite(rw_reg)
    }
}

impl From<ReadOnlyRegister> for Register {
    fn from(r_reg: ReadOnlyRegister) -> Register {
        Register::Read(r_reg)
    }
}

// impl From<WriteOnlyRegister> for Register {
//     fn from(w_reg: WriteOnlyRegister) -> Register {
//         Register::Write(w_reg)
//     }
// }

impl From<SpecialRegister> for Register {
    fn from(s_reg: SpecialRegister) -> Register {
        Register::Special(s_reg)
    }
}

impl From<WritableRegister> for Register {
    fn from(w_reg: WritableRegister) -> Register {
        match w_reg {
            WritableRegister::ReadWrite(rwr)    => Register::ReadWrite(rwr),
            WritableRegister::Write(wr)         => Register::Write(wr)
        }
    }
}

impl From<ReadableRegister> for Register {
    fn from(r_reg: ReadableRegister) -> Register {
        match r_reg {
            ReadableRegister::ReadWrite(rwr)    => Register::ReadWrite(rwr),
            ReadableRegister::Read(rr)         => Register::Read(rr)
        }
    }
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum WritableRegister {
    ReadWrite(ReadWriteRegister),
    Write(WriteOnlyRegister)
}

impl From<ReadWriteRegister> for WritableRegister {
    fn from(rw_reg: ReadWriteRegister) -> WritableRegister {
        WritableRegister::ReadWrite(rw_reg)
    }
}

impl From<WriteOnlyRegister> for WritableRegister {
    fn from(w_reg: WriteOnlyRegister) -> WritableRegister {
        WritableRegister::Write(w_reg)
    }
}

impl From<ReadWriteRegister> for ReadableRegister {
    fn from(rw_reg: ReadWriteRegister) -> ReadableRegister {
        ReadableRegister::ReadWrite(rw_reg)
    }
}

impl From<ReadOnlyRegister> for ReadableRegister {
    fn from(r_reg: ReadOnlyRegister) -> ReadableRegister {
        ReadableRegister::Read(r_reg)
    }
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum ReadableRegister {
    ReadWrite(ReadWriteRegister),
    Read(ReadOnlyRegister)
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum ReadWriteRegister {
    X1,
    X2,
    X3,
    X4,
    X5,
    X6,
    X7,
    X8,
    X9,
    X10,
    X11,
    X12,
    X13,
    X14,
    X15,
    X16,
    StackPointer
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum WriteOnlyRegister {
    // StackPointer
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum ReadOnlyRegister {
    Zero,
    Full
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum SpecialRegister {
    ProgramCounter
}


impl FromStr for ReadWriteRegister {
    type Err = ISAError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "x1"                    => Ok(ReadWriteRegister::X1),
            "x2"                    => Ok(ReadWriteRegister::X2),
            "x3"                    => Ok(ReadWriteRegister::X3),
            "x4"                    => Ok(ReadWriteRegister::X4),
            "x5"                    => Ok(ReadWriteRegister::X5),
            "x6"                    => Ok(ReadWriteRegister::X6),
            "x7"                    => Ok(ReadWriteRegister::X7),
            "x8"                    => Ok(ReadWriteRegister::X8),
            "x9"                    => Ok(ReadWriteRegister::X9),
            "x10"                   => Ok(ReadWriteRegister::X10),
            "x11"                   => Ok(ReadWriteRegister::X11),
            "x12"                   => Ok(ReadWriteRegister::X12),
            "x13"                   => Ok(ReadWriteRegister::X13),
            "x14"                   => Ok(ReadWriteRegister::X14),
            "x15"                   => Ok(ReadWriteRegister::X15),
            "x16"                   => Ok(ReadWriteRegister::X16),
            "sp" | "stack_pointer"  => Ok(ReadWriteRegister::StackPointer),
            _ => Err(ISAError::RegisterParseError(s.to_owned()))
        }
    }
}

impl FromStr for ReadOnlyRegister {
    type Err = ISAError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "zero" | "x0"           => Ok(ReadOnlyRegister::Zero),
            "full" | "xff"          => Ok(ReadOnlyRegister::Full),
            _ => Err(ISAError::RegisterParseError(s.to_owned()))
        }
    }
}

impl FromStr for WriteOnlyRegister {
    type Err = ISAError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            // "stack_pointer" | "sp"  => Ok(WriteOnlyRegister::StackPointer),
            _ => Err(ISAError::RegisterParseError(s.to_owned()))
        }
    }
}

impl FromStr for SpecialRegister {
    type Err = ISAError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "program_counter" | "pc"    => Ok(SpecialRegister::ProgramCounter),
            _ => Err(ISAError::RegisterParseError(s.to_owned()))
        }
    }
}


impl FromStr for WritableRegister {
    type Err = ISAError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match ReadWriteRegister::from_str(s) {
            Ok(rw) => { return Ok(WritableRegister::ReadWrite(rw)); },
            Err(_) => {}
        }

        match WriteOnlyRegister::from_str(s) {
            Ok(w) => { return Ok(WritableRegister::Write(w)); },
            Err(_) => {}
        }

        Err(ISAError::RegisterParseError(s.to_owned()))
    }
}

impl FromStr for ReadableRegister {
    type Err = ISAError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match ReadWriteRegister::from_str(s) {
            Ok(rw) => { return Ok(ReadableRegister::ReadWrite(rw)); },
            Err(_) => {}
        }

        match ReadOnlyRegister::from_str(s) {
            Ok(r) => { return Ok(ReadableRegister::Read(r)); },
            Err(_) => {}
        }

        Err(ISAError::RegisterParseError(s.to_owned()))
    }
}

impl FromStr for Register {
    type Err = ISAError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match ReadWriteRegister::from_str(s) {
            Ok(rw) => { return Ok(Register::ReadWrite(rw)); },
            Err(_) => {}
        }

        match WriteOnlyRegister::from_str(s) {
            Ok(w) => { return Ok(Register::Write(w)); },
            Err(_) => {}
        }

        match ReadOnlyRegister::from_str(s) {
            Ok(r) => { return Ok(Register::Read(r)); },
            Err(_) => {}
        }

        match SpecialRegister::from_str(s) {
            Ok(s) => { return Ok(Register::Special(s)); },
            Err(_) => {}
        }

        Err(ISAError::RegisterParseError(s.to_owned()))
    }
}

impl fmt::Display for ReadWriteRegister {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ReadWriteRegister::X1 => write!(f, "x1"),
            ReadWriteRegister::X2 => write!(f, "x2"),
            ReadWriteRegister::X3 => write!(f, "x3"),
            ReadWriteRegister::X4 => write!(f, "x4"),
            ReadWriteRegister::X5 => write!(f, "x5"),
            ReadWriteRegister::X6 => write!(f, "x6"),
            ReadWriteRegister::X7 => write!(f, "x7"),
            ReadWriteRegister::X8 => write!(f, "x8"),
            ReadWriteRegister::X9 => write!(f, "x9"),
            ReadWriteRegister::X10 => write!(f, "x10"),
            ReadWriteRegister::X11 => write!(f, "x11"),
            ReadWriteRegister::X12 => write!(f, "x12"),
            ReadWriteRegister::X13 => write!(f, "x13"),
            ReadWriteRegister::X14 => write!(f, "x14"),
            ReadWriteRegister::X15 => write!(f, "x15"),
            ReadWriteRegister::X16 => write!(f, "x16"),
            ReadWriteRegister::StackPointer => write!(f, "sp"),
        }
    }
}

impl fmt::Display for WriteOnlyRegister {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "")
        // match self {
        //     WriteOnlyRegister::StackPointer => write!(f, "sp")
        // }
    }
}

impl fmt::Display for ReadOnlyRegister {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ReadOnlyRegister::Zero => write!(f, "zero"),
            ReadOnlyRegister::Full => write!(f, "full")
        }
    }
}

impl fmt::Display for SpecialRegister {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            SpecialRegister::ProgramCounter => write!(f, "pc")
        }
    }
}

impl fmt::Display for WritableRegister {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            WritableRegister::ReadWrite(rw) => rw.fmt(f),
            WritableRegister::Write(w)      => w.fmt(f)
        }
    }
}

impl fmt::Display for ReadableRegister {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ReadableRegister::ReadWrite(rw) => rw.fmt(f),
            ReadableRegister::Read(r)       => r.fmt(f)
        }
    }
}

impl fmt::Display for Register {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Register::ReadWrite(rw)     => rw.fmt(f),
            Register::Read(r)           => r.fmt(f),
            Register::Write(w)          => w.fmt(f),
            Register::Special(s)        => s.fmt(f)
        }
    }
}