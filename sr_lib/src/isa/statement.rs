/*
 * File: statement.rs
 * Project: isa
 * Created Date: Thursday March 28th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 15th April 2019 3:40:48 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum Statement<I: Instruction> {
    UnresolvedInstruction(UnresolvedInstruction),
    ResolvedInstruction(I),
    Label(Label),
    PseudoInstruction(PseudoInstruction)
}

impl<I: Instruction> Statement<I> {
    
    pub fn branch(cond: Condition, add: PreresolutionAddress<I::A>, r1: ReadableRegister, r2: ReadableRegister) -> Statement<I> {
        match add {
            PreresolutionAddress::Resolved(a) => Statement::ResolvedInstruction(I::branch(cond, a, r1, r2)),
            PreresolutionAddress::Unresolved(label) => Statement::UnresolvedInstruction(UnresolvedInstruction::UnresolvedConditionalBranch(cond, label, r1, r2))
        }
    }

    pub fn jump(add: PreresolutionAddress<I::A>) -> Statement<I> {
        match add {
            PreresolutionAddress::Resolved(a) => Statement::ResolvedInstruction(I::jump(a)),
            PreresolutionAddress::Unresolved(label) => Statement::UnresolvedInstruction(UnresolvedInstruction::UnresolvedJump(label))
        }
    }

    pub fn call(add: PreresolutionAddress<I::A>) -> Statement<I> {
        match add {
            PreresolutionAddress::Resolved(a) => Statement::ResolvedInstruction(I::call(a)),
            PreresolutionAddress::Unresolved(label) => Statement::UnresolvedInstruction(UnresolvedInstruction::UnresolvedCall(label))
        }
    }
}

impl<I: Instruction> From<Label> for Statement<I> {

    fn from(l: Label) -> Self {
        Statement::Label(l)
    }

}

impl<I: Instruction> From<PseudoInstruction> for Statement<I> {
    fn from(pi: PseudoInstruction) -> Self {
        Statement::PseudoInstruction(pi)
    }
}


impl<I: Instruction> From<UnresolvedInstruction> for Statement<I> {
    fn from(i: UnresolvedInstruction) -> Self {
        Statement::UnresolvedInstruction(i)
    }

}

impl<I: Instruction> From<I> for Statement<I> {

    fn from(i: I) -> Self {
        Statement::ResolvedInstruction(i)
    }

}