/*
 * File: address.rs
 * Project: isa
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 17th April 2019 10:38:34 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use std::fmt;
use std::hash::Hash;
use std::collections::BTreeMap;
use std::ops::Add;

use crate::isa::*;

pub trait CanAddress: Clone + Copy + PartialEq + Eq + PartialOrd + Ord + Hash + fmt::Debug + Sync + Send + Default + fmt::LowerHex + Add<Output = Self> {}

pub trait Address: CanAddress {
    fn next(&self) -> Self;
    fn inc(&mut self);
}

impl CanAddress for u64 {}
impl CanAddress for u32 {}
impl CanAddress for u16 {}
impl CanAddress for u8 {}
impl CanAddress for i64 {}
impl CanAddress for i32 {}
impl CanAddress for i16 {}
impl CanAddress for i8 {}

impl Address for u64 {
    fn inc(&mut self) {
        *self += 1;
    }

    fn next(&self) -> u64 {
        self + 1
    }
}

#[derive(Clone,Copy,Hash,PartialOrd,Ord,PartialEq,Eq,Default)]
pub struct InstructionAddress<A: Address>(pub A);

impl<A: Address> Add for InstructionAddress<A> {
    type Output = InstructionAddress<A>;

    fn add(self, other: InstructionAddress<A>) -> InstructionAddress<A> {
        InstructionAddress(self.0 + other.0)
    }
}

impl<A: Address> CanAddress for InstructionAddress<A> {}

impl<A: Address> Address for InstructionAddress<A> {
    fn next(&self) -> Self {
        InstructionAddress(self.0.next())
    }

    fn inc(&mut self) {
        self.0.inc();
    }
}

impl<A: Address> fmt::LowerHex for InstructionAddress<A> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "i@0x{:x}", self.0) // delegate to i32's implementation
    }
}

impl<A: Address> fmt::Debug for InstructionAddress<A> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::LowerHex::fmt(self, f)
    }
}

#[derive(Clone,Copy,Hash,PartialOrd,Ord,PartialEq,Eq,Default)]
pub struct DataAddress<A: Address>(pub A);

impl<A: Address> Add for DataAddress<A> {
    type Output = DataAddress<A>;

    fn add(self, other: DataAddress<A>) -> DataAddress<A> {
        DataAddress(self.0 + other.0)
    }
}

impl<A: Address> CanAddress for DataAddress<A> {}

impl<A: Address> Address for DataAddress<A> {
    fn next(&self) -> Self {
        DataAddress(self.0.next())
    }

    fn inc(&mut self) {
        self.0.inc();
    }
}

impl<A: Address> fmt::LowerHex for DataAddress<A> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "d@0x{:x}", self.0) // delegate to i32's implementation
    }
}

impl<A: Address> fmt::Debug for DataAddress<A> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::LowerHex::fmt(self, f)
    }
}


#[derive(Debug,Clone,Hash,PartialOrd,Ord,PartialEq,Eq)]
pub struct Label {
    pub name: String,
    pub global: bool
}

impl From<&str> for Label {
    fn from(s: &str) -> Label {
        Label { name: s.to_owned(), global: false }
    }
}

impl Label {

    pub fn new(global: bool, name: &str) -> Label {
        Label { name: name.to_owned(), global }
    }

    pub fn resolve_inst<A: Address>(&self, ledger: &BTreeMap<String, InstructionAddress<A>>) -> Result<InstructionAddress<A>, ResolutionError> {
        ledger.get(&self.name)
                .ok_or(ResolutionError::LabelCannotBeResolved(self.name.to_owned()))
                .and_then(|a|Ok(*a))
    }
}

#[derive(Debug,Clone,Hash,PartialOrd,Ord,PartialEq,Eq)]
pub enum PreresolutionAddress<A: Address> {
    Resolved(InstructionAddress<A>),
    Unresolved(Label)
}