/*
 * File: arithmetic_operation.rs
 * Project: isa
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 29th March 2019 2:39:29 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;
use std::str::FromStr;
use std::ops::*;
use std::fmt;

pub trait ArithmeticCapable: Sized + Add<Output = Self> + Sub<Output = Self> + BitAnd<Output = Self>
                            + BitOr<Output = Self> + BitXor<Output = Self> + Mul<Output = Self> + Div<Output = Self> 
                            + Rem<Output = Self> + Shl<Output = Self> + Shr<Output = Self> {}

impl<T: Add<Output = T> + Sub<Output = T> + BitAnd<Output = T>
                            + BitOr<Output = T> + BitXor<Output = T> + Mul<Output = T> + Div<Output = T> 
                            + Rem<Output = T> + Shl<Output = T> + Shr<Output = T> > ArithmeticCapable for T {}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum ArithmeticOperation {
    Add,
    Substract,
    Multiply,
    Divide,
    Remainder,
    ExclusiveOr,
    Or,
    And,
    // ShiftLeftLogical,
    ShiftLeftArithmetic,
    // ShiftRightLogical,
    ShiftRightArithmetic
}

impl FromStr for ArithmeticOperation {
    type Err = ISAError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "add"   | "+"       => Ok(ArithmeticOperation::Add),
            "sub"   | "-"       => Ok(ArithmeticOperation::Substract),
            "xor"   | "^"       => Ok(ArithmeticOperation::ExclusiveOr),
            "and"   | "&"       => Ok(ArithmeticOperation::And),
            "or"    | "|"       => Ok(ArithmeticOperation::Or),
            "mul"   | "*"       => Ok(ArithmeticOperation::Multiply),
            "div"   | "/"       => Ok(ArithmeticOperation::Divide),
            "rem"   | "%"       => Ok(ArithmeticOperation::Remainder),
            "sla"   | "<<"      => Ok(ArithmeticOperation::ShiftLeftArithmetic),
            "sra"   | ">>"      => Ok(ArithmeticOperation::ShiftRightArithmetic),
            _ => Err(ISAError::ArithmeticOperationParseError(s.to_owned()))
        }
    }
}

impl fmt::Display for ArithmeticOperation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ArithmeticOperation::Add                    => write!(f, "add"),
            ArithmeticOperation::Substract              => write!(f, "sub"),
            ArithmeticOperation::ExclusiveOr            => write!(f, "xor"),
            ArithmeticOperation::And                    => write!(f, "and"),
            ArithmeticOperation::Or                     => write!(f, "or"),
            ArithmeticOperation::Multiply               => write!(f, "mul"),
            ArithmeticOperation::Divide                 => write!(f, "div"),
            ArithmeticOperation::Remainder              => write!(f, "rem"),
            ArithmeticOperation::ShiftLeftArithmetic    => write!(f, "sla"),
            ArithmeticOperation::ShiftRightArithmetic   => write!(f, "sra"),
        }
    }
}