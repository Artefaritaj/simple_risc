/*
 * File: condition.r
 * Project: isa
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 21st May 2019 11:38:11 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */
use crate::isa::*;
use std::str::FromStr;
use std::fmt;

pub trait MayEq {
    fn may_eq(&self, other: &Self) -> Option<bool>;
}

impl MayEq for u64 {
    fn may_eq(&self, other: &Self) -> Option<bool> {
        Some(self == other)
    }
}



pub trait ConditionCapable: MayEq + PartialOrd {}

impl<T: MayEq + PartialOrd> ConditionCapable for T {}

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum Condition {
    Equal,
    NotEqual,
    StrictlyGreater,
    GreaterOrEqual,
    StrictlyLower,
    LowerOrEqual
}


impl FromStr for Condition {
    type Err = ISAError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "beq"   | "=="      => Ok(Condition::Equal),
            "bneq"  | "!="      => Ok(Condition::NotEqual),
            "bg"    | ">"       => Ok(Condition::StrictlyGreater),
            "bge"   | ">="      => Ok(Condition::GreaterOrEqual),
            "bl"    | "<"       => Ok(Condition::StrictlyLower),
            "ble"   | "<="      => Ok(Condition::LowerOrEqual),
            _ => Err(ISAError::ConditionParseError(s.to_owned()))
        }
    }
}


impl fmt::Display for Condition {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Condition::Equal            => write!(f, "beq"),
            Condition::NotEqual         => write!(f, "bneq"),
            Condition::StrictlyGreater  => write!(f, "bg"),
            Condition::GreaterOrEqual   => write!(f, "bge"),
            Condition::StrictlyLower    => write!(f, "bl"),
            Condition::LowerOrEqual     => write!(f, "ble"),
        }
    }
}