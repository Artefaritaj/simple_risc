/*
 * File: pseudo.rs
 * Project: isa
 * Created Date: Monday April 15th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 15th April 2019 4:14:02 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */
use crate::isa::*;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum PseudoInstruction {
    Push(Vec<ReadableRegister>),
    Pop(Vec<WritableRegister>)
}

