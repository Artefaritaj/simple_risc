/*
 * File: memory.rs
 * Project: machine
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 21st May 2019 2:17:22 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;

use std::collections::BTreeMap;
use std::fmt;

//A = Address type
//M = Memory data type


pub trait MemoryRead<A: CanAddress, M> {
    fn read(&self, add: A) -> M;
}

pub trait MemoryWrite<A: CanAddress, M> {
    fn write(&mut self, add: A, val: M);
}

pub trait MemoryReadWrite<A: CanAddress, M> : MemoryRead<A, M> + MemoryWrite<A, M> {}

impl<A: CanAddress, M, T: MemoryRead<A, M> + MemoryWrite<A, M>> MemoryReadWrite<A, M> for T {}

#[derive(Debug,Clone,Hash,PartialEq,Eq)]
pub struct Memory<A: CanAddress, M: CanMemoryData> {
    pub content: BTreeMap<A, M>
}

impl<A: CanAddress, M: CanMemoryData> MemoryRead<A, M> for Memory<A, M> {
    fn read(&self, add: A) -> M {
        match self.content.get(&add) {
            Some(val) => *val,
            None => M::default()
        }
    }
}

impl<A: CanAddress, M: CanMemoryData> MemoryWrite<A, M> for Memory<A, M> {
    fn write(&mut self, add: A, val: M) {
        self.content.insert(add, val);
    }
} 

impl<A: CanAddress, M: CanMemoryData> Memory<A, M> {
    pub fn new() -> Memory<A, M> {
        Memory { content: BTreeMap::new() }
    }

    pub fn clear(&mut self) {
        self.content.clear();
    }

    pub fn wordcount_used(&self) -> u64 {
        self.content.len() as u64
    }
}

impl<A: CanAddress + fmt::LowerHex, M: CanMemoryData + fmt::LowerHex> fmt::Display for Memory<A, M> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Memory:\n")?;
        for (add, val) in self.content.iter() {
            write!(f, "\t{:x}: 0x{:x}\n", add, val)?;
        }
        Ok(())
    }
}


