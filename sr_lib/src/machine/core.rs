/*
 * File: core.rs
 * Project: machine
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 21st May 2019 3:27:27 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

// use std::collections::BTreeMap;
use std::fmt;
use std::convert::{ TryFrom, TryInto };

use failure::{Error, format_err };

use crate::machine::*;
use crate::isa::*;
use crate::program::*;
use crate::program::static_analysis::*;

//A = Address type
//M = Memory data type 
//R = Register value type

#[derive(Clone)]
pub struct Core<I: Instruction> 
where   rand::distributions::Standard: rand::distributions::Distribution<I::R>,
        I::A: TryFrom<I::R>,
        I::M: fmt::LowerHex {

    pub data_memory: Memory<DataAddress<I::A>, I::M>,
    pub instruction_memory: Memory<InstructionAddress<I::A>, I>,
    pub state: State<I>,
    pub monitor: Monitor<I>
}

impl<I: Instruction + 'static> Core<I>
where   rand::distributions::Standard: rand::distributions::Distribution<I::R>,
        I::A: TryFrom<I::R>,
        I::M: fmt::LowerHex {

    pub fn new() -> Core<I> {
        Core { 
            data_memory: Memory::new(),
            instruction_memory: Memory::new(),
            state: State::new(),
            monitor: Monitor::new()
        }
    }

    pub fn load_program<PI: Instruction + Into<I>>(&mut self, program: Program<PI>)
    where   I::R: From<PI::A>,
            InstructionAddress<I::A>: From<InstructionAddress<PI::A>> {
        //clear last program
        self.instruction_memory.clear();

        //set PC to entry point
        self.state.write_reg(SpecialRegister::ProgramCounter.into(), program.entry.0.into());
        
        //fill with new program
        for (a, i) in program.content {
            self.instruction_memory.write(a.into(), i.into());
        }
    }

    pub fn init_cfg(&mut self, cfg: &CFG<I::A>) {
        self.monitor.init_cfg(&cfg);
    }

    pub fn fetch(&self) -> Result<I, Error> {
        let pc: I::A = self.state.read_reg(Register::program_counter()).try_into()    .map_err(|_|format_err!("Cannot fetch because register cannot be converted to address."))?;
        let inst = self.instruction_memory.read(InstructionAddress(pc));
        Ok(inst)
    }

    pub fn execute_one_instruction(&mut self) -> Result<ApplicationEffect, Error> {
        let current_add: I::A = self.state.read_reg(Register::program_counter()).try_into()    .map_err(|_|format_err!("Cannot fetch because register cannot be converted to address."))?;
        let inst = self.fetch()?;
        trace!("@0x{:x}\t\t{}", current_add, inst);
        let (data_mem, state, monitor) = (&mut self.data_memory, &mut self.state, &mut self.monitor);
        
        let app_effect = inst.apply(state, data_mem, Some(monitor))?;
        trace!("*****\n {}\n {}", state, data_mem);

        let successor: I::A = self.state.read_reg(Register::program_counter()).try_into()    .map_err(|_|format_err!("Cannot fetch because register cannot be converted to address."))?;
        monitor.register_instruction_access(InstructionAddress(current_add))?;

        if app_effect.must_halt == false {
            monitor.register_instruction_transition(InstructionAddress(current_add), InstructionAddress(successor))?;
        }

        Ok(app_effect)
    }

    pub fn execute_many_instructions(&mut self, n: u64) -> Result<u64, Error> {
        for inst_counter in 0..n {
            let app_effect = self.execute_one_instruction()?;

            if app_effect.must_halt == true {
                return Ok(inst_counter+1)
            }
        }
        Ok(n)
    }

    pub fn run(&mut self) -> Result<u64, Error> {
        let mut app_effect = ApplicationEffect::default();
        let mut inst_counter = 0;

        while app_effect.must_halt != true {
            app_effect = self.execute_one_instruction()?;
            inst_counter += 1;
        }

        Ok(inst_counter)
    }

} 

#[test]
fn test_core_simple_v2() {
    let mut core: Core<InstructionV2<u64, u64, u64>> = Core::new();

    let inst_list: Vec<InstructionV2<u64, u64, u64>> = vec![
        InstructionV2::LoadImmediate(ReadWriteRegister::X1.into(), 0xFFFFFF),
        InstructionV2::DirectStore(DataAddress(0x5), ReadWriteRegister::X1.into())
        ];


    let prog = ObjectFile::instruction_list_to_program(inst_list);
    core.load_program(prog);

    // println!("0: ****\n {}\n{}", core.state, core.data_memory);
    // println!("Executing {}", core.fetch());

    for _i in 1..3 {
        core.execute_one_instruction().unwrap();
        // println!("{}: ****\n {}\n{}", i, core.state, core.data_memory);
        // println!("Executing {}", core.fetch());
    }
    
}