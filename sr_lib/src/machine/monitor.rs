/*
 * File: monitor.rs
 * Project: machine
 * Created Date: Thursday April 18th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 2nd August 2019 4:14:46 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;
// use crate::program::*;
use crate::program::static_analysis::*;

use failure::Error;

use std::fmt;
use std::collections::HashMap;
use std::iter::FromIterator;

#[derive(Clone)]
pub struct Monitor<I: Instruction> {
    precise_instruction_counts: HashMap<I, u64>,
    intruction_type_counts: HashMap<InstructionType, u64>,
    total_instruction_count: u64,
    cfg_tracer: Option<GraphTracer<InstructionAddress<I::A>>>
}

impl<I: Instruction> Monitor<I> {
    pub fn new() -> Monitor<I> {
        Monitor {
            precise_instruction_counts: HashMap::new(),
            intruction_type_counts: HashMap::new(),
            total_instruction_count: 0u64,
            cfg_tracer: None
        }
    }

    pub fn init_cfg(&mut self, cfg: &CFG<I::A>) {
        self.cfg_tracer = Some(GraphTracer::from_graph(&cfg.graph));
    }

    pub fn register_instruction(&mut self, i: &I) {
        let previous_precise: u64 = match self.precise_instruction_counts.remove(i) {
            None => 0,
            Some(i) => i
        };

        self.precise_instruction_counts.insert(*i, previous_precise + 1);

        let previous_type: u64 = match self.intruction_type_counts.remove(&i.instruction_type()) {
            None => 0,
            Some(i) => i
        };

        self.intruction_type_counts.insert(i.instruction_type(), previous_type + 1);

        self.total_instruction_count += 1;
    }

    pub fn register_instruction_access(&mut self, add: InstructionAddress<I::A>) -> Result<(), Error> {
        match self.cfg_tracer {
            Some(ref mut tracer) => tracer.trace_node(&add),
            None => Ok(())
        }
    }

    pub fn register_instruction_transition(&mut self, predecessor: InstructionAddress<I::A>, successor: InstructionAddress<I::A>) -> Result<(), Error> {
        match self.cfg_tracer {
            Some(ref mut tracer) => tracer.trace_edge(&predecessor, &successor),
            None => Ok(())
        }
    }
}

impl<I: Instruction> fmt::Display for Monitor<I> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Monitor:\n")?;
        write!(f, "\t{} instructions executed.\n", self.total_instruction_count)?;

        let mut itc_vec = Vec::from_iter(self.intruction_type_counts.iter());
        itc_vec.sort_by(|&(_, a), &(_, b)| b.cmp(&a));

        write!(f, "\tInstruction type counts:\n")?;
        for (i, count) in itc_vec {
            write!(f, "\t\t{:?}:\t{}\n", i, count)?;
        }

        let mut pic_vec = Vec::from_iter(self.precise_instruction_counts.iter());
        pic_vec.sort_by(|&(_, a), &(_, b)| b.cmp(&a));

        write!(f, "\tPrecise instruction counts:\n")?;
        for (i, count) in pic_vec {
            write!(f, "\t\t{}:\t{}\n", i, count)?;
        }

        if let Some(ref tracer) = self.cfg_tracer {
            write!(f, "\tCFG:\n")?;
            let node_count = tracer.node_count();
            write!(f, "\t\tInstructions: {}\n", node_count)?;
            let edge_count = tracer.edge_count();
            write!(f, "\t\tTransitions: {}\n", edge_count)?;
            let av_fanout: f64 = edge_count as f64 / node_count as f64;
            write!(f, "\t\tAverage fanout: {}\n\n", av_fanout)?;

            write!(f, "\t\tVisited instructions ratio: {}\n", tracer.visited_node_ratio())?;
            write!(f, "\t\tVisited transitions ratio: {}\n", tracer.visited_edge_ratio())?;
        }
        

        Ok(())
    }
}