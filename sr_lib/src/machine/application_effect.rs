/*
 * File: application_effect.rs
 * Project: inst_ops
 * Created Date: Friday March 22nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 5th April 2019 9:29:31 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

#[derive(Debug,Clone)]
pub struct ApplicationEffect {
    pub program_counter_set: bool,
    pub must_halt: bool
}

impl Default for ApplicationEffect {
    fn default() -> ApplicationEffect {
        ApplicationEffect { 
            program_counter_set: false,
            must_halt: false
        }
    }
}

impl ApplicationEffect {
    pub fn program_counter_is_set(mut self) -> ApplicationEffect {
        self.program_counter_set = true;
        self
    }

    pub fn must_halt(mut self) -> ApplicationEffect {
        self.must_halt = true;
        self
    }
}