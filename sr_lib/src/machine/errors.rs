/*
 * File: errors.rs
 * Project: machine
 * Created Date: Friday March 22nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 21st May 2019 3:16:20 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;

use failure::Fail;

#[derive(Debug,Fail)]
pub enum MachineError<I: Instruction + 'static> {
    #[fail(display = "Instruction {:?} not implemented yet.", _0)]
    UnimplementedInstruction(I),
    
    #[fail(display = "Address 0x{:x} is unreachable.", _0)]
    UnreachableAddress(I::A),

    #[fail(display = "Address 0x{:x} is out of bounds.", _0)]
    OutOfBoundsAccess(I::A),

    #[fail(display = "Action {} is undecidable.", _0)]
    UndecidableAction(String),

    #[fail(display = "No conversion possible")]
    NoConvert
}

