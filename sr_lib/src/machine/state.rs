/*
 * File: state.rs
 * Project: machine
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 9th May 2019 3:08:56 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;
use crate::machine::*;

use std::collections::BTreeMap;
use std::fmt;

//A = Address type
//M = Memory data type 
//R = Register value type



#[derive(Debug,Clone,Hash,PartialEq,Eq)]
pub struct State<I: Instruction> {
    pub registers: BTreeMap<Register, I::R>,
    pub return_stack: ReturnStack<I::A>,
}

impl<I: Instruction> RegisterMachine<I::A, I::R> for State<I> {
    fn read_reg(&self, reg: Register) -> I::R {
        match self.registers.get(&reg) {
            Some(val) => *val,
            None => I::R::default()
        }
    }

    fn write_reg(&mut self, reg: Register, val: I::R) {
        self.registers.insert(reg, val);
    }

    fn push_to_ret_stack(&mut self, add: InstructionAddress<I::A>) {
        self.return_stack.push(add);
    }

    fn pop_from_ret_stack(&mut self) -> InstructionAddress<I::A> {
        self.return_stack.pop()
    }
}

impl<I: Instruction> State<I> {

    pub fn new() -> State<I> {
        State { 
            registers: BTreeMap::new(), 
            return_stack: ReturnStack::new(),
        }
    }
}

impl<I: Instruction> fmt::Display for State<I>
where I::R: fmt::LowerHex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "State:\n")?;
        for (reg, val) in self.registers.iter() {
            write!(f, "\t{:?} = 0x{:x}\n", reg, val)?;
        }
        Ok(())
    }
}