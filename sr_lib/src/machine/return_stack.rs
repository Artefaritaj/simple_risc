/*
 * File: return_stack.rs
 * Project: machine
 * Created Date: Tuesday March 26th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 2nd August 2019 4:15:13 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;

use std::hash::*;
use std::fmt;

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct ReturnStack<A>
where A: Address {
    stack: Vec<InstructionAddress<A>>
}

impl<A: Address> ReturnStack<A> {
    pub fn push(&mut self, add: InstructionAddress<A>) {
        self.stack.push(add);
    }

    pub fn pop(&mut self) -> InstructionAddress<A> {
        self.stack.pop().unwrap_or(InstructionAddress(A::default()))
    }

    pub fn new() -> ReturnStack<A> {
        ReturnStack { stack: Vec::new() }
    }
}

impl<A: Address> fmt::LowerHex for ReturnStack<A> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ReturnStack: ")?;
        for add in self.stack.iter() {
            write!(f, "\t{:x},", add)?;
        }
        Ok(())
    }
}



impl<A: Address> Hash for ReturnStack<A> {
    fn hash<H: Hasher>(&self, state: &mut H) {

        //deal with (complicated) recursions: A | A, A B | A B, A B C | A B C, ...
        //compute [max] max stack level to take into account (marked by | above)
        let mut max = self.stack.len();
        
        if max > 1 {
            
            let stack_size = self.stack.len();
            let mut pattern_size = 1;
            while pattern_size * 2 <= stack_size {
                //detect pattern repetition of size [pattern_size]
                if self.stack[(stack_size - pattern_size) .. stack_size] == self.stack[(stack_size - 2*pattern_size) .. (stack_size - pattern_size)] {
                    //recursion found
                    max -= pattern_size;
                    break;
                }
                else {
                    pattern_size += 1;
                }
            } 
        }
        

        for i in 0..max {
           /* if self.stack[i] == self.stack[i-1] { //this special condition allows to deal with recursive calls: if two successive calls, then hash is identical and CFG exploration stopg
                // println!("Recursive call detected !");
                break;
            }
            else {*/
                self.stack[i].hash(state);
            //}
        }
    }
}
