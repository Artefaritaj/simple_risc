/*
 * File: register_machine.rs
 * Project: machine
 * Created Date: Thursday May 9th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 9th May 2019 3:08:46 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;

pub trait RegisterMachine<A: Address, R> {
    fn read_reg(&self, reg: Register) -> R;
    fn write_reg(&mut self, reg: Register, val: R);
    fn push_to_ret_stack(&mut self, add: InstructionAddress<A>);
    fn pop_from_ret_stack(&mut self) -> InstructionAddress<A>;
}
