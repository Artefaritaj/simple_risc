/*
 * File: non_deterministic.rs
 * Project: inst_ops
 * Created Date: Friday March 29th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 9th May 2019 3:19:29 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */
use crate::machine::*;
use crate::isa::*;

pub fn apply_nd<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, reg: WritableRegister )
-> Result<ApplicationEffect, MachineError<I>> 
where   rand::distributions::Standard: rand::distributions::Distribution<I::R>, {
    
    //jump to correct address
    state.write_reg(reg.into(), rand::random::<I::R>());

    Ok(ApplicationEffect::default())
}