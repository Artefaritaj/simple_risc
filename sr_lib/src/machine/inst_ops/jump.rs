/*
 * File: jump.rs
 * Project: inst_ops
 * Created Date: Tuesday March 26th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 21st May 2019 3:05:26 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::machine::*;
use crate::isa::*;

pub fn apply_direct_jump<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, jump_to: InstructionAddress<I::A> )
-> Result<ApplicationEffect, MachineError<I>>  {
    
    //jump to correct address
    state.write_reg(Register::program_counter(), jump_to.0.into());

    Ok(ApplicationEffect::default().program_counter_is_set()) 
}

pub fn apply_indirect_jump<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, addreg: ReadableRegister) 
-> Result<ApplicationEffect, MachineError<I>> {

    let add = state.read_reg(addreg.into());
    state.write_reg(Register::program_counter(), add.into());
    
    Ok(ApplicationEffect::default().program_counter_is_set())
}