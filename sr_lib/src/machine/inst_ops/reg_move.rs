/*
 * File: reg_move.rs
 * Project: inst_ops
 * Created Date: Tuesday March 26th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 9th May 2019 3:19:43 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::machine::*;
use crate::isa::*;

pub fn apply_move<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, dest: WritableRegister, source: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    
    let val = state.read_reg(source.into());
    state.write_reg(dest.into(), val);

    Ok(ApplicationEffect::default()) 
}