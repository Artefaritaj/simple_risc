/*
 * File: call_ret.rs
 * Project: inst_ops
 * Created Date: Tuesday March 26th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 21st May 2019 3:20:32 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::machine::*;
use crate::isa::*;

use std::convert::{ TryFrom, TryInto };

pub fn apply_call<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, jump_to: InstructionAddress<I::A> )
-> Result<ApplicationEffect, MachineError<I>>
where I::A: TryFrom<I::R> {
    
    //store successor to return stack
    let pc: I::A = state.read_reg(Register::program_counter()).try_into()   .map_err(|_|MachineError::NoConvert)?;
    state.push_to_ret_stack(InstructionAddress(pc.next()));
    
    //jump to correct address
    state.write_reg(Register::program_counter(), jump_to.0.into());

    Ok(ApplicationEffect::default().program_counter_is_set())
}

pub fn apply_call_x14<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, jump_to: InstructionAddress<I::A> )
-> Result<ApplicationEffect, MachineError<I>>
where I::A: TryFrom<I::R> {
    
    //store successor to return stack
    let pc: I::A = state.read_reg(Register::program_counter()).try_into()   .map_err(|_|MachineError::NoConvert)?;
    state.write_reg(Register::x14(), pc.next().into());
    
    //jump to correct address
    state.write_reg(Register::program_counter(), jump_to.0.into());

    Ok(ApplicationEffect::default().program_counter_is_set())
}

pub fn apply_ret<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM)
-> Result<ApplicationEffect, MachineError<I>> {
    
    //get successor in return stack
    let jump_to = state.pop_from_ret_stack();
    
    //jump to correct address
    state.write_reg(Register::program_counter(), jump_to.0.into());

    Ok(ApplicationEffect::default().program_counter_is_set()) 
}
