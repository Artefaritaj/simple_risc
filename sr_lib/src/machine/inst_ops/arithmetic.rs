/*
 * File: arithmetic.rs
 * Project: inst_ops
 * Created Date: Monday March 25th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 9th May 2019 3:22:33 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::machine::*;
use crate::isa::*;

pub fn apply_arithmetic_op<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, op: ArithmeticOperation, dest: WritableRegister, source1: ReadableRegister, source2: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    match op {
        ArithmeticOperation::Add                    => apply_add(state, dest, source1, source2),
        ArithmeticOperation::Substract              => apply_sub(state, dest, source1, source2),
        ArithmeticOperation::ExclusiveOr            => apply_xor(state, dest, source1, source2),
        ArithmeticOperation::And                    => apply_and(state, dest, source1, source2),
        ArithmeticOperation::Or                     => apply_or(state, dest, source1, source2),
        ArithmeticOperation::Divide                 => apply_div(state, dest, source1, source2),
        ArithmeticOperation::Multiply               => apply_mul(state, dest, source1, source2),
        ArithmeticOperation::Remainder              => apply_rem(state, dest, source1, source2),
        ArithmeticOperation::ShiftLeftArithmetic    => apply_sla(state, dest, source1, source2),
        ArithmeticOperation::ShiftRightArithmetic   => apply_sra(state, dest, source1, source2),

        // _ => Err(MachineError::UnimplementedInstruction(Instruction::IntegerArithmetic(op, dest, source1, source2)))
    }
}

fn apply_add<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, dest: WritableRegister, source1: ReadableRegister, source2: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    
    state.write_reg(dest.into(), state.read_reg(source1.into()) + state.read_reg(source2.into()));

    Ok(ApplicationEffect::default())
}

fn apply_sub<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, dest: WritableRegister, source1: ReadableRegister, source2: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    
    state.write_reg(dest.into(), state.read_reg(source1.into()) - state.read_reg(source2.into()));

    Ok(ApplicationEffect::default())
}

fn apply_xor<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, dest: WritableRegister, source1: ReadableRegister, source2: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    
    state.write_reg(dest.into(), state.read_reg(source1.into()) ^ state.read_reg(source2.into()));

    Ok(ApplicationEffect::default())
}

fn apply_or<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, dest: WritableRegister, source1: ReadableRegister, source2: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    
    state.write_reg(dest.into(), state.read_reg(source1.into()) | state.read_reg(source2.into()));

    Ok(ApplicationEffect::default())
}

fn apply_and<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, dest: WritableRegister, source1: ReadableRegister, source2: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    
    state.write_reg(dest.into(), state.read_reg(source1.into()) & state.read_reg(source2.into()));

    Ok(ApplicationEffect::default())
}

fn apply_mul<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, dest: WritableRegister, source1: ReadableRegister, source2: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    
    state.write_reg(dest.into(), state.read_reg(source1.into()) * state.read_reg(source2.into()));

    Ok(ApplicationEffect::default())
}

fn apply_div<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, dest: WritableRegister, source1: ReadableRegister, source2: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    
    state.write_reg(dest.into(), state.read_reg(source1.into()) / state.read_reg(source2.into()));

    Ok(ApplicationEffect::default())
}

fn apply_rem<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, dest: WritableRegister, source1: ReadableRegister, source2: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    
    state.write_reg(dest.into(), state.read_reg(source1.into()) % state.read_reg(source2.into()));

    Ok(ApplicationEffect::default())
}

fn apply_sla<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, dest: WritableRegister, source1: ReadableRegister, source2: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    
    state.write_reg(dest.into(), state.read_reg(source1.into()) << state.read_reg(source2.into()));

    Ok(ApplicationEffect::default())
}

fn apply_sra<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, dest: WritableRegister, source1: ReadableRegister, source2: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    
    state.write_reg(dest.into(), state.read_reg(source1.into()) >> state.read_reg(source2.into()));

    Ok(ApplicationEffect::default())
}