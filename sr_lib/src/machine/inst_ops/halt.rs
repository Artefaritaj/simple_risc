/*
 * File: nop.rs
 * Project: inst_ops
 * Created Date: Tuesday March 26th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 26th April 2019 10:01:21 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::machine::*;
use crate::isa::*;

pub fn apply_halt<I: Instruction>()
-> Result<ApplicationEffect, MachineError<I>> {
    Ok(ApplicationEffect::default().must_halt())
}