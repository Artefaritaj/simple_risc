/*
 * File: load.rs
 * Project: inst_ops
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 21st May 2019 3:17:49 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::machine::*;
use crate::isa::*;

use std::convert::{ TryFrom, TryInto };

/// Load data from memory into a register
pub fn dload_app<I: Instruction, RM, MEM>(state: &mut RM, mem: &mut MEM, reg: WritableRegister, add: DataAddress<I::A>) 
-> Result<ApplicationEffect, MachineError<I>>
where   RM: RegisterMachine<I::A, I::R>,
        MEM: MemoryReadWrite<DataAddress<I::A>, I::M> {
            
    let val: I::M = mem.read(add);
    state.write_reg(reg.into(), val.into());
    
    Ok(ApplicationEffect::default()) 
}

/// Store data from register into memory
pub fn dstore_app<I: Instruction, RM, MEM>(state: &mut RM, mem: &mut MEM, reg: ReadableRegister, add: DataAddress<I::A>)
-> Result<ApplicationEffect, MachineError<I>>
where   I::M: From<I::R>,
        RM: RegisterMachine<I::A, I::R>,
        MEM: MemoryReadWrite<DataAddress<I::A>, I::M> {

    let val: I::R = state.read_reg(reg.into());
    mem.write(add, val.into());
    
    Ok(ApplicationEffect::default()) 
}

/// Load data from memory at address in source into a register
pub fn iload_app<I: Instruction, RM, MEM>(state: &mut RM, mem: &mut MEM, dest: WritableRegister, add_reg: ReadableRegister) 
-> Result<ApplicationEffect, MachineError<I>>
where   I::A: TryFrom<I::R>,
        RM: RegisterMachine<I::A, I::R>,
        MEM: MemoryReadWrite<DataAddress<I::A>, I::M> {

    let add: I::A = state.read_reg(add_reg.into()).try_into()   .map_err(|_|MachineError::NoConvert)?;
            
    let val: I::M = mem.read(DataAddress(add));
    state.write_reg(dest.into(), val.into());
    
    Ok(ApplicationEffect::default()) 
}

/// Store data from register into memory at address in register
pub fn istore_app<I: Instruction, RM, MEM>(state: &mut RM, mem: &mut MEM, val_reg: ReadableRegister, add_reg: ReadableRegister)
-> Result<ApplicationEffect, MachineError<I>>
where   I::A: TryFrom<I::R>,
        I::M: From<I::R>,
        RM: RegisterMachine<I::A, I::R>,
        MEM: MemoryReadWrite<DataAddress<I::A>, I::M> {

    let add: I::A = state.read_reg(add_reg.into()).try_into()   .map_err(|_|MachineError::NoConvert)?;
    let val: I::R = state.read_reg(val_reg.into());
    mem.write(DataAddress(add), val.into());
    
    Ok(ApplicationEffect::default()) 
}