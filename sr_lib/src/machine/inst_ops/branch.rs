/*
 * File: branch.rs
 * Project: inst_ops
 * Created Date: Monday March 25th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 21st May 2019 4:47:37 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::machine::*;
use crate::isa::*;

use std::cmp::Ordering;

pub fn apply_branch<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, cond: Condition, jump_to: InstructionAddress<I::A>, source1: ReadableRegister, source2: ReadableRegister )
-> Result<ApplicationEffect, MachineError<I>> {
    
    let val1 = state.read_reg(source1.into());
    let val2 = state.read_reg(source2.into());

    

    let condition_met_possibility = match cond {
        Condition::Equal                => val1.may_eq(&val2),
        Condition::NotEqual             => val1.may_eq(&val2).map(|b|!b),
        Condition::StrictlyGreater      => val1.partial_cmp(&val2).map(|o| o == Ordering::Greater),
        Condition::StrictlyLower        => val1.partial_cmp(&val2).map(|o| o == Ordering::Less),
        Condition::GreaterOrEqual       => val1.partial_cmp(&val2).map(|o| o == Ordering::Greater || o == Ordering::Equal),
        Condition::LowerOrEqual         => val1.partial_cmp(&val2).map(|o| o == Ordering::Less || o == Ordering::Equal),
        
        // _ => None

    };

    // debug!("{:x} {} {:x} => {:?}", val1, cond, val2, condition_met_possibility);

    // let condition_met = match cond {
    //     Condition::Equal            => val1 == val2,
    //     Condition::NotEqual         => val1 != val2,
    //     Condition::GreaterOrEqual   => val1 >= val2,
    //     Condition::LowerOrEqual     => val1 <= val2,
    //     Condition::StrictlyGreater  => val1 >  val2,
    //     Condition::StrictlyLower    => val1 <  val2
    // };

    if let Some(condition_met) = condition_met_possibility {
        if condition_met == true {
            state.write_reg(Register::program_counter(), jump_to.0.into());

            Ok(ApplicationEffect::default().program_counter_is_set())
        }
        else {
            //PC will be incremented globally
            Ok(ApplicationEffect::default())
        }    
    }
    else {
        Err(MachineError::UndecidableAction(format!("{:x} {} {:x}", val1, cond, val2)))
    }
}
