/*
 * File: immediate.rs
 * Project: inst_ops
 * Created Date: Friday March 22nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 9th May 2019 3:20:11 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::machine::*;
use crate::isa::*;

/// Load a literal into a register (allow/only way to embed data in program)
pub fn load_immediate<I: Instruction, RM: RegisterMachine<I::A, I::R>>(state: &mut RM, reg: WritableRegister, imm: I::R) 
-> Result<ApplicationEffect, MachineError<I>> {
            
    state.write_reg(reg.into(), imm.into());

    Ok(ApplicationEffect::default())
}