/*
 * File: mod.rs
 * Project: inst_ops
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 26th April 2019 9:59:02 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

pub mod transfer;
pub mod immediate;
pub mod arithmetic;
pub mod branch;
pub mod call_ret;
pub mod jump;
pub mod reg_move;
pub mod halt;
pub mod non_deterministic;