/*
 * File: object_file.rs
 * Project: src
 * Created Date: Monday April 15th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 19th April 2019 10:07:58 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use failure::Error;

use crate::isa::*;
use crate::program::*;
use std::collections::BTreeMap;

pub struct ObjectFile<I: Instruction> {
    pub global_labels: BTreeMap<String, InstructionAddress<I::A>>,
    pub local_labels: BTreeMap<String, InstructionAddress<I::A>>,
    pub unresolved: BTreeMap<InstructionAddress<I::A>, UnresolvedInstruction>,
    pub resolved: BTreeMap<InstructionAddress<I::A>, I>,
    pub end_address: InstructionAddress<I::A>
}

impl<I: Instruction> ObjectFile<I> {
    pub fn new(file_statements: Vec<Statement<I>>) -> ObjectFile<I> {
        let mut global_labels: BTreeMap<String, InstructionAddress<I::A>> = BTreeMap::new();
        let mut local_labels: BTreeMap<String, InstructionAddress<I::A>> = BTreeMap::new();
        let mut resolved: BTreeMap<InstructionAddress<I::A>, I> = BTreeMap::new();
        let mut unresolved: BTreeMap<InstructionAddress<I::A>, UnresolvedInstruction> = BTreeMap::new();

        let mut address = InstructionAddress(I::A::default());

        for statement in file_statements.into_iter() {
            // debug!("0x{:x}\t\t{:?}", address, statement);
            match statement {
                Statement::Label(Label { name, global }) => {
                    match global {
                        true => { global_labels.insert(name, address); },
                        false => { local_labels.insert(name, address); }
                    }
                },
                Statement::ResolvedInstruction(i) => {
                    resolved.insert(address, i);
                    address.inc();
                },
                Statement::UnresolvedInstruction(i) => {
                    unresolved.insert(address, i);
                    address.inc();
                },
                Statement::PseudoInstruction(pi) => {
                    //generate equivalent instructions
                    let gadget = I::develop(pi);

                    for inst in gadget.into_iter() {
                        resolved.insert(address, inst);
                        address.inc();
                    }
                }
            }
        }

        ObjectFile { global_labels, local_labels, unresolved, resolved, end_address: address }
    }

     pub fn instruction_list_to_program(inst_list: Vec<I>) -> Program<I> {
        let mut prog: Program<I> = Program::new();
        let mut add: I::A = I::A::default();

        for inst in inst_list.iter() {
            prog.content.insert(InstructionAddress(add), *inst);
            add.inc();
        }

        prog
    }

    pub fn link(objs: &Vec<Self>) -> Result<Program<I>, Error> {
        //1rst pass: build global ledger from global labels
        let mut global_ledger: BTreeMap<String, InstructionAddress<I::A>> = BTreeMap::new();
        let mut address: InstructionAddress<I::A> = InstructionAddress(I::A::default());

        for obj in objs.iter() {
            for (name, local_add) in obj.global_labels.iter() {
                global_ledger.insert(name.to_owned(), *local_add + address);
            }
            // debug!("Current address = 0x{:x}, End address = 0x{:x}", address, obj.end_address);
            address = address + obj.end_address;
        }
        debug!("Global ledger: {:?}", global_ledger);

        //2nd: for each file, resolve all instructions
        let mut prog: Program<I> = Program::new();
        address = InstructionAddress(I::A::default());

        for obj in objs.iter() {
            //append (and offset addresses) resolved
            let mut offset_resolved: BTreeMap<InstructionAddress<I::A>, I> = obj.resolved.iter().map(|(local_add, inst)| (*local_add + address, *inst)).collect();
            prog.content.append(&mut offset_resolved);

            //build local_ledger = local_labels + global_labels
            let mut local_ledger: BTreeMap<String, InstructionAddress<I::A>> = BTreeMap::new();
            local_ledger.append(&mut global_ledger.clone());

            //offset local addresses
            let mut new_local_labels = obj.local_labels.iter().map(|(name, local_add)| (name.to_owned(), *local_add + address)).collect();
            debug!("Local ledger: {:?}", new_local_labels);
            local_ledger.append(&mut new_local_labels);

            //resolve unresolved and add to prog
            for (local_add, to_resolve) in obj.unresolved.iter() {
                let inst: I = I::resolve(to_resolve.clone(), &local_ledger)?;
                prog.content.insert(*local_add + address, inst);
            }

            address = address + obj.end_address;
        }

        //look for entry
        if let Some(entry_add) = global_ledger.get("entry") {
            prog.entry = *entry_add;
            info!("Entry found at {:x}", entry_add.0);
        }

        Ok(prog)
    }
}

