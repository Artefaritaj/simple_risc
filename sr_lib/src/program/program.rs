/*
 * File: program.rs
 * Project: machine
 * Created Date: Tuesday April 16th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 25th April 2019 10:29:38 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use std::fmt;
use crate::isa::*;
// use crate::machine::*;
// use crate::program::*;

use std::collections::BTreeMap;

#[derive(Debug,Clone)]
pub struct Program<I: Instruction> {
    pub content: BTreeMap<InstructionAddress<I::A>, I>,
    pub entry: InstructionAddress<I::A>
}

impl<I: Instruction> Program<I> {
    pub fn new() -> Program<I> {
        Program { content: BTreeMap::new(), entry: InstructionAddress(I::A::default()) }
    }
}

impl<I: Instruction> fmt::Display for Program<I> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Entry is 0x{:x}\n", self.entry.0)?;

        for (add, inst) in self.content.iter() {
            write!(f, "0x{:x}\t\t{}\n", add, inst)?;
        }
        Ok(())
    }
}
