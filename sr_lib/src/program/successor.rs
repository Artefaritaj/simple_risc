/*
 * File: successor.rs
 * Project: program
 * Created Date: Friday April 19th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 9th May 2019 10:58:43 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum Successor<A: Address> {
    None,
    All,
    Next,
    DirectJump(InstructionAddress<A>),
    Call(InstructionAddress<A>),
    Return,
    Register(ReadableRegister)
}