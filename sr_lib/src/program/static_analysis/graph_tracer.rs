/*
 * File: graph.rs
 * Project: program
 * Created Date: Thursday April 25th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 9th May 2019 11:59:31 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use failure::{Error, bail};

// use crate::program::*;
use crate::program::static_analysis::*;

use std::fmt;
use std::collections::{BTreeMap};


#[derive(Debug,Clone)]
pub struct GraphTracer<N>
where   N: fmt::Debug + Copy + Ord + Eq + fmt::LowerHex {
    pub nodes: BTreeMap<N, u64>,
    pub edges: BTreeMap<N, BTreeMap<N, u64>>
}

impl<N> Default for GraphTracer<N>
where   N: fmt::Debug + Copy + Ord + Eq + fmt::LowerHex {
    fn default() -> GraphTracer<N> {
        GraphTracer {
            nodes: BTreeMap::new(),
            edges: BTreeMap::new()
        }
    }
} 

impl<N> GraphTracer<N>
where   N: fmt::Debug + Copy + Ord + Eq + fmt::LowerHex{

    pub fn from_graph(g: &Graph<N>) -> GraphTracer<N> {
        let mut tracer = GraphTracer::default();
        for node in g.nodes.iter() {
            tracer.add_node(node.clone());
        }

        for (p, smap) in g.edges.iter() {
            let hm = smap.iter().map(|s|(s.clone(), 0)).collect();
            tracer.edges.insert(p.clone(), hm);
        }
        
        tracer
    }

    pub fn add_node(&mut self, node_value: N) {
        self.nodes.insert(node_value, 0);
    }


    pub fn add_edge(&mut self, predecessor: N, successor: N) {
        if self.nodes.contains_key(&predecessor) == false {
            self.nodes.insert(predecessor, 0);
        }

        if self.nodes.contains_key(&successor) == false {
            self.nodes.insert(successor, 0);
        }

        let mut previous = match self.edges.remove(&predecessor) {
            Some(p) => p,
            None => BTreeMap::new()
        };

        previous.insert(successor, 0);
        self.edges.insert(predecessor, previous);
    }

    pub fn node_count(&self) -> usize {
        self.nodes.len()
    }

    pub fn trace_node(&mut self, node: &N) -> Result<(), Error> {
        match self.nodes.get_mut(node) {
            Some(ref mut t) => { **t += 1; },
            None            => { bail!("Tracing node not in graph: {:x}.", node); }
        }

        Ok(())
    }

    pub fn trace_edge(&mut self, predecessor: &N, successor: &N) -> Result<(), Error> {
        match self.edges.get_mut(predecessor) {
            Some(ref mut map) => {
                match map.get_mut(successor) {
                    Some(ref mut tracer) => {
                        **tracer += 1;
                    },
                    None => { bail!("Tracing edge where successor is not in graph: {:x} -> {:x}.", predecessor, successor); }
                }
            },
            None => { bail!("Tracing edge where predecessor is not in graph: {:x} -> {:x}.", predecessor, successor); }
        }

        Ok(())
    }

    pub fn edge_count(&self) -> usize {
        let mut tot = 0;
        for (_, map) in self.edges.iter() {
            tot += map.len();
        }
        tot
    }

    pub fn contains_node(&self, node: &N) -> bool {
        self.nodes.contains_key(&node)
    }

    pub fn visited_node_ratio(&self) -> f64 {
        let mut visited_count = 0;
        for (_, visits) in self.nodes.iter() {
            if *visits > 0 {
                visited_count += 1;
            }
        }

        visited_count as f64 / self.node_count() as f64
    }

    pub fn visited_edge_ratio(&self) -> f64 {
        let mut visited_count = 0;
        let mut tot = 0;
        for (_, map) in self.edges.iter() {
            tot += map.len();
            for (_, visits) in map.iter() {
                if *visits > 0 {
                    visited_count += 1;
                }
            }
        }

        visited_count as f64 / tot as f64
    }
}