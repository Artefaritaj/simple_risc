/*
 * File: cfg.rs
 * Project: machine
 * Created Date: Friday April 19th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 13th June 2019 3:51:15 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;
use crate::program::*;
use crate::program::static_analysis::*;
use crate::machine::*;

use failure::{ Error, format_err };

use std::collections::{HashSet};
use std::collections::hash_map::DefaultHasher;
use std::hash::*;
use std::fmt;
use std::convert::{ TryFrom, TryInto };
// use rand::distributions::{Distribution, Standard};

#[derive(Debug,Clone)]
pub struct CFG<A: Address> {
    pub graph: Graph<InstructionAddress<A>>,
}

impl<A: Address> CFG<A> {
    pub fn new() -> CFG<A> {
        CFG { 
            graph: Graph::default(),
        }
    }

    pub fn node_count(&self) -> usize {
        self.graph.node_count()
    }

    pub fn edge_count(&self) -> usize {
        self.graph.edge_count()
    }
}

fn hash_state_memory<I: Instruction>(state: &State<I>, mem:  &Memory<DataAddress<I::A>, I::M>) -> u64 {
    let mut hasher = DefaultHasher::new();
    state.hash(&mut hasher);
    mem.hash(&mut hasher);
    hasher.finish()
}

pub fn determinism_tracing<I: Instruction + 'static, PI: Instruction>(prog: Program<PI>) -> Result<CFG<I::A>, Error>
where   PI: Into<I>,
        I::A: TryFrom<I::R>,
        I::M: fmt::LowerHex,
        rand::distributions::Standard: rand::distributions::Distribution<I::R>,
        I::R: From<PI::A>,
        InstructionAddress<I::A>: From<InstructionAddress<PI::A>> {
    
    let mut core: Core<I> = Core::new();
    core.load_program(prog);

    let mut cfg: CFG<I::A> = CFG::new();
    
    let mut bigstates_buffer: Vec<(State<I>, Memory<DataAddress<I::A>, I::M>)> = Vec::new();
    let mut analyzed: HashSet<u64> = HashSet::new();
    bigstates_buffer.push((core.state.clone(), core.data_memory.clone()));

    while bigstates_buffer.is_empty() == false {
        let (mut state, mut mem) = bigstates_buffer.pop().unwrap();//cannot fail

        let pc: I::A = state.read_reg(Register::program_counter()).try_into()   .map_err(|_|format_err!("Cannot convert register to address."))?;
        cfg.graph.add_node(InstructionAddress(pc));
        let inst = core.instruction_memory.read(InstructionAddress(pc.into()));
        
        debug!("0x{:x}: {}", pc, inst);

        let next_pcs: Vec<I::A> = match inst.apply(&mut state, &mut mem, None) {//execute instruction
            Ok(app_effect) => {//instruction executed with success -> add new state to buffer
                if app_effect.must_halt == false {
                    let successor = state.read_reg(Register::program_counter());
                    match successor.try_into() {
                        Ok(add) => vec![add],
                        Err(_) => {
                            //cannot convert reg to address -> jump to non determinist register
                            
                            let mut successors_pc: Vec<I::A> = Vec::new();
                            for (add, _) in core.instruction_memory.content.iter() {
                                successors_pc.push(add.0.into());
                            }
                            successors_pc
                        }
                    }
                }
                else {//halt
                    Vec::new()
                }
            },
            Err(me) => {
                match me {
                    MachineError::UndecidableAction(_) => {//branching undecidable -> keep state but PC -> explore all successors
                        debug!("UndecidableAction, state is\n{}\n{}", state, mem);
                        let successors = inst.successors();
                        let mut successors_pc: Vec<I::A> = Vec::new();
                        for successor in successors.iter() {
                            match successor {
                                Successor::Next => {
                                    let mut pc: I::A = state.read_reg(Register::program_counter()).try_into()   .map_err(|_|format_err!("Cannot convert register to address (code 2)."))?;
                                    pc.inc();
                                    successors_pc.push(pc.into());
                                },
                                Successor::DirectJump(add) => {
                                    successors_pc.push(add.0.into());
                                },
                                Successor::All => {
                                    for (add, _) in core.instruction_memory.content.iter() {
                                        successors_pc.push(add.0.into());
                                    }
                                },
                                s => { 
                                    debug!("Successor {:?} is not handled in case of undecidable action", s); 
                                }
                            }
                        }
                        successors_pc
                    },
                    e => {return Err(e.into()); }
                }
            }

            
        };

        //create new state for each next PC
        for new_pc in next_pcs {
            cfg.graph.add_edge(InstructionAddress(pc.into()), InstructionAddress(new_pc.into()));
            let mut new_state = state.clone();
            new_state.write_reg(Register::program_counter(), new_pc.into());

            

            //add new state to buffer
            let state_hash = hash_state_memory(&new_state, &mem);
            // debug!("New successor 0x{:x} with hash {:x}", new_pc, state_hash);
            if analyzed.contains(&state_hash) == false {
                bigstates_buffer.push((new_state, mem.clone()));
                analyzed.insert(state_hash);
            }
            else {
                // debug!("State {} has already been analyzed.", state_hash);
            }
        }
        
    }

    Ok(cfg)
}


pub fn callstack_tracking<I: Instruction>(prog: Program<I>) -> CFG<I::A> {
    let mut cfg: CFG<I::A> = CFG::new();

    //get the entry instruction
    let entry_add = prog.entry;
    let mut addresses_buffer: Vec< (InstructionAddress<I::A>, Option<ReturnStack<I::A>>) > = vec![(entry_add, Some(ReturnStack::new())) ];
    let mut analyzed: HashSet<u64> = HashSet::new();

    while addresses_buffer.is_empty() == false {
        let (current_address, current_return_stack) = addresses_buffer.pop().unwrap();//cannot fail
        
        //compute hash = signature of what we analyze
        let mut hasher = DefaultHasher::new();
        current_address.hash(&mut hasher);
        current_return_stack.hash(&mut hasher);
        let analyzed_hash = hasher.finish();
        //add this signature to history set
        analyzed.insert(analyzed_hash);

        //now we can go on

        //get successors for new_address
        let successors = nonrecursive_successor_analysis(&prog, current_address, current_return_stack);
        debug!("New address: {:x}", current_address);
        //add edges to cfg
        for (successor_add, new_rstack) in successors.iter() {
            cfg.graph.add_edge(current_address, *successor_add);
            if let Some(rstack) = new_rstack {
                debug!("-> {:x} ({:x})", successor_add, rstack);
            } 
            else {
                debug!("-> {:x}", successor_add);
            }
            
        }

        //filter already analyzed addresses
        let mut valid_successors: Vec< (InstructionAddress<I::A>, Option<ReturnStack<I::A>>) > = successors.into_iter().filter(|(add, rstack)|
        {
            //compute hash
            let mut hasher = DefaultHasher::new();
            add.hash(&mut hasher);
            rstack.hash(&mut hasher);
            let final_hash = hasher.finish();

            !analyzed.contains(&final_hash)
        }).collect();

        addresses_buffer.append(&mut valid_successors);
    }


    cfg
}

pub fn structural<I: Instruction>(prog: Program<I>) -> CFG<I::A> 
{
    let mut cfg: CFG<I::A> = CFG::new();

    //set one node per instruction
    // for (add, _) in prog.content.iter() {
    //     cfg.add_instruction(*add);
    // }

    //get the entry instruction
    let entry_add = prog.entry;
    let mut addresses_buffer = vec![entry_add];
    let mut analyzed: HashSet<InstructionAddress<I::A>> = HashSet::new();

    //start recursivity at entry
    // recursive_structural_analysis(&mut cfg, &prog, entry_add);
    while addresses_buffer.is_empty() == false {
        let current_address = addresses_buffer.pop().unwrap();//cannot fail
        analyzed.insert(current_address);
        
        //get successors for new_address
        let successors = nonrecursive_successor_analysis(&prog, current_address, None);

        debug!("New address: {:x}", current_address);
        //add edges to cfg
        for (successor_add, _) in successors.iter() {
            cfg.graph.add_edge(current_address, *successor_add);
            debug!("-> {:x}", successor_add);
        }

        //filter already analyzed addresses
        let mut valid_successors: Vec<InstructionAddress<I::A>> = successors.into_iter().filter(|(x, _)|!analyzed.contains(x)).map(|(x, _)|x).collect();

        // for valid in valid_successors.iter() {
        //     debug!("{:x} is valid", valid);
        // }

        addresses_buffer.append(&mut valid_successors);
    }
    
    cfg
}

fn nonrecursive_successor_analysis<I: Instruction>(prog: &Program<I>, address: InstructionAddress<I::A>, return_stack_opt: Option<ReturnStack<I::A>>) 
-> HashSet<(InstructionAddress<I::A>, Option<ReturnStack<I::A>>)> 
/*where   rand::distributions::Standard: rand::distributions::Distribution<I::R>,
        I::A: From<I::R>,
        I::M: fmt::LowerHex*/ {
    let successors = match prog.content.get(&address) {
        Some(inst)  => inst.successors(),
        None        => btreeset![Successor::None]
    };

    let mut successor_addresses = HashSet::new();

    //foreach successor
    for successor in successors.iter() {
        //create edge in cfg
        //then recursive structural analysis
        match successor {
            Successor::Next => {
                let next = address.next();
                successor_addresses.insert((next, return_stack_opt.clone()));
            },
            Successor::None => {},
            Successor::All | Successor::Register(_) /*| Successor::Return*/ => {
                for (new_add, _) in prog.content.iter() {
                    successor_addresses.insert((*new_add, return_stack_opt.clone()));
                }
            },
            Successor::DirectJump(new_add) => {
                successor_addresses.insert((*new_add, return_stack_opt.clone()));
            },
            Successor::Call(new_add) => {
                let mut return_add = address;
                return_add.inc();
                let mut new_rstack = return_stack_opt.clone();
                if let Some(ref mut rstack) = new_rstack {
                    rstack.push(return_add);
                }

                successor_addresses.insert((*new_add, new_rstack));

            },
            // Successor::Register(_reg) => {
            //     panic!("Register analyzis requires machine state.");
            // },
            Successor::Return => {
                if let Some(ref return_stack) = return_stack_opt {
                    let mut new_stack = return_stack.clone();
                    let return_add = new_stack.pop();
                    successor_addresses.insert((return_add, Some(new_stack)));
                }
                else {
                    panic!("Return analyzis requires return stack ({:x}).", address)
                }
            }
        }
    }

    successor_addresses
}




#[test]
fn test_simple() {
    use std::marker::PhantomData;

    let inst_list: Vec<InstructionV2<u64, u64, u64>> = vec![
        InstructionV2::LoadImmediate(ReadWriteRegister::X1.into(), 0xFFFFFF),
        InstructionV2::DirectStore(DataAddress(0x5), ReadWriteRegister::X1.into()),
        InstructionV2::Halt(PhantomData) // implicit even if not present
        ];


    let prog: Program<InstructionV2<u64, u64, u64>> = ObjectFile::instruction_list_to_program(inst_list);

    let cfg: CFG<u64> = structural(prog);

    assert_eq!(cfg.graph.node_count(), 3);
    assert_eq!(cfg.graph.edge_count(), 2);
}

// #[test]
// fn test_determinism_tracing_v2() {
//     type DTInstV2 = InstructionV2<u64, DeterminismTracer<u64>, DeterminismTracer<u64>>;

//     let inst_list: Vec<InstructionV2<u64, u64, u64>> = vec![
//         InstructionV2::NonDeterministic(ReadWriteRegister::X1.into()),
//         InstructionV2::LoadImmediate(ReadWriteRegister::X2.into(), 0xFFFFFFFF),
//         InstructionV2::IntegerArithmetic(ArithmeticOperation::ExclusiveOr, ReadWriteRegister::X3.into(), ReadWriteRegister::X1.into(), ReadWriteRegister::X2.into()),
//         InstructionV2::DirectStore(DataAddress(0x5), ReadWriteRegister::X3.into()),
//     ];


//     let prog = ObjectFile::instruction_list_to_program(inst_list);
    
//     let _cfg = determinism_tracing::<DTInstV2, InstructionV2<u64, u64, u64>>(prog);
//     // println!("{:?}", cfg);
// }

#[test]
fn test_determinism_tracing_v1() {
    use std::marker::PhantomData;

    type DTInstV1 = InstructionV1<u64, DeterminismTracer<u64>, DeterminismTracer<u64>>;

    let inst_list: Vec<InstructionV1<u64, u64, u64>> = vec![
        InstructionV1::NonDeterministic(ReadWriteRegister::X1.into()),
        InstructionV1::LoadImmediate(ReadWriteRegister::X2.into(), 0xFFFFFFFF),
        InstructionV1::IntegerArithmetic(ArithmeticOperation::ExclusiveOr, ReadWriteRegister::X3.into(), ReadWriteRegister::X1.into(), ReadWriteRegister::X2.into()),
        InstructionV1::DirectStore(DataAddress(0x5), ReadWriteRegister::X3.into()),
        InstructionV1::IndirectJump(ReadWriteRegister::X3.into()),
        // InstructionV1::ConditionalBranch(Condition::Equal, InstructionAddress(0x1), ReadWriteRegister::X2.into(), ReadWriteRegister::X3.into()),
        InstructionV1::Halt(PhantomData),
    ];


    let prog = ObjectFile::instruction_list_to_program(inst_list);
    
    let _cfg = determinism_tracing::<DTInstV1, InstructionV1<u64, u64, u64>>(prog);
    // println!("{:?}", cfg);
}