/*
 * File: graph.rs
 * Project: program
 * Created Date: Thursday April 25th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 25th April 2019 3:24:32 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

// use failure::{Error, bail};

use std::fmt;
use std::collections::{BTreeSet, BTreeMap};

#[derive(Debug,Clone)]
pub struct Graph<N>
where N: fmt::Debug + Copy + Ord + Eq + fmt::LowerHex {
    pub nodes: BTreeSet<N>,
    pub edges: BTreeMap<N, BTreeSet<N>>
}

impl<N> Default for Graph<N> 
where N: fmt::Debug + Copy + Ord + Eq + fmt::LowerHex {
    fn default() -> Graph<N> {
        Graph {
            nodes: BTreeSet::new(),
            edges: BTreeMap::new()
        }
    }
} 

impl<N> Graph<N>
where N: fmt::Debug + Copy + Ord + Eq + fmt::LowerHex {
    pub fn add_node(&mut self, node_value: N) {
        self.nodes.insert(node_value);
    }


    pub fn add_edge(&mut self, predecessor: N, successor: N) {
        if self.nodes.contains(&predecessor) == false {
            self.nodes.insert(predecessor);
        }

        if self.nodes.contains(&successor) == false {
            self.nodes.insert(successor);
        }

        let mut previous = match self.edges.remove(&predecessor) {
            Some(p) => p,
            None    => BTreeSet::new()
        };

        previous.insert(successor);

        self.edges.insert(predecessor, previous);
    }

    pub fn node_count(&self) -> usize {
        self.nodes.len()
    }

    pub fn edge_count(&self) -> usize {
        self.edges.len()
    }

    pub fn contains_node(&self, node: &N) -> bool {
        self.nodes.contains(&node)
    }
}