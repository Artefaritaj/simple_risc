/*
 * File: core.rs
 * Project: machine
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 21st May 2019 9:47:01 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use std::collections::{
    BTreeMap, 
    HashSet,
    hash_map::DefaultHasher
    };
use std::fmt;
use std::hash::{Hash, Hasher};

use failure::{Error, format_err};

use crate::machine::*;
use crate::isa::*;
use crate::program::*;
use crate::program::static_analysis::*;

//A = Address type
//M = Memory data type 
//R = Register value type


/// The static core is used for static execution: all determinist states will be computed
#[derive(Clone)]
pub struct StaticCore<I: Instruction> 
where   rand::distributions::Standard: rand::distributions::Distribution<I::R>,
        I::A: From<I::R>,
        I::M: fmt::LowerHex {

    // pub data_memory: Memory<DataAddress<I::A>, I::M>,
    pub instruction_memory: Memory<InstructionAddress<I::A>, I>,
    // pub state: State<I>,
    // pub states: BTreeMap<
    //                 InstructionAddress<I::A>, 
    //                 HashSet<u64>//set of hashes of state + mem
    //             >
    
}



impl<I: Instruction + 'static> StaticCore<I>
where   rand::distributions::Standard: rand::distributions::Distribution<I::R>,
        I::A: From<I::R>,
        I::M: fmt::LowerHex {

    fn hash_state_memory(state: State<I>, mem:  Memory<DataAddress<I::A>, I::M>) -> u64 {
        let mut hasher = DefaultHasher::new();
        state.hash(&mut hasher);
        mem.hash(&mut hasher);
        hasher.finish()
    }

    pub fn new() -> StaticCore<I> {
        StaticCore { 
            instruction_memory: Memory::new(),
            // states: BTreeMap::new()
        }
    }

    pub fn analyze<PI: Instruction + Into<I>>(&mut self, program: Program<PI>)
    where   I::R: From<PI::A>,
            InstructionAddress<I::A>: From<InstructionAddress<PI::A>> {
        //clear last program
        self.instruction_memory.clear();

        //fill with new program
        for (a, i) in program.content {
            self.instruction_memory.write(a.into(), i.into());
        }

        //init analysis

        let mut state: StaticState<I> = StaticState::new();

        //set PC to entry point
        state.write_reg(SpecialRegister::ProgramCounter.into(), DeterminismTracer::Deterministic(program.entry.0.into()));
        
        let mut analyzed_states: BTreeMap<InstructionAddress<I::A>, HashSet<u64>> = BTreeMap::new(); //hashes of states
        

        
    }

    

    // pub fn fetch(state: &State<I>, instruction_memory: &Memory<InstructionAddress<I::A>, I>) -> I {
    //     let pc = state.read_reg(Register::program_counter());
    //     let inst = instruction_memory.read(InstructionAddress(pc.into()));
    //     inst
    // }

    pub fn nondeterministic_execute_one_instruction(inst: I, state: &mut StaticState<I>, data_mem: &mut Memory<DataAddress<I::A>, DeterminismTracer<I::M>>)
    -> Result<ApplicationEffect, Error> {
        
        let current_add: I::A = match state.read_reg(Register::program_counter()) {
            DeterminismTracer::NonDeterministic => { return Err(format_err!("Non deterministic program counter")); },
            DeterminismTracer::Deterministic(val) => val.into()
        };

        let app_effect = inst.apply(state, data_mem, None)?;

        unimplemented!()
    }

    // pub fn execute_one_instruction(inst: I, state: &mut State<I>, data_mem: &mut Memory<DataAddress<I::A>, I::M>) -> Result<ApplicationEffect, Error> {
    //     let current_add: I::A = state.read_reg(Register::program_counter()).into();
    //     // trace!("@0x{:x}\t\t{}", current_add, inst);
        
    //     let app_effect = inst.apply(state, data_mem, None)?;
    //     // trace!("*****\n {}\n {}", state, data_mem);

    //     let successor: I::A = state.read_reg(Register::program_counter()).into();
    //     // monitor.register_instruction_access(InstructionAddress(current_add))?;

    //     // if app_effect.must_halt == false {
    //     //     monitor.register_instruction_transition(InstructionAddress(current_add), InstructionAddress(successor))?;
    //     // }

    //     Ok(app_effect)
    // }


} 
