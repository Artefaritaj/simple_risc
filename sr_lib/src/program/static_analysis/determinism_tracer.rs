/*
 * File: determinism_tracer.rs
 * Project: program
 * Created Date: Friday April 19th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 22nd May 2019 8:58:03 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::isa::*;

use std::fmt;
use std::ops;
use std::hash::{Hash, Hasher};
use std::cmp::Ordering;
use std::convert::TryFrom;

use failure::format_err;

#[derive(Debug,Clone,Copy)]
pub enum DeterminismTracer<V> {
    Deterministic(V),
    NonDeterministic
}

impl<V> From<V> for DeterminismTracer<V> {
    fn from(v: V) -> DeterminismTracer<V> {
        DeterminismTracer::Deterministic(v)
    }
}


// fn deterministic(v: V) -> DeterminismTracer<V> {
//     DeterminismTracer::Deterministic(v)
// }

// fn non_deterministic() -> DeterminismTracer<V> {
//     DeterminismTracer::NonDeterministic
// }

impl<V: Default> Default for DeterminismTracer<V> {
    fn default() -> Self {
        // DeterminismTracer::NonDeterministic
        DeterminismTracer::Deterministic(V::default())
    }
}

impl<V: fmt::LowerHex> fmt::LowerHex for DeterminismTracer<V> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            DeterminismTracer::Deterministic(val) => write!(f, "{:x}", val),
            DeterminismTracer::NonDeterministic => write!(f, "XXXX")
        }
    }
}

// pub trait ArithmeticCapable: Sized + Add<Output = Self> + Sub<Output = Self> + BitAnd<Output = Self>
//                             + BitOr<Output = Self> + BitXor<Output = Self> + Mul<Output = Self> + Div<Output = Self> 
//                             + Rem<Output = Self> + Shl<Output = Self> + Shr<Output = Self> {}

// pub trait ConditionCapable: Eq + Ord {}

impl<V: ops::Add<Output = V>> ops::Add for DeterminismTracer<V> {
    type Output = Self;
    fn add(self, rhs: Self) -> Self {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match rhs {
                    DeterminismTracer::Deterministic(rhsval) => DeterminismTracer::Deterministic(selfval + rhsval),
                    _ => DeterminismTracer::NonDeterministic
                }
            },
            DeterminismTracer::NonDeterministic => DeterminismTracer::NonDeterministic
        }
    }
}

impl<V: ops::Sub<Output = V>> ops::Sub for DeterminismTracer<V> {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match rhs {
                    DeterminismTracer::Deterministic(rhsval) => DeterminismTracer::Deterministic(selfval - rhsval),
                    _ => DeterminismTracer::NonDeterministic
                }
            },
            DeterminismTracer::NonDeterministic => DeterminismTracer::NonDeterministic
        }
    }
}

impl<V: ops::BitAnd<Output = V>> ops::BitAnd for DeterminismTracer<V> {
    type Output = Self;
    fn bitand(self, rhs: Self) -> Self {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match rhs {
                    DeterminismTracer::Deterministic(rhsval) => DeterminismTracer::Deterministic(selfval & rhsval),
                    _ => DeterminismTracer::NonDeterministic
                }
            },
            DeterminismTracer::NonDeterministic => DeterminismTracer::NonDeterministic
        }
    }
}

impl<V: ops::BitOr<Output = V>> ops::BitOr for DeterminismTracer<V> {
    type Output = Self;
    fn bitor(self, rhs: Self) -> Self {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match rhs {
                    DeterminismTracer::Deterministic(rhsval) => DeterminismTracer::Deterministic(selfval | rhsval),
                    _ => DeterminismTracer::NonDeterministic
                }
            },
            DeterminismTracer::NonDeterministic => DeterminismTracer::NonDeterministic
        }
    }
}

impl<V: ops::BitXor<Output = V>> ops::BitXor for DeterminismTracer<V> {
    type Output = Self;
    fn bitxor(self, rhs: Self) -> Self {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match rhs {
                    DeterminismTracer::Deterministic(rhsval) => DeterminismTracer::Deterministic(selfval ^ rhsval),
                    _ => DeterminismTracer::NonDeterministic
                }
            },
            DeterminismTracer::NonDeterministic => DeterminismTracer::NonDeterministic
        }
    }
}

impl<V: ops::Mul<Output = V>> ops::Mul for DeterminismTracer<V> {
    type Output = Self;
    fn mul(self, rhs: Self) -> Self {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match rhs {
                    DeterminismTracer::Deterministic(rhsval) => DeterminismTracer::Deterministic(selfval * rhsval),
                    _ => DeterminismTracer::NonDeterministic
                }
            },
            DeterminismTracer::NonDeterministic => DeterminismTracer::NonDeterministic
        }
    }
}

impl<V: ops::Div<Output = V>> ops::Div for DeterminismTracer<V> {
    type Output = Self;
    fn div(self, rhs: Self) -> Self {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match rhs {
                    DeterminismTracer::Deterministic(rhsval) => DeterminismTracer::Deterministic(selfval / rhsval),
                    _ => DeterminismTracer::NonDeterministic
                }
            },
            DeterminismTracer::NonDeterministic => DeterminismTracer::NonDeterministic
        }
    }
}

impl<V: ops::Rem<Output = V>> ops::Rem for DeterminismTracer<V> {
    type Output = Self;
    fn rem(self, rhs: Self) -> Self {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match rhs {
                    DeterminismTracer::Deterministic(rhsval) => DeterminismTracer::Deterministic(selfval % rhsval),
                    _ => DeterminismTracer::NonDeterministic
                }
            },
            DeterminismTracer::NonDeterministic => DeterminismTracer::NonDeterministic
        }
    }
}

impl<V: ops::Shl<Output = V>> ops::Shl for DeterminismTracer<V> {
    type Output = Self;
    fn shl(self, rhs: Self) -> Self {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match rhs {
                    DeterminismTracer::Deterministic(rhsval) => DeterminismTracer::Deterministic(selfval << rhsval),
                    _ => DeterminismTracer::NonDeterministic
                }
            },
            DeterminismTracer::NonDeterministic => DeterminismTracer::NonDeterministic
        }
    }
}

impl<V: ops::Shr<Output = V>> ops::Shr for DeterminismTracer<V> {
    type Output = Self;
    fn shr(self, rhs: Self) -> Self {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match rhs {
                    DeterminismTracer::Deterministic(rhsval) => DeterminismTracer::Deterministic(selfval >> rhsval),
                    _ => DeterminismTracer::NonDeterministic
                }
            },
            DeterminismTracer::NonDeterministic => DeterminismTracer::NonDeterministic
        }
    }
}

impl<V: PartialEq > PartialEq for DeterminismTracer<V> {
    fn eq(&self, other: &DeterminismTracer<V>) -> bool {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match other {
                    DeterminismTracer::Deterministic(rhsval) => selfval == rhsval,
                    _ => false
                }
            },
            DeterminismTracer::NonDeterministic => false
        }
    }
}
impl<V: Eq > Eq for DeterminismTracer<V> {}

impl<V: Eq > MayEq for DeterminismTracer<V> {
    fn may_eq(&self, other: &Self) -> Option<bool> {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match other {
                    DeterminismTracer::Deterministic(rhsval) => Some(selfval.eq(rhsval)),
                    _ => None
                }
            },
            DeterminismTracer::NonDeterministic => None
        }
    }
}

impl<V: Ord> Ord for DeterminismTracer<V> {
    fn cmp(&self, other: &DeterminismTracer<V>) -> Ordering {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match other {
                    DeterminismTracer::Deterministic(rhsval) => selfval.cmp(rhsval),
                    _ => Ordering::Greater
                }
            },
            DeterminismTracer::NonDeterministic => {
                match other {
                    DeterminismTracer::Deterministic(_) => Ordering::Less,
                    DeterminismTracer::NonDeterministic => Ordering::Equal
                }
            }
        }
    }
}

impl<V: PartialOrd> PartialOrd for DeterminismTracer<V> {
    fn partial_cmp(&self, other: &DeterminismTracer<V>) -> Option<Ordering> {
        match self {
            DeterminismTracer::Deterministic(selfval) => {
                match other {
                    DeterminismTracer::Deterministic(rhsval) => selfval.partial_cmp(rhsval),
                    _ => None
                }
            },
            DeterminismTracer::NonDeterministic => None
        }
    }
}

impl<V: Hash> Hash for DeterminismTracer<V> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            DeterminismTracer::Deterministic(val) => {
                "det".hash(state);
                val.hash(state);
            },
            DeterminismTracer::NonDeterministic => {
                "nondet".hash(state);
            }
        }
    }
}

// impl<V: From<u64>> From<u64> for DeterminismTracer<V> {
//     fn from(source: u64) -> DeterminismTracer<V> {
//         DeterminismTracer { val: V::from(source), deterministic: true }
//     }
// }

// impl<V> From<DeterminismTracer<V>> for u64 
// where   u64: From<V>{
//     fn from(source: DeterminismTracer<V>) -> u64 {
//         match source {
//             DeterminismTracer::Deterministic(val) => u64::from(val),
//             DeterminismTracer::NonDeterministic => 0
//         }
        
//     }
// }

impl<V> TryFrom<DeterminismTracer<V>> for u64 
where   u64: From<V> {
    type Error = failure::Error;

    fn try_from(source: DeterminismTracer<V>) -> Result<u64, Self::Error> {
        match source {
            DeterminismTracer::Deterministic(val) => Ok(u64::from(val)),
            DeterminismTracer::NonDeterministic => Err(format_err!("Cannot convert non deterministic value."))
        }
    }

}

impl<V: Address> From<InstructionAddress<V>> for InstructionAddress<DeterminismTracer<V>>
where {
    fn from(source: InstructionAddress<V>) -> InstructionAddress<DeterminismTracer<V>>  {
        InstructionAddress(DeterminismTracer::Deterministic(source.0))
    }
}

impl<V:> rand::distributions::Distribution<DeterminismTracer<V>> for rand::distributions::Standard
where rand::distributions::Standard: rand::distributions::Distribution<V> {
    fn sample<R: rand::Rng + ?Sized>(&self, _rng: &mut R) -> DeterminismTracer<V> {
        DeterminismTracer::NonDeterministic
    }
}

impl<V: CanAddress + ops::Add<Output = V>> CanAddress for DeterminismTracer<V> {}
impl<V: Address> Address for DeterminismTracer<V> {
    fn next(&self) -> Self {
        match self {
            DeterminismTracer::Deterministic(val) => DeterminismTracer::Deterministic(val.next()),
            DeterminismTracer::NonDeterministic => DeterminismTracer::NonDeterministic
        }
    }

    fn inc(&mut self) {
        match self {
            DeterminismTracer::Deterministic(ref mut val) => { val.inc(); },
            DeterminismTracer::NonDeterministic => {}
        }
    }
}



#[test]
fn test_core_dtracer() {
    use crate::machine::*;
    use crate::program::*;

    type DtU64 = DeterminismTracer<u64>;
    let mut core: Core<InstructionV2<u64, DtU64, DtU64>> = Core::new();

    let inst_list: Vec<InstructionV2<u64, DtU64, DtU64>> = vec![
        InstructionV2::LoadImmediate(ReadWriteRegister::X1.into(), DeterminismTracer::NonDeterministic),
        InstructionV2::LoadImmediate(ReadWriteRegister::X2.into(), DeterminismTracer::Deterministic(0xFFFFFFFF)),
        InstructionV2::IntegerArithmetic(ArithmeticOperation::ExclusiveOr, ReadWriteRegister::X3.into(), ReadWriteRegister::X1.into(), ReadWriteRegister::X2.into()),
        InstructionV2::DirectStore(DataAddress(0x5), ReadWriteRegister::X3.into())
        ];


    let prog = ObjectFile::instruction_list_to_program(inst_list);
    core.load_program(prog);

    // println!("0: ****\n {}\n{}", core.state, core.data_memory);
    // println!("Executing {}", core.fetch());

    for _i in 1..5 {
        core.execute_one_instruction().unwrap();
        // println!("{}: ****\n {}\n{}", i, core.state, core.data_memory);
        // println!("Executing {}", core.fetch());
    }
}