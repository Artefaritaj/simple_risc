/*
 * File: lib.rs
 * Project: src
 * Created Date: Thursday March 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 25th April 2019 11:44:54 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

// extern crate failure;
// extern crate lazy_static;

#[macro_use] extern crate log;
#[macro_use] extern crate sugar;

pub mod isa;
pub mod machine;
pub mod program;