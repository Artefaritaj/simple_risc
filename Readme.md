# A case against indirect jumps for secure programs

In this repository you will find the paper and additional material for our paper *A case against indirect jumps for secure programs*.

The repository is organized as follows:
- Papers, the source files (latex) for the paper
- sr_lib, the virtual machine as discussed in the paper as a rust library (plus algorithms for CFG extraction). 
- sr_ass, a simple assembly language for our virtual machine (grammar, parser, ...)
- sr_cli, the command line interface to launch a program on the virtual machine.

## How to test the VM

You need a valid installation of the rust programming language. Go to [rustup](https://rustup.rs/) if not.

Navigate into the **sr_cli** folder and build the project (sr_ass and sr_lib are dependencies) with:
```
cargo build --release
```

To execute a program:

``
./target/release/sr_cli --isa v1 -m examples/aes_v1/aesv1.m run
``

You can modify the selected isa by replacing **v1** with **v2** or **v3**.
The *m* file is a *meta* file, it contains a list of assembly file to load.
If you want to load a single assembly file, you can instead use *-i file.s*.

There are 3 modes of execution:
- run: to the run the program once.
- analyze: to only perform a CFG extraction.
- run_stats: Run 10,000 executions of the program and issue the number of executed instruction for each execution.