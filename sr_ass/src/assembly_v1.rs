/*
 * File: assembly.rs
 * Project: src
 * Created Date: Friday March 22nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 26th April 2019 11:09:47 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use sr_lib::isa::*;
// use sr_lib::machine::*;
use sr_lib::program::*;

use failure::Error;

use crate::grammar_v1;
use crate::errors::AssemblyError;

use std::fs;
use std::io::Read;
use std::path::Path;
use std::fmt;

type Inst64V1 = InstructionV1<u64, u64, u64>;
type Prog64V1 = Program<Inst64V1>;

pub struct AssemblyV1 {}

impl AssemblyV1 {
    //parse a string and output a vec of (potentially unresolved) statements
    pub fn parse(input_str: &str) -> Result<Vec<Statement<InstructionV1<u64, u64, u64>>>, AssemblyError> {
        grammar_v1::statement_list(input_str).map_err(|e|AssemblyError::ParsingFailed(format!("{}", e)))
    }

    pub fn parse_file<P: AsRef<Path> + fmt::Debug + Clone>(filepath: P) -> Result<Vec<Statement<InstructionV1<u64, u64, u64>>>, AssemblyError> {
        let path_display = filepath.clone();
        let mut f = fs::File::open(filepath).map_err(|e|AssemblyError::ParsingFailed(format!("Cannot load program file {:?}: {}", path_display, e)))?;
        let mut input_str = String::new();
        f.read_to_string(&mut input_str).map_err(|e|AssemblyError::ParsingFailed(format!("{}", e)))?;
        grammar_v1::statement_list(&input_str).map_err(|e|AssemblyError::ParsingFailed(format!("In {:?}: {}", path_display, e)))
    }

    pub fn compile_one(input_str: &str) -> Result<Prog64V1, Error> {
        let statements = AssemblyV1::parse(input_str)?;
        let obj_file = ObjectFile::new(statements);
        let prog = ObjectFile::link(&vec![obj_file])?;
        Ok(prog)
    }
}

#[test]
fn test_v1_parse_readwrite_register() {
    let test_vec = vec!["x1", "X2", "x16"];
    let truth = vec![ReadWriteRegister::X1, ReadWriteRegister::X2, ReadWriteRegister::X16];

    for (test, correct) in test_vec.iter().zip(truth) {
        let parsed_res = grammar_v1::readwrite_register(test);
        assert!(parsed_res.is_ok(), "Couldn't parse {}: {:?}", test, parsed_res.err());
        let parsed = parsed_res.unwrap();
        assert_eq!(parsed, correct, "Couldn't parse {} to find {:?}", test, correct);
    }
}

#[test]
fn test_v1_parse_readonly_register() {
    let test_vec = vec!["xFF", "full", "x0", "zero"];
    let truth = vec![ReadOnlyRegister::Full, ReadOnlyRegister::Full, ReadOnlyRegister::Zero, ReadOnlyRegister::Zero];

    for (test, correct) in test_vec.iter().zip(truth) {
        let parsed_res = grammar_v1::readonly_register(test);
        assert!(parsed_res.is_ok(), "Couldn't parse {}: {:?}", test, parsed_res.err());
        let parsed = parsed_res.unwrap();
        assert_eq!(parsed, correct, "Couldn't parse {} to find {:?}", test, correct);
    }
}


#[test]
fn test_v1_parse_register() {
    let test_vec = vec!["x1", "X2", "x16", "STACK_POINTER", "pc", "zero", "xFF"];
    let truth = vec![Register::x1(), Register::x2(), Register::x16(), Register::stack_pointer(), Register::program_counter(), Register::zero(), Register::full()];

    for (test, correct) in test_vec.iter().zip(truth) {
        let parsed_res = grammar_v1::register(test);
        assert!(parsed_res.is_ok(), "Couldn't parse {}: {:?}", test, parsed_res.err());
        let parsed = parsed_res.unwrap();
        assert_eq!(parsed, correct, "Couldn't parse {} to find {:?}", test, correct);
    }
}

#[test]
fn test_v1_parse_labeldef() {
    let test_vec = vec!["l1:", " l2 :", "l3 : //comment"];
    let must_fail = vec![" l2", "l3 // :"];

    for test in test_vec.iter() {
        let parsed_res = grammar_v1::label_def(test);
        assert!(parsed_res.is_ok(), "Couldn't parse {}: {:?}", test, parsed_res.err());
    }

    for failing_test in must_fail.iter() {
        let parsed_res = grammar_v1::label_def(failing_test);
        assert!(parsed_res.is_err(), "Shouldn't parse {}: {:?}", failing_test, parsed_res);
    }
}


#[test]
fn test_v1_vector_loadimm_inst() {
    let test_str = "load x1, #0xFFFFFF";

    let inst = grammar_v1::loadimm_inst(test_str).unwrap();
    let truth = InstructionV1::LoadImmediate(ReadWriteRegister::X1.into(), 0xFFFFFF);
    assert_eq!(inst, truth);
}

#[test]
fn test_v1_vector_dload_inst() {
    let test_str = "load x2, d@d5";

    let inst = grammar_v1::dload_inst(test_str).unwrap();
    let truth = InstructionV1::DirectLoad(ReadWriteRegister::X2.into(), DataAddress(5));
    assert_eq!(inst, truth);
}

#[test]
fn test_v1_vector_iload_inst() {
    let test_str = "load x2, x3";

    let inst = grammar_v1::iload_inst(test_str).unwrap();
    let truth = InstructionV1::IndirectLoad(ReadWriteRegister::X2.into(), ReadWriteRegister::X3.into());
    assert_eq!(inst, truth);
}

#[test]
fn test_v1_vector_dstore_inst() {
    let test_str = "store d@b101, x3";

    let inst = grammar_v1::dstore_inst(test_str).unwrap();
    let truth = InstructionV1::DirectStore(DataAddress(5), ReadWriteRegister::X3.into());
    assert_eq!(inst, truth);
}

#[test]
fn test_v1_vector_istore_inst() {
    let test_str = "store x5, x3";

    let inst = grammar_v1::istore_inst(test_str).unwrap();
    let truth = InstructionV1::IndirectStore(ReadWriteRegister::X5.into(), ReadWriteRegister::X3.into());
    assert_eq!(inst, truth);
}

#[test]
fn test_v1_prog1() {
    let input_str = include_str!("../test_vectors/prog1.s");
    let program_res = grammar_v1::statement_list(input_str);
    assert!(program_res.is_ok(), "Program parsing failed: {:?}", program_res.err());

    // let _prog = program_res.unwrap();

    // for (i, statement) in prog.iter().enumerate() {
        // println!("{}: {:?}", i, statement);
    // }
}

#[test]
fn test_v1_branches() {
    let test_vec = vec!["beq offset, x1, x0", "> i@0xda, x5, full"];
    let truth: Vec<Statement<InstructionV1<u64, u64, u64>>> = vec![
        Statement::branch(Condition::Equal, PreresolutionAddress::Unresolved(Label::from("offset")), ReadWriteRegister::X1.into(), ReadOnlyRegister::Zero.into()),
        Statement::branch(Condition::StrictlyGreater, PreresolutionAddress::Resolved(InstructionAddress(0xda)), ReadWriteRegister::X5.into(), ReadOnlyRegister::Full.into())

    ];

    for (test, truth) in test_vec.iter().zip(truth) {
        let parsed = grammar_v1::statement(test).unwrap();
        assert_eq!(parsed, truth);
    }
}

#[test]
fn test_v1_jump() {
    let test_vec = vec!["jump offset", "goto i@0xda"];
    let truth: Vec<Statement<InstructionV1<u64, u64, u64>>> = vec![
        Statement::jump(PreresolutionAddress::Unresolved(Label::from("offset"))),
        Statement::jump(PreresolutionAddress::Resolved(InstructionAddress(0xda)))
    ];

    for (test, truth) in test_vec.iter().zip(truth) {
        let parsed = grammar_v1::statement(test).unwrap();
        assert_eq!(parsed, truth);
    }
}

// #[test]
// fn test_v1_call_ret() {
//     let test_vec = vec!["call offset", "call i@0xda", "ret"];
//     let truth: Vec<Statement<InstructionV1<u64, u64, u64>>> = vec![
//         Statement::call(PreresolutionAddress::Unresolved(Label::from("offset"))),
//         Statement::call(PreresolutionAddress::Resolved(InstructionAddress(0xda))),
//         Statement::ResolvedInstruction(InstructionV1::Return)
//     ];

//     for (test, truth) in test_vec.iter().zip(truth) {
//         let parsed = grammar_v1::statement(test).unwrap();
//         assert_eq!(parsed, truth);
//     }
// }

#[test]
fn test_v1_push_pop() {
    use sr_lib::machine::Core;
    
    let input_str = "load x4, #0xff\nload x3, #0x5\npush x1, x3, x4\npop x3, x2\n";
    let prog = AssemblyV1::compile_one(input_str).unwrap();
    let mut core: Core<InstructionV1<u64, u64, u64>> = Core::new();
    core.load_program(prog);

    // println!("0: ****\n {}\n{}", core.state, core.data_memory);
    // println!("Executing {}", core.fetch());

    // for i in 1..10 {
    //     core.execute_one_instruction().unwrap();
    //     println!("{}: ****\n {}\n{}", i, core.state, core.data_memory);
    //     println!("Executing {}", core.fetch());
    // }
    let res = core.run();
    assert!(res.is_ok());
    assert_eq!(core.state.read_reg(ReadWriteRegister::X2.into()), 0x5);
    assert_eq!(core.state.read_reg(ReadWriteRegister::X3.into()), 0xFF);
    assert_eq!(core.state.read_reg(ReadWriteRegister::StackPointer.into()), 0x1);
}


#[test]
fn test_v1_move() {
    let test_vec = vec!["move x3, zero", "copy x2, x4"];
    let truth: Vec<Statement<InstructionV1<u64, u64, u64>>> = vec![
        InstructionV1::RegisterMove(ReadWriteRegister::X3.into(), ReadOnlyRegister::Zero.into()).into(),
        InstructionV1::RegisterMove(ReadWriteRegister::X2.into(), ReadWriteRegister::X4.into()).into()
    ];

    for (test, truth) in test_vec.iter().zip(truth) {
        let parsed = grammar_v1::statement(test).unwrap();
        assert_eq!(parsed, truth);
    }
}

#[test]
fn test_v1_pgcd() {
    use sr_lib::machine::*;
    let input_str = include_str!("../test_vectors/pgcd.s");
    let prog = AssemblyV1::compile_one(input_str).unwrap();

    let mut core: Core<InstructionV1<u64, u64, u64>> = Core::new();
    core.load_program(prog);
    
    //compute gcd(1625, 455) = 65
    let res = core.execute_many_instructions(30); //run 30 dynamic instructions
    assert!(res.is_ok());
    assert_eq!(res.unwrap(), 27);
    assert_eq!(core.data_memory.read(DataAddress(0)), 0x41);//verify result
}


#[test]
fn test_v1_pgcd2() {
    use sr_lib::machine::*;
    let input_str = include_str!("../test_vectors/pgcd.s");
    let prog = AssemblyV1::compile_one(input_str).unwrap();

    let mut core: Core<InstructionV1<u64, u64, u64>> = Core::new();
    core.load_program(prog);
    
    //compute gcd(1625, 455) = 65
    let res = core.run();
    assert!(res.is_ok());
    assert_eq!(res.unwrap(), 27);
    assert_eq!(core.data_memory.read(DataAddress(0)), 0x41);//verify result
}