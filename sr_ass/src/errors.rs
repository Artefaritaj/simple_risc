/*
 * File: errors.rs
 * Project: src
 * Created Date: Tuesday March 26th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 29th March 2019 10:48:33 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */
use failure::Fail;

#[derive(Debug,Fail)]
pub enum AssemblyError {
    #[fail(display = "Cannot create instruction from parsed input {}", _0)]
    InstructionCreationFail(String),

    #[fail(display = "Cannot parse input: {}", _0)]
    ParsingFailed(String)
}