/*
 * File: lib.rs
 * Project: src
 * Created Date: Friday March 22nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 26th April 2019 11:10:52 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

pub mod assembly_v1;
pub mod assembly_v2;
pub mod assembly_v3;

pub mod errors;

pub mod grammar_v1 {
    include!(concat!(env!("OUT_DIR"), "/sr_assembly_v1.rs"));
}

pub mod grammar_v2 {
    include!(concat!(env!("OUT_DIR"), "/sr_assembly_v2.rs"));
}

pub mod grammar_v3 {
    include!(concat!(env!("OUT_DIR"), "/sr_assembly_v3.rs"));
}

pub use assembly_v1::AssemblyV1;
pub use assembly_v2::AssemblyV2;
pub use assembly_v3::AssemblyV3;