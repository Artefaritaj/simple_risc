# Simple RISC assembly


## Memory transfer

Load value from memory address to register.
```
load    a0, d@0xdeadbeaf
```

Store value from register to memory address.
```
store   d@0xdeadbeaf, a0
```

```
load    val_reg, add_reg    //load in val_reg to data present at memory address in add_reg
store   val_reg, add_reg    //store value in val_reg at memory address in add_reg
```

Load a **n**on **d**eterministic value in specified register (can be rand / input / ...)
```
nd  reg
```

## Immediate

Load value from literal (value embedded in instruction) to register.
```
load    a0, #0xdeadbeaf
```

## Integer arithmetic


```
xor     dest, s1, s2    // dest = s1 xor s2
sub     dest, s1, s2    // dest = s1 - s2
add     dest, s1, s2    // dest = s1 + s2
and     dest, s1, s2    // dest = s1 and s2
or      dest, s1, s2    // dest = s1 or s2
mul     dest, s1, s2    // dest = s1 * s2
div     dest, s1, s2    // dest = s1 / s2
rem     dest, s1, s2    // dest = s1 % s2
//sll     dest, s1, s2    // dest = s1 << s2 (logical)
//srl     dest, s1, s2    // dest = s1 >> s2 (logical)
sla     dest, s1, s2    // dest = s1 << s2 (arithmetic)
sra     dest, s1, s2    // dest = s1 >> s2 (arithmetic)
```

```
not     dest, s1
// can be computed with
load    s2, #FFFFFFFFFFFFFFFF
xor     dest, s1, s2
// or
xor     dest, s1, full
```

## Conditionnal branches

```
beq     add, s1, s2     // jump to add if s1 == s2
bneq    add, s1, s2     // jump to add if s1 != s2
bge     add, s1, s2     // jump to add if s1 >= s2
bg      add, s1, s2     // jump to add if s1 >  s2
ble     add, s1, s2     // jump to add if s1 <= s2
bl      add, s1, s2     // jump to add if s1 <  s2
```

## Call/Return

```
call i@0xdeadbeef
ret
```
or
```
call label
return
```

## Jump

```
jump i@0xdeadbeef
jump label
goto label
```

## Move

```
move    dest, source
mov     dest, source
copy    dest, source
```

## No operation

```
nop
```