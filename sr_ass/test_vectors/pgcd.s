/*
 * File: pgcd.s
 * Project: test_vectors
 * Created Date: Tuesday March 26th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 29th March 2019 3:09:58 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

start:
//set and save inputs

    //nd  x1
    //nd  x2
    load    x1, #d1625
    load    x2, #d455
    store d@0x1, x1
    store d@0x2, x2

// compute PGCD of x1 and x2 with recursive method
pgcd:
    beq     end, x2, zero   // jump to end if x2 == 0
    copy    x3, x2          // x3 <- x2
    rem     x2, x1, x2      // x2 <- x1 % x2
    copy    x1, x3          // x1 <- x3
    goto    pgcd

// store result at address 0 in data memory
end:
    store   d@0x0, x1