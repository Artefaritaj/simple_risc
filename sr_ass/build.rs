/*
 * File: build.rs
 * Project: target
 * Created Date: Tuesday March 26th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 26th April 2019 11:05:31 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

extern crate peg;

fn main() {
    peg::cargo_build("grammar/sr_assembly_v1.rustpeg");
    peg::cargo_build("grammar/sr_assembly_v2.rustpeg");
    peg::cargo_build("grammar/sr_assembly_v3.rustpeg");
}