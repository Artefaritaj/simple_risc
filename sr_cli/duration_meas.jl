using CSV
using Statistics

function measure(file)
    df = CSV.read(file, header=0)
    mat = convert(Matrix, df)
    (mean(mat), std(mat))
end