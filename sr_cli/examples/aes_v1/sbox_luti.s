/*
 * File: sbox.s
 * Project: aes
 * Created Date: Tuesday April 16th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 26th April 2019 2:30:44 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1 is sbox input

!sbox_luti:
// ***** save context
    push x2, x3, x4, x14

// init
    load    x4, #0x8
    rem     x2, x1, x4 //x2 = x1(input) % 8
    mul     x2, x2, x4 //x2 = 8*x2 (byte to bit index)
    div     x3, x1, x4 //x3 = x1(input) % 8

    load    x4, #0x1
    sla     x3, x3, x4
    load    x4, #0x2
    add     x3, x3, x4

    call    this //get this address in x14
this:
    add     x14, x14, x3
    jump    x14

sbox0_7:
    load    x3, #0xC56F6BF27B777C63
    goto    select_byte
sbox8_f:
    load    x3, #0x76ABD7FE2B670130
    goto    select_byte
sbox10_17:
    load    x3, #0xF04759FA7DC982CA
    goto    select_byte
sbox18_1f:
    load    x3, #0xC072A49CAFA2D4AD
    goto    select_byte
sbox20_27:
    load    x3, #0xCCF73F362693FDB7
    goto    select_byte
sbox28_2f:
    load    x3, #0x1531D871F1E5A534
    goto    select_byte
sbox30_37:
    load    x3, #0x9A059618C323C704
    goto    select_byte
sbox38_3f:
    load    x3, #0x75B227EBE2801207
    goto    select_byte
sbox40_47:
    load    x3, #0xA05A6E1B1A2C8309
    goto    select_byte
sbox48_4f:
    load    x3, #0x842FE329B3D63B52
    goto    select_byte
sbox50_57:
    load    x3, #0x5BB1FC20ED00D153
    goto    select_byte
sbox58_5f:
    load    x3, #0xCF584C4A39BECB6A
    goto    select_byte
sbox60_67:
    load    x3, #0x85334D43FBAAEFD0
    goto    select_byte
sbox68_6f:
    load    x3, #0xA89F3C507F02F945
    goto    select_byte
sbox70_77:
    load    x3, #0xF5389D928F40A351
    goto    select_byte
sbox78_7f:
    load    x3, #0xD2F3FF1021DAB6BC
    goto    select_byte
sbox80_87:
    load    x3, #0x1744975FEC130CCD
    goto    select_byte
sbox88_8f:
    load    x3, #0x73195D643D7EA7C4
    goto    select_byte
sbox90_97:
    load    x3, #0x88902A22DC4F8160
    goto    select_byte
sbox98_9f:
    load    x3, #0xDB0B5EDE14B8EE46
    goto    select_byte
sboxa0_a7:
    load    x3, #0x5C2406490A3A32E0
    goto    select_byte
sboxa8_af:
    load    x3, #0x79E4959162ACD3C2
    goto    select_byte
sboxb0_b7:
    load    x3, #0xA94ED58D6D37C8E7
    goto    select_byte
sboxb8_bf:
    load    x3, #0x08AE7A65EAF4566C
    goto    select_byte
sboxc0_c7:
    load    x3, #0xC6B4A61C2E2578BA
    goto    select_byte
sboxc8_cf:
    load    x3, #0x8A8BBD4B1F74DDE8
    goto    select_byte
sboxd0_d7:
    load    x3, #0x0EF6034866B53E70
    goto    select_byte
sboxd8_df:
    load    x3, #0x9E1DC186B9573561
    goto    select_byte
sboxe0_e7:
    load    x3, #0x948ED9691198F8E1
    goto    select_byte
sboxe8_ef:
    load    x3, #0xDF2855CEE9871E9B
    goto    select_byte
sboxf0_f7:
    load    x3, #0x6842E6BF0D89A18C
    goto    select_byte
sboxf8_ff:
    load    x3, #0x16BB54B00F2D9941
    goto    select_byte

select_byte:
    load    x4, #0xFF
    sra     x3, x3, x2
    and     x1, x3, x4

//x1 is sbox output
// ****** restore context
    pop     x14, x4, x3, x2

//return
    jump    x14

error:
    halt