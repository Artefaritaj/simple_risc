/*
 * File: rotword.s
 * Project: aes
 * Created Date: Thursday April 18th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 26th April 2019 2:27:07 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1: 32-bti word to rotate

!rotword:
    //save context
    push    x2, x4

    load    x4, #0xFF000000
    and     x2, x1, x4

    load    x4, #0x18
    sra     x2, x2, x4

    load    x4, #0x00FFFFFF
    and     x1, x1, x4

    load    x4, #0x8
    sla     x1, x1, x4

    xor     x1, x1, x2

    //restore context
    pop     x4, x2

    //return
    jump    x14