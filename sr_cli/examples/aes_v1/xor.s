/*
 * File: xor.s
 * Project: aes
 * Created Date: Monday April 15th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 26th April 2019 2:21:22 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1 contains operand 1 address,
//x2 contains operand 2 address,
//x3 contains number of u64 words to xor
!xor:
// ***** save context *****
    //push x3, loop counter
    //push x5 and x6 to make place for data operand
    //push x4, for constant 1
    push    x3, x4, x5, x6, x14

// ***** initialization *******
    load    x4, #0x1
    goto    loop_condition

// ***** loop on words
xor_loop_start:
    sub     x3, x3, x4

    //load operands (indexed load)
    add     x5, x1, x3
    load    x5, x5
    add     x6, x2, x3
    load    x6, x6

    //xor two words and save in x5
    xor x5, x5, x6

    //store in place of first operand
    add     x6, x1, x3
    store   x6, x5    
    
loop_condition:
    bneq    xor_loop_start, x3, zero

end:
// ******* restore context ******
    pop     x14, x6, x5, x4, x3
//return
    jump    x14

error:
    halt
