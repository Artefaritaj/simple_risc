//x1: state address
//x2: key address
//x3: number of words (=2)
//x6: if 0 dont apply mix column

!round:
    push    x14
    
    call    subbytes
    call    shiftrows
    //MixColumns
    beq     mixcolumns_ret, x6, zero
    call    mixcolumns
mixcolumns_ret:
    //KeyScheduleRound
    call    key_round
    
    //AddRoundKey
    call    xor

    pop     x14
    jump    x14