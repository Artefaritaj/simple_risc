/*
 * File: subbytes.s
 * Project: aes
 * Created Date: Tuesday April 16th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 24th May 2019 4:15:50 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1 contains operand address,
//x3 contains number of u64 words to deal with

!subbytes:
    // ****** save context
    push x1, x2, x3, x4, x5, x6, x7, x14

    // init
    copy    x5, x1

//x1 arg
//x2 input word data
//x3 loop counter
//x4 constant
//x5 operand address (copy from input to use x1 for subprocedures)
//x6 temp
//x7 output word data
    goto word_loop_test

word_loop_start:
    load    x4, #0x1
    sub     x3, x3, x4

    add     x2, x5, x3 //compute word address
    load    x2, x2  //load data
    //erase output data
    copy    x7, zero

    //here our word is loaded, we must loop on bytes
    //push and setup new loop counter
        push    x3
        load    x3, #0x8

        goto    byte_loop_test
    byte_loop_start:
        load    x4, #0x1
        sub     x3, x3, x4
        
        load    x4, #0xFF
        and     x1, x2, x4 //x1 = x2 & 0xFF

        load    x4, #0x8
        sra     x2, x2, x4 //x2 = x2 >> 8

        call    sbox_lut
        //now x1 contains sbox output (1 byte)
        //place it in x7
        load    x4, #0x8
        sra     x7, x7, x4
        load    x4, #0x38
        sla     x1, x1, x4 // x1 = x1 << x6
        xor     x7, x7, x1

    byte_loop_test:
        bneq    byte_loop_start, x3, zero

    byte_loop_end:
        pop     x3

    //copy output word in memory
    add     x2, x5, x3 //compute word address
    store   x2, x7 //mem[x2] = x7

word_loop_test:
    bneq    word_loop_start, x3, zero

word_loop_end:
    // ******* restore context
    pop     x14, x7, x6, x5, x4, x3, x2, x1

    jump    x14

error:
    halt