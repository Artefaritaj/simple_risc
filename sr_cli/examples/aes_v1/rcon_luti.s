/*
 * File: rcon_lut.s
 * Project: aes
 * Created Date: Thursday April 18th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 26th April 2019 2:29:50 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */


!rcon_luti:
    //save context
    push    x4, x14

    load    x4, #0x1
    sla     x1, x1, x4

    //get current address
    call    this
this:

    
    add     x14, x14, x1 //x14 contains current address
    jump    x14
     
i1:
    load    x1, #0x01000000
    goto    end
i2:
    load    x1, #0x02000000
    goto    end
i3:
    load    x1, #0x04000000
    goto    end
i4:
    load    x1, #0x08000000
    goto    end
i5:
    load    x1, #0x10000000
    goto    end
i6:
    load    x1, #0x20000000
    goto    end
i7:
    load    x1, #0x40000000
    goto    end
i8:
    load    x1, #0x80000000
    goto    end
i9:
    load    x1, #0x1B000000
    goto    end
i10:
    load    x1, #0x36000000
    goto    end

end:
    //restore context
    pop     x14, x4

    //return
    jump    x14