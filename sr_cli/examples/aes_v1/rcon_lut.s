/*
 * File: rcon_lut.s
 * Project: aes
 * Created Date: Thursday April 18th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 24th May 2019 4:14:31 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

!rcon_init:
    push    x1, x4, x14

    load    x4, #0x100001 //data address for rcon
    load    x1, #0x01000000
    store   x4, x1
    
    load    x4, #0x100002
    load    x1, #0x02000000
    store   x4, x1

    load    x4, #0x100003
    load    x1, #0x04000000
    store   x4, x1

    load    x4, #0x100004
    load    x1, #0x08000000
    store   x4, x1

    load    x4, #0x100005
    load    x1, #0x10000000
    store   x4, x1

    load    x4, #0x100006
    load    x1, #0x20000000
    store   x4, x1

    load    x4, #0x100007
    load    x1, #0x40000000
    store   x4, x1

    load    x4, #0x100008
    load    x1, #0x80000000
    store   x4, x1

    load    x4, #0x100009
    load    x1, #0x1B000000
    store   x4, x1

    load    x4, #0x10000A
    load    x1, #0x36000000
    store   x4, x1

    pop     x14, x4, x1
    jump    x14

!rcon_lut:
    //save context
    push    x4

    load    x4, #0x100000
    add     x4, x4, x1
    load    x1, x4

end:
    //restore context
    pop     x4

    //return
    jump    x14