/*
 * File: main.s
 * Project: aes
 * Created Date: Monday April 15th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 10th May 2019 4:41:29 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//address map
//0: key[0]
//1: key[1]
//2: plaintext[0]
//3: plaintext[1]
//4: round number

//0x100: sp start

//x14 is pseudo link register
//x15 is temp register

!entry:
//init sp
    load    x1, #0x100
    copy    sp, x1

//init rcon
    call    rcon_init
//init sbox
    call    sbox_init

//load key
    load    x1, #0x2B7E151628AED2A6
    load    x2, #0x0
    store   x2, x1

    load    x1, #0xABF7158809CF4F3C
    load    x2, #0x1
    store   x2, x1

//load plaintext
    nd      x1
//    load    x1, #0x3243F6A8885A308D
    load    x2, #0x2
    store   x2, x1

    nd      x1
//    load    x1, #0x313198A2E0370734
    load    x2, #0x3
    store   x2, x1


//ARK0
    //prepare arguments
    //state address
    load    x1, #0x2 
    //key address
    load    x2 #0x0
    //number of words to xor
    load    x3, #0x2
    //call xor for ARK0
    call    xor

//9 rounds
    // init
    load    x4, #0x0 // start round
    load    x5, #0x9 // end round
    load    x13, #0x1 //constant 1

    goto round_end_test

round_loop_start:
    add     x4, x4, x13 //x4++
    //prepare arguments
    //state address
    load    x1, #0x2 
    //key address
    load    x2, #0x0
    //number of words to xor
    load    x3, #0x2
    //round index
    load    x15, #0x4
    store   x15, x4
    load    x6, #0x1 // apply mix columns
    call    round

round_end_test:
    bneq    round_loop_start, x4, x5

    //round index
    load    x15, #0x4
    load    x4, #0x0A
    store   x15, x4
    load    x6, #0x0

    call    round


    halt