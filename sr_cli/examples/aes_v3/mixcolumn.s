/*
 * File: mixcolumn.s
 * Project: aes
 * Created Date: Tuesday April 16th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 7th May 2019 9:11:57 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1 contains colums (32 bits = 4 bytes)
!mixcolumn:
    // save context
    push    x2, x3, x4, x5
    
    //x1: arg/temp
    //x2: temp output
    //x3: input copy
    //x4: constants
    //x5: temp
    copy    x2, zero
    copy    x3, x1

    //1) byte 0
    load    x4, #0xFF000000
    and     x1, x3, x4
    load    x4, #0x18
    sra     x1, x1, x4

    // -> byte 1 and 2 output
    load    x4, #0x8
    sla     x5, x1, x4

    xor     x2, x2, x5 //* 2
    sla     x5, x5, x4
    xor     x2, x2, x5 //* 1

    // -> byte 0 output
    push    x1
    call    xtimes
    load    x4, #0x18
    sla     x1, x1, x4
    xor     x2, x2, x1 //* 0
    pop     x1
    // -> byte 3 output
    call    xtimes3 
    load    x4, #0x0
    sla     x1, x1, x4
    xor     x2, x2, x1 //* 3

    //2) byte 1
    load    x4, #0x00FF0000
    and     x1, x3, x4
    load    x4, #0x10
    sra     x1, x1, x4

    // -> byte 2 and 3 output
    load    x4, #0x08
    sla     x5, x1, x4
    xor     x2, x2, x5 //* 2

    load    x4, #0x00
    sla     x5, x1, x4
    xor     x2, x2, x5 //* 3

    // -> byte 1 output
    push    x1
    call    xtimes
    load    x4, #0x10
    sla     x1, x1, x4
    xor     x2, x2, x1 //* 1
    pop     x1
    // -> byte 0 output
    call    xtimes3
    load    x4, #0x18
    sla     x1, x1, x4
    xor     x2, x2, x1 //* 0

    //3) byte 2
    load    x4, #0x0000FF00
    and     x1, x3, x4
    load    x4, #0x08
    sra     x1, x1, x4

    // -> byte 0 and 3 output
    load    x4, #0x0
    sla     x5, x1, x4
    xor     x2, x2, x5 //* 3

    load    x4, #0x18
    sla     x5, x1, x4
    xor     x2, x2, x5 //* 0

    // -> byte 2 output
    push    x1
    call    xtimes
    load    x4, #0x08
    sla     x1, x1, x4
    xor     x2, x2, x1 //* 2
    pop     x1
    // -> byte 1 output
    call    xtimes3
    load    x4, #0x10
    sla     x1, x1, x4
    xor     x2, x2, x1 //*  1

    //3) byte 3
    load    x4, #0x000000FF
    and     x1, x3, x4
//    load    x4, #0x00
//    sra     x1, x1, x4

    // -> byte 0 and 1 output
    load    x4, #0x18
    sla     x5, x1, x4
    xor     x2, x2, x5 //* 0

    load    x4, #0x10
    sla     x5, x1, x4
    xor     x2, x2, x5 //* 1

    // -> byte 2 output
    push    x1
    call    xtimes
    load    x4, #0x00
    sla     x1, x1, x4
    xor     x2, x2, x1 //* 2
    pop     x1
    // -> byte 1 output
    call    xtimes3
    load    x4, #0x08
    sla     x1, x1, x4
    xor     x2, x2, x1 //* 3

    copy    x1, x2

    // restore context
    pop     x5, x4, x3, x2
    return