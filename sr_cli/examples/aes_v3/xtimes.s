/*
 * File: xtimes.s
 * Project: aes
 * Created Date: Tuesday April 16th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 26th April 2019 2:29:07 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1 byte value to modify
!xtimes:
    // ****** save context
    push    x2, x4

    load    x4, #0x1
    sla     x1, x1, x4 // x1 = x1 << 1

    //test modulo
    load    x4, #0x100
    and     x2, x1, x4
    beq     end, x2, zero

sub_modulo:
    load    x4, #0x11B
    xor     x1, x1, x4

end:
    

    // ****** restore context
    pop     x4, x2
    //return
    return