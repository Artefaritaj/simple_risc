/*
 * File: key_round.s
 * Project: aes
 * Created Date: Thursday April 18th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 18th April 2019 2:46:17 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x2: key address

!key_round:
    //save context
    push    x1, x2, x3, x4, x5, x6

    //x4: constants
    //x5: first 64-bit key
    //x6: second 64-bit key

    //load 1rst 64-bit word
    load    x4, #0x0
    add     x5, x2, x4
    load    x5, x5
    //x5 now contains 1rst 64-bit vector

    //load 2nd 64-bit word
    load    x4, #0x1
    add     x6, x2, x4
    load    x6, x6
    //x6 now contains second 64-bit vector

    //word 1)

    // compute transformation
    load    x4, #0xFFFFFFFF
    and     x1, x6, x4

    goto    rotword
!rotword_ret:
    goto    subword
!subword_ret:
    push    x1

    //load round number
    load    x4, #0x4
    load    x1, x4

    goto    rcon_lut
!rcon_ret:
    copy    x3, x1
    pop     x1

    xor     x1, x1, x3
    //transformation result in x1

    load    x4, #0x20
    sla     x1, x1, x4
    //add to first word
    xor     x5, x5, x1

    //word 2)
    load    x4, #0xFFFFFFFF00000000
    and     x1, x5, x4
    load    x4, #0x20
    sra     x1, x1, x4
    xor     x5, x5, x1

    //word 3)
    load    x4, #0xFFFFFFFF
    and     x1, x5, x4
    load    x4, #0x20
    sla     x1, x1, x4
    xor     x6, x6, x1

    //word 4)
    load    x4, #0xFFFFFFFF00000000
    and     x1, x6, x4
    load    x4, #0x20
    sra     x1, x1, x4
    xor     x6, x6, x1

    //store results
    load    x4, #0x0
    add     x1, x2, x4
    store   x1, x5

    load    x4, #0x1
    add     x1, x2, x4
    store   x1, x6

    //restore context
    pop     x6, x5, x4, x3, x2, x1

    //return
    goto    key_round_ret