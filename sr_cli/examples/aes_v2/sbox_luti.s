/*
 * File: sbox.s
 * Project: aes
 * Created Date: Tuesday April 16th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 18th April 2019 10:28:04 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1 is sbox input

!sbox_luti:
// ***** save context
    push x2, x3, x4

// init
    load    x4, #0x8
    rem     x2, x1, x4 //x2 = x1(input) % 8
    mul     x2, x2, x4 //x2 = 8*x2 (byte to bit index)
    div     x3, x1, x4 //x3 = x1(input) % 8

    //multi branch
    load    x4, #0x0 //x4 = 0 
    beq     sbox0_7, x3, x4
    load    x4, #0x1
    beq     sbox8_f, x3, x4
    load    x4, #0x2
    beq     sbox10_17, x3, x4
    load    x4, #0x3
    beq     sbox18_1f, x3, x4
    load    x4, #0x4
    beq     sbox20_27, x3, x4
    load    x4, #0x5
    beq     sbox28_2f, x3, x4
    load    x4, #0x6
    beq     sbox30_37, x3, x4
    load    x4, #0x7
    beq     sbox38_3f, x3, x4
    load    x4, #0x8
    beq     sbox40_47, x3, x4
    load    x4, #0x9
    beq     sbox48_4f, x3, x4
    load    x4, #0xA
    beq     sbox50_57, x3, x4
    load    x4, #0xB
    beq     sbox58_5f, x3, x4
    load    x4, #0xC
    beq     sbox60_67, x3, x4
    load    x4, #0xD
    beq     sbox68_6f, x3, x4
    load    x4, #0xE
    beq     sbox70_77, x3, x4
    load    x4, #0xF
    beq     sbox78_7f, x3, x4
    load    x4, #0x10
    beq     sbox80_87, x3, x4
    load    x4, #0x11
    beq     sbox88_8f, x3, x4
    load    x4, #0x12
    beq     sbox90_97, x3, x4
    load    x4, #0x13
    beq     sbox98_9f, x3, x4
    load    x4, #0x14
    beq     sboxa0_a7, x3, x4
    load    x4, #0x15
    beq     sboxa8_af, x3, x4
    load    x4, #0x16
    beq     sboxb0_b7, x3, x4
    load    x4, #0x17
    beq     sboxb8_bf, x3, x4
    load    x4, #0x18
    beq     sboxc0_c7, x3, x4
    load    x4, #0x19
    beq     sboxc8_cf, x3, x4
    load    x4, #0x1A
    beq     sboxd0_d7, x3, x4
    load    x4, #0x1B
    beq     sboxd8_df, x3, x4
    load    x4, #0x1C
    beq     sboxe0_e7, x3, x4
    load    x4, #0x1D
    beq     sboxe8_ef, x3, x4
    load    x4, #0x1E
    beq     sboxf0_f7, x3, x4
    load    x4, #0x1F
    beq     sboxf8_ff, x3, x4
    goto    error

sbox0_7:
    load    x3, #0xC56F6BF27B777C63
    goto    select_byte
sbox8_f:
    load    x3, #0x76ABD7FE2B670130
    goto    select_byte
sbox10_17:
    load    x3, #0xF04759FA7DC982CA
    goto    select_byte
sbox18_1f:
    load    x3, #0xC072A49CAFA2D4AD
    goto    select_byte
sbox20_27:
    load    x3, #0xCCF73F362693FDB7
    goto    select_byte
sbox28_2f:
    load    x3, #0x1531D871F1E5A534
    goto    select_byte
sbox30_37:
    load    x3, #0x9A059618C323C704
    goto    select_byte
sbox38_3f:
    load    x3, #0x75B227EBE2801207
    goto    select_byte
sbox40_47:
    load    x3, #0xA05A6E1B1A2C8309
    goto    select_byte
sbox48_4f:
    load    x3, #0x842FE329B3D63B52
    goto    select_byte
sbox50_57:
    load    x3, #0x5BB1FC20ED00D153
    goto    select_byte
sbox58_5f:
    load    x3, #0xCF584C4A39BECB6A
    goto    select_byte
sbox60_67:
    load    x3, #0x85334D43FBAAEFD0
    goto    select_byte
sbox68_6f:
    load    x3, #0xA89F3C507F02F945
    goto    select_byte
sbox70_77:
    load    x3, #0xF5389D928F40A351
    goto    select_byte
sbox78_7f:
    load    x3, #0xD2F3FF1021DAB6BC
    goto    select_byte
sbox80_87:
    load    x3, #0x1744975FEC130CCD
    goto    select_byte
sbox88_8f:
    load    x3, #0x73195D643D7EA7C4
    goto    select_byte
sbox90_97:
    load    x3, #0x88902A22DC4F8160
    goto    select_byte
sbox98_9f:
    load    x3, #0xDB0B5EDE14B8EE46
    goto    select_byte
sboxa0_a7:
    load    x3, #0x5C2406490A3A32E0
    goto    select_byte
sboxa8_af:
    load    x3, #0x79E4959162ACD3C2
    goto    select_byte
sboxb0_b7:
    load    x3, #0xA94ED58D6D37C8E7
    goto    select_byte
sboxb8_bf:
    load    x3, #0x08AE7A65EAF4566C
    goto    select_byte
sboxc0_c7:
    load    x3, #0xC6B4A61C2E2578BA
    goto    select_byte
sboxc8_cf:
    load    x3, #0x8A8BBD4B1F74DDE8
    goto    select_byte
sboxd0_d7:
    load    x3, #0x0EF6034866B53E70
    goto    select_byte
sboxd8_df:
    load    x3, #0x9E1DC186B9573561
    goto    select_byte
sboxe0_e7:
    load    x3, #0x948ED9691198F8E1
    goto    select_byte
sboxe8_ef:
    load    x3, #0xDF2855CEE9871E9B
    goto    select_byte
sboxf0_f7:
    load    x3, #0x6842E6BF0D89A18C
    goto    select_byte
sboxf8_ff:
    load    x3, #0x16BB54B00F2D9941
    goto    select_byte

select_byte:
    load    x4, #0xFF
    sra     x3, x3, x2
    and     x1, x3, x4

//x1 is sbox output
// ****** restore context
    pop     x4, x3, x2

//return
    load    x15, #0x1
    beq     sbox_ret1, x14, x15
    load    x15, #0x2
    beq     sbox_ret2, x14, x15

error:
    halt