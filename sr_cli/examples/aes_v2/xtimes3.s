/*
 * File: xtimes.s
 * Project: aes
 * Created Date: Tuesday April 16th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 17th April 2019 9:57:29 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1 byte value to modify
!xtimes3:
    // ****** save context
    push    x2, x3, x4

    copy    x3, x1

    load    x4, #0x1
    sla     x1, x1, x4 // x1 = x1 << 1

    //test modulo
    load    x4, #0x100
    and     x2, x1, x4
    beq     end, x2, zero

sub_modulo:
    load    x4, #0x11B
    xor     x1, x1, x4

end:
    xor     x1, x1, x3

    // ****** restore context
    pop     x4, x3, x2

    load    x15, #0x1
    beq     xtimes3_ret1, x14, x15
    load    x15, #0x2
    beq     xtimes3_ret2, x14, x15
    load    x15, #0x3
    beq     xtimes3_ret3, x14, x15
    load    x15, #0x4
    beq     xtimes3_ret4, x14, x15

    halt