/*
 * File: xor.s
 * Project: aes
 * Created Date: Monday April 15th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 18th April 2019 3:08:32 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1 contains operand 1 address,
//x2 contains operand 2 address,
//x3 contains number of u64 words to xor
!xor:
// ***** save context *****
    //push x3, loop counter
    //push x5 and x6 to make place for data operand
    //push x4, for constant 1
    push x3, x4, x5, x6

// ***** initialization *******
    load    x4, #0x1
    goto    loop_condition

// ***** loop on words
xor_loop_start:
    sub     x3, x3, x4

    //load operands (indexed load)
    add     x5, x1, x3
    load    x5, x5
    add     x6, x2, x3
    load    x6, x6

    //xor two words and save in x5
    xor x5, x5, x6

    //store in place of first operand
    add     x6, x1, x3
    store   x6, x5    
    
loop_condition:
    bneq    xor_loop_start, x3, zero

end:
// ******* restore context ******
    pop x6, x5, x4, x3

//return
    load    x15, #0x1
    beq     xor_ret1, x14, x15
    load    x15, #0x2
    beq     xor_ret2, x14, x15
//    load    x15, #0x3
//    beq     xor_ret3, x14, x15

error:
    halt
