/*
 * File: rcon_lut.s
 * Project: aes
 * Created Date: Thursday April 18th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 18th April 2019 3:01:12 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */


!rcon_luti:
    //save context
    push    x4

    load    x4, #0x5
    bg      bigger5, x1, x4
lower5:
    load    x4, #0x1
    beq     i1, x1, x4
    load    x4, #0x2
    beq     i2, x1, x4
    load    x4, #0x3
    beq     i3, x1, x4
    load    x4, #0x4
    beq     i4, x1, x4
    load    x4, #0x5
    beq     i5, x1, x4
bigger5:
    load    x4, #0x6
    beq     i6, x1, x4
    load    x4, #0x7
    beq     i7, x1, x4
    load    x4, #0x8
    beq     i8, x1, x4
    load    x4, #0x9
    beq     i9, x1, x4
    load    x4, #0xA
    beq     i10, x1, x4
//error if we reach here
    goto    error
     
i1:
    load    x1, #0x01000000
    goto    end
i2:
    load    x1, #0x02000000
    goto    end
i3:
    load    x1, #0x04000000
    goto    end
i4:
    load    x1, #0x08000000
    goto    end
i5:
    load    x1, #0x10000000
    goto    end
i6:
    load    x1, #0x20000000
    goto    end
i7:
    load    x1, #0x40000000
    goto    end
i8:
    load    x1, #0x80000000
    goto    end
i9:
    load    x1, #0x1B000000
    goto    end
i10:
    load    x1, #0x36000000
    goto    end

end:
    //restore context
    pop     x4

    //return
    goto    rcon_ret

error:
    halt