/*
 * File: mixcolumns.s
 * Project: aes
 * Created Date: Wednesday April 17th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 17th April 2019 10:32:41 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1 contains operand address

!mixcolumns:
    // save context
    push    x1, x2, x3, x4, x5, x6, x14

    //x1: arg
    //x2: input word data
    //x3: address copy
    //x4: constants
    //x5: temp
    //x6: output word data
    //x14: return code

    //init
    copy    x3, x1
    

    // word 1
    copy    x6, zero

    
    load    x2, x3 //load data word

    // mc1
    load    x4, #0x20
    sra     x1, x2, x4

    load    x14, #0x1
    goto    mixcolumn
!mixcolumn_ret1:
    sla     x1, x1, x4
    xor     x6, x1, x6

    //mc2
    load    x4, #0xFFFFFFFF
    and     x1, x2, x4

    load    x14, #0x2
    goto    mixcolumn
!mixcolumn_ret2:
    xor     x6, x1, x6

    //store in mem
    store   x3, x6

    // word 2
    copy    x6, zero
    load    x4, #0x1
    add     x2, x3, x4
    load    x2, x2

    // mc3
    load    x4, #0x20
    sra     x1, x2, x4

    load    x14, #0x3
    goto    mixcolumn
!mixcolumn_ret3:
    sla     x1, x1, x4
    xor     x6, x1, x6

    //mc4
    load    x4, #0xFFFFFFFF
    and     x1, x2, x4

    load    x14, #0x4
    goto    mixcolumn
!mixcolumn_ret4:
    xor     x6, x1, x6

    //store in mem
    load    x4, #0x1
    add     x5, x3, x4
    store   x5, x6

    // restore context
    pop     x14, x6, x5, x4, x3, x2, x1
    
    //return
    goto mixcolumns_ret