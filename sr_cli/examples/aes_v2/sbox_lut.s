/*
 * File: sbox.s
 * Project: aes
 * Created Date: Tuesday April 16th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 7th May 2019 10:10:58 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1 is sbox input

!sbox_init:
    push    x1, x4

    load    x4, #0x200000 //data address for sbox
    load    x1, #0xC56F6BF27B777C63
    store   x4, x1

    load    x4, #0x200001 //data address for sbox
    load    x1, #0x76ABD7FE2B670130
    store   x4, x1

    load    x4, #0x200002 //data address for sbox
    load    x1, #0xF04759FA7DC982CA
    store   x4, x1

    load    x4, #0x200003 //data address for sbox
    load    x1, #0xC072A49CAFA2D4AD
    store   x4, x1

    load    x4, #0x200004 //data address for sbox
    load    x1, #0xCCF73F362693FDB7
    store   x4, x1

    load    x4, #0x200005 //data address for sbox
    load    x1, #0x1531D871F1E5A534
    store   x4, x1

    load    x4, #0x200006 //data address for sbox
    load    x1, #0x9A059618C323C704
    store   x4, x1

    load    x4, #0x200007 //data address for sbox
    load    x1, #0x75B227EBE2801207
    store   x4, x1

    load    x4, #0x200008 //data address for sbox
    load    x1, #0xA05A6E1B1A2C8309
    store   x4, x1

    load    x4, #0x200009 //data address for sbox
    load    x1, #0x842FE329B3D63B52
    store   x4, x1

    load    x4, #0x20000A //data address for sbox
    load    x1, #0x5BB1FC20ED00D153
    store   x4, x1

    load    x4, #0x20000B //data address for sbox
    load    x1, #0xCF584C4A39BECB6A
    store   x4, x1

    load    x4, #0x20000C //data address for sbox
    load    x1, #0x85334D43FBAAEFD0
    store   x4, x1

    load    x4, #0x20000D //data address for sbox
    load    x1, #0xA89F3C507F02F945
    store   x4, x1

    load    x4, #0x20000E //data address for sbox
    load    x1, #0xF5389D928F40A351
    store   x4, x1

    load    x4, #0x20000F //data address for sbox
    load    x1, #0xD2F3FF1021DAB6BC
    store   x4, x1

    load    x4, #0x200010 //data address for sbox
    load    x1, #0x1744975FEC130CCD
    store   x4, x1

    load    x4, #0x200011 //data address for sbox
    load    x1, #0x73195D643D7EA7C4
    store   x4, x1

    load    x4, #0x200012 //data address for sbox
    load    x1, #0x88902A22DC4F8160
    store   x4, x1

    load    x4, #0x200013 //data address for sbox
    load    x1, #0xDB0B5EDE14B8EE46
    store   x4, x1

    load    x4, #0x200014 //data address for sbox
    load    x1, #0x5C2406490A3A32E0
    store   x4, x1

    load    x4, #0x200015 //data address for sbox
    load    x1, #0x79E4959162ACD3C2
    store   x4, x1

    load    x4, #0x200016 //data address for sbox
    load    x1, #0xA94ED58D6D37C8E7
    store   x4, x1

    load    x4, #0x200017 //data address for sbox
    load    x1, #0x08AE7A65EAF4566C
    store   x4, x1

    load    x4, #0x200018 //data address for sbox
    load    x1, #0xC6B4A61C2E2578BA
    store   x4, x1

    load    x4, #0x200019 //data address for sbox
    load    x1, #0x8A8BBD4B1F74DDE8
    store   x4, x1

    load    x4, #0x20001A //data address for sbox
    load    x1, #0x0EF6034866B53E70
    store   x4, x1

    load    x4, #0x20001B //data address for sbox
    load    x1, #0x9E1DC186B9573561
    store   x4, x1

    load    x4, #0x20001C //data address for sbox
    load    x1, #0x948ED9691198F8E1
    store   x4, x1

    load    x4, #0x20001D //data address for sbox
    load    x1, #0xDF2855CEE9871E9B
    store   x4, x1

    load    x4, #0x20001E //data address for sbox
    load    x1, #0x6842E6BF0D89A18C
    store   x4, x1

    load    x4, #0x20001F //data address for sbox
    load    x1, #0x16BB54B00F2D9941
    store   x4, x1

    pop     x4, x1
    jump    sbox_init_ret

!sbox_lut:
// ***** save context
    push x2, x3, x4, x14

// init
    load    x4, #0x8
    rem     x2, x1, x4 //x2 = x1(input) % 8
    mul     x2, x2, x4 //x2 = 8*x2 (byte to bit index)
    div     x3, x1, x4 //x3 = x1(input) % 8

    load    x4, #0x200000
    add     x4, x4, x3
    load    x3, x4

select_byte:
    load    x4, #0xFF
    sra     x3, x3, x2
    and     x1, x3, x4

//x1 is sbox output
// ****** restore context
    pop     x14, x4, x3, x2

//return
    //return
    load    x15, #0x1
    beq     sbox_ret1, x14, x15
    load    x15, #0x2
    beq     sbox_ret2, x14, x15

error:
    halt