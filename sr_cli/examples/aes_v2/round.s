/*
 * File: round.s
 * Project: aes
 * Created Date: Tuesday April 16th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 18th April 2019 3:07:39 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1: state address
//x2: key address
//x3: number of words (=2)
//x6: if 0 dont apply mix column

!round:
    push    x14
    
    //SubBytes
    load    x14, #0x1
    goto subbytes
!subbytes_ret1:
    //ShiftRows
    load    x14, #0x1
    goto shiftrows
!shiftrows_ret1:
    //MixColumns
    beq     mixcolumns_ret, x6, zero
    goto mixcolumns
!mixcolumns_ret:
    //KeyScheduleRound
    goto key_round
!key_round_ret:
    
    //AddRoundKey
    load    x14, #0x2
    goto    xor
!xor_ret2:

    pop     x14
    
    load    x15, #0x1
    beq     round_ret1, x14, x15
    load    x15, #0x2
    beq     round_ret2, x14, x15