/*
 * File: subword.s
 * Project: aes
 * Created Date: Thursday April 18th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 18th April 2019 2:56:45 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1 a 32-bit word to apply sboxes to

!subword:
    // save context
    push    x2, x3, x4, x5, x14

    //x1: arg
    //x2: copy arg input word
    //x3: loop index
    //x4: constant
    //x5: output word
    //x14: link register

    //init
    copy    x2, x1
    copy    x5, zero
    load    x3, #0x4

    goto    sw_loop_test

sw_loop_start:
    load    x4, #0x1
    sub     x3, x3, x4

    load    x4, #0xFF
    and     x1, x2, x4 //x1 = x2 & 0xFF

    load    x4, #0x8
    sra     x2, x2, x4 //x2 = x2 >> 8

    load    x14, #0x2
    goto    sbox_lut
!sbox_ret2:
    //now x1 contains sbox output (1 byte)
    //place it in x5
    load    x4, #0x8
    sra     x5, x5, x4
    load    x4, #0x18
    sla     x1, x1, x4 // x1 = x1 << x6
    xor     x5, x5, x1

sw_loop_test:
    bneq    sw_loop_start, x3, zero

end:
    copy    x1, x5

    // restore context
    pop     x14, x5, x4, x3, x2

    //return
    goto    subword_ret