/*
 * File: shiftrows.s
 * Project: aes
 * Created Date: Tuesday April 16th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 17th April 2019 10:41:39 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

//x1 contains operand address

!shiftrows:
// x2, x3 input data words
// x4, x5 ouput data words
// x6 temp
// x7 temp

    // ******* save context
    push x1, x2, x3, x4, x5, x6, x7

    //init
    copy    x4, zero
    copy    x5, zero

    // load input words (2 of them)
    load    x2, x1
    load    x6, #0x1
    add     x6, x1, x6
    load    x3, x6

    //1) stays in place (4 bytes)
    load    x6, #0xFF000000FF000000
    and     x4, x2, x6
    and     x5, x3, x6

    //2) cross (4 bytes)
    load    x6, #0x0000FF000000FF00
    and     x6, x2, x6
    xor     x5, x5, x6

    load    x6, #0x0000FF000000FF00
    and     x6, x3, x6
    xor     x4, x4, x6

    //3) rotate (8 bytes)
    //first source word
    load    x6, #0x20
    sra     x7, x2, x6
    sla     x6, x2, x6
    xor     x7, x7, x6

    load    x6, #0x00FF0000000000FF
    and     x6, x7, x6
    xor     x4, x4, x6

    load    x6, #0x000000FF00FF0000
    and     x6, x7, x6
    xor     x5, x5, x6

    //second source word
    load    x6, #0x20
    sra     x7, x3, x6
    sla     x6, x3, x6
    xor     x7, x7, x6

    load    x6, #0x00FF0000000000FF
    and     x6, x7, x6
    xor     x5, x5, x6

    load    x6, #0x000000FF00FF0000
    and     x6, x7, x6
    xor     x4, x4, x6

    //store result in place
    store   x1, x4
    load    x6, #0x1
    add     x6, x1, x6
    store   x6, x5


    // ******* restore context
    pop x7, x6, x5, x4, x3, x2, x1
    
    //return
    load    x15, #0x1
    beq     shiftrows_ret1, x14, x15
//    load    x15, #0x2
//    beq     shiftrows_ret2, x14, x15

error:
    halt
