/*
 * File: pgcd.s
 * Project: test_vectors
 * Created Date: Tuesday March 26th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 10th May 2019 3:45:07 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

start:
//set and save inputs

    //nd  x1
    //nd  x2
    load    x1, #d1625
    load    x2, #d455
    store d@0x1, x1
    store d@0x2, x2

// compute PGCD of x1 and x2 with recursive method
    call    pgcd

// store result at address 0 in data memory
end:
    store   d@0x0, x1
    halt

pgcd:
    beq     lab_return, x2, zero
    copy    x3, x2          // x3 <- x2
    rem     x2, x1, x2      // x2 <- x1 % x2
    copy    x1, x3          // x1 <- x3
    call    pgcd
lab_return:
    return