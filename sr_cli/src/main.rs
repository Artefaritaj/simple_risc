/*
 * File: main.rs
 * Project: src
 * Created Date: Thursday April 11th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 13th June 2019 3:51:07 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

#[macro_use] extern crate clap;
#[macro_use] extern crate failure;
#[macro_use] extern crate log;
extern crate fern;
extern crate chrono;
extern crate sr_lib;
extern crate sr_ass;

use chrono::Local;
use clap::{App, ArgMatches};
use failure::Error;
use sr_lib::isa::*;
use sr_lib::machine::*;
use sr_lib::program::*;
// use sr_lib::program::static_analysis::*;
use sr_ass::*;

use std::fs;
use std::io::{Read};
use std::path::{Path, PathBuf};

const RUNCOUNT: usize = 10000;

#[derive(Debug,Clone,Copy,PartialOrd,Ord,PartialEq,Eq,Hash)]
enum ISA {
    V1,
    V2,
    V3
}

fn init_log_file() -> Result<(), Error> {
    let _ = fs::create_dir("log").map_err(|e|format_err!("Cannot create dir: {}", e));
    let log_filename = format!("log/out.log");

    //remove file if exists
    let _ = fs::remove_file(&log_filename).map_err(|e|format_err!("Cannot remove file: {}", e));
    Ok(())
}

fn init_logger(verbosity: log::LevelFilter, log_verbosity: log::LevelFilter) -> Result<(), Error> {
    let log_filename = format!("log/out.log");
    let base_config = fern::Dispatch::new()
    .format(|out, message, record| {
        //out.finish(format_args!("{}[{}][{}] {}",
        out.finish(format_args!("{}[{}] {}",
            Local::now()
                .format("[%Y-%m-%d][%H:%M:%S]"),
            //record.target(),
            record.level(),
            message))
    });

    let stdout_log = fern::Dispatch::new()
        .level(verbosity)
        .chain(std::io::stdout());

    let file_log = fern::Dispatch::new()
        .level(log_verbosity)
        .chain(fern::log_file(log_filename).map_err(|e|format_err!("{}", e))?);

    base_config
        .chain(file_log)
        .chain(stdout_log)
        .apply()
        .map_err(|e|format_err!("{}", e))?;

    Ok(())
}

fn verbosity_level(n: u64) -> log::LevelFilter {
    match n {
        1 => log::LevelFilter::Error,
        2 => log::LevelFilter::Warn,
        3 => log::LevelFilter::Info,
        4 => log::LevelFilter::Debug,
        5 => log::LevelFilter::Trace,
        _ => log::LevelFilter::Off
    }
}

fn get_isa(isa: Option<&str>) -> ISA {
    match isa {
        Some(isa_str) => {
            match isa_str {
                "v1" => ISA::V1,
                "v2" => ISA::V2,
                "v3" => ISA::V3,
                _    => ISA::V1
            }
        },
        None => ISA::V1
    }
}

fn extract_filepaths_in_multiple(meta_path: &str) -> Result<Vec<PathBuf>, Error> {
    let mut f = fs::File::open(meta_path).map_err(|e|format_err!("Cannot open file {}: {}", meta_path, e))?;
    let mut input_str = String::new();
    f.read_to_string(&mut input_str).map_err(|e|format_err!("{}", e))?;

    let folder_path = Path::new(meta_path).parent().ok_or(format_err!("No parent found for {}", meta_path))?;
    let folder_pb = PathBuf::from(folder_path);

    Ok(input_str.split('\n').map(|s| {
        let mut pb = folder_pb.clone();
        pb.push(s);
        pb
    }).collect())
}

fn extract_paths_arg(run_matches: &ArgMatches) -> Result<Vec<PathBuf>, Error> {
    let mut filepaths: Vec<PathBuf> = Vec::new();

    if let Some(prog_paths) = run_matches.values_of("input") {
        let input_filepaths: Vec<&str> = prog_paths.collect();

        for i in input_filepaths {
            filepaths.push(PathBuf::from(i));
        }
    }

    if let Some(multiple_paths) = run_matches.values_of("multiple_input") {
        let meta_filepaths: Vec<&str> = multiple_paths.collect();

        for meta in meta_filepaths {
            let mut new_paths = extract_filepaths_in_multiple(meta)?;
            filepaths.append(&mut new_paths);
        }
        
    }


    Ok(filepaths)
}

fn main() -> Result<(), Error> {

    let cliyaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(cliyaml).get_matches();

    let verbosity = verbosity_level(matches.occurrences_of("verbose"));
    let log_verbosity = verbosity_level(matches.occurrences_of("logverbose"));
    init_log_file()?;
    init_logger(verbosity, log_verbosity)?;

    let isa = get_isa(matches.value_of("isa"));
    let files = extract_paths_arg(&matches)?;

    debug!("Files {:?} extracted.", files);

    //run a program
    if let Some(_) = matches.subcommand_matches("run") {
        run_prog(&files, isa)?;
    }

    if let Some(_) = matches.subcommand_matches("run_stats") {
        runstats_prog(&files, isa)?;
    }

    //analyze
    if let Some(_) = matches.subcommand_matches("analyze") {
        analyze(&files, isa)?;
    }

    Ok(())
}

fn run_sr_machine_isa_v1(filepaths: &Vec<PathBuf>) -> Result<(), Error> {

    // type DTInstV1 = InstructionV1<u64, DeterminismTracer<u64>, DeterminismTracer<u64>>;
    let mut obj_files: Vec<ObjectFile<InstructionV1<u64, u64, u64>>> = Vec::new();

    //load assembly in string
    for filepath in filepaths.iter() {
        let statements = AssemblyV1::parse_file(&filepath)?;
        obj_files.push(ObjectFile::new(statements));
    }
    

    //compile assembly to machine code
    let prog = ObjectFile::link(&obj_files)?;
    let cfg = static_analysis::cfg::structural(prog.clone());
    // let cfg = static_analysis::cfg::determinism_tracing::<DTInstV1, InstructionV1<u64, u64, u64>>(prog.clone())?;

    // println!("Program is {}", prog);

    //init core and load program
    let mut core: Core<InstructionV1<u64, u64, u64>> = Core::new();
    core.load_program(prog);
    core.init_cfg(&cfg);

    //run program
    core.run()?;

    //display final state
    println!("\n{}\n{}\n{}", core.monitor, core.state, core.data_memory);

    Ok(())
}

fn run_sr_machine_isa_v2(filepaths: &Vec<PathBuf>) -> Result<(), Error> {
    debug!("Run ISAv2.");
    let mut obj_files: Vec<ObjectFile<InstructionV2<u64, u64, u64>>> = Vec::new();

    //load assembly in string
    for filepath in filepaths.iter() {
        let statements = AssemblyV2::parse_file(&filepath)?;
        obj_files.push(ObjectFile::new(statements));
    }
    

    //compile assembly to machine code
    let prog = ObjectFile::link(&obj_files)?;
    let cfg = static_analysis::cfg::structural(prog.clone());

    // println!("Program is {}", prog);

    //init core and load program
    let mut core: Core<InstructionV2<u64, u64, u64>> = Core::new();
    core.load_program(prog);
    core.init_cfg(&cfg);

    //run program
    core.run()?;

    //display final state
    println!("\n{}\n{}\n{}", core.monitor, core.state, core.data_memory);

    Ok(())
}

fn run_sr_machine_isa_v3(filepaths: &Vec<PathBuf>) -> Result<(), Error> {

    let mut obj_files: Vec<ObjectFile<InstructionV3<u64, u64, u64>>> = Vec::new();

    //load assembly in string
    for filepath in filepaths.iter() {
        let statements = AssemblyV3::parse_file(&filepath)?;
        obj_files.push(ObjectFile::new(statements));
    }
    

    //compile assembly to machine code
    let prog = ObjectFile::link(&obj_files)?;
    let cfg = static_analysis::cfg::callstack_tracking(prog.clone());
    // let cfg = static_analysis::cfg::structural(prog.clone());

    // println!("Program is {}", prog);

    //init core and load program
    let mut core: Core<InstructionV3<u64, u64, u64>> = Core::new();
    core.load_program(prog);
    core.init_cfg(&cfg);

    //run program
    core.run()?;

    //display final state
    println!("\n{}\n{}\n{}", core.monitor, core.state, core.data_memory);

    Ok(())
}



fn run_prog(filepaths: &Vec<PathBuf>, isa: ISA) -> Result<(), Error> {

    match isa {
        ISA::V1 => run_sr_machine_isa_v1(filepaths),
        ISA::V2 => run_sr_machine_isa_v2(filepaths),
        ISA::V3 => run_sr_machine_isa_v3(filepaths) 
    }
}

fn runstats_sr_machine_isa_v1(filepaths: &Vec<PathBuf>) -> Result<(), Error> {

    let mut obj_files: Vec<ObjectFile<InstructionV1<u64, u64, u64>>> = Vec::new();

    //load assembly in string
    for filepath in filepaths.iter() {
        let statements = AssemblyV1::parse_file(&filepath)?;
        obj_files.push(ObjectFile::new(statements));
    }
    

    //compile assembly to machine code
    let prog = ObjectFile::link(&obj_files)?;

    // println!("Program is {}", prog);

    for _ in 0..RUNCOUNT {

        //init core and load program
        let mut core: Core<InstructionV1<u64, u64, u64>> = Core::new();
        core.load_program(prog.clone());

        //run program
        let inst_count = core.run()?;
        println!("{}", inst_count);
    }

    Ok(())
}

fn runstats_sr_machine_isa_v2(filepaths: &Vec<PathBuf>) -> Result<(), Error> {
    debug!("Run ISAv2.");
    let mut obj_files: Vec<ObjectFile<InstructionV2<u64, u64, u64>>> = Vec::new();

    // let stdout_before = std::io::stdout();
    // let mut stdout = stdout_before.lock();

    //load assembly in string
    for filepath in filepaths.iter() {
        let statements = AssemblyV2::parse_file(&filepath)?;
        obj_files.push(ObjectFile::new(statements));
    }
    

    //compile assembly to machine code
    let prog = ObjectFile::link(&obj_files)?;

    // println!("Program is {}", prog);

    for _ in 0..RUNCOUNT {

        //init core and load program
        let mut core: Core<InstructionV2<u64, u64, u64>> = Core::new();
        core.load_program(prog.clone());

        //run program
        let inst_count = core.run()?;
        // write!(stdout, "{}", inst_count)?;
        println!("{}", inst_count);
    }

    Ok(())
}

fn runstats_sr_machine_isa_v3(filepaths: &Vec<PathBuf>) -> Result<(), Error> {

    let mut obj_files: Vec<ObjectFile<InstructionV3<u64, u64, u64>>> = Vec::new();

    //load assembly in string
    for filepath in filepaths.iter() {
        let statements = AssemblyV3::parse_file(&filepath)?;
        obj_files.push(ObjectFile::new(statements));
    }
    

    //compile assembly to machine code
    let prog = ObjectFile::link(&obj_files)?;
    
    for _ in 0..RUNCOUNT {

        //init core and load program
        let mut core: Core<InstructionV3<u64, u64, u64>> = Core::new();
        core.load_program(prog.clone());

        //run program
        let inst_count = core.run()?;
        println!("{}", inst_count);
    }

    Ok(())
}



fn runstats_prog(filepaths: &Vec<PathBuf>, isa: ISA) -> Result<(), Error> {

    match isa {
        ISA::V1 => runstats_sr_machine_isa_v1(filepaths),
        ISA::V2 => runstats_sr_machine_isa_v2(filepaths),
        ISA::V3 => runstats_sr_machine_isa_v3(filepaths) 
    }
}

fn analyze_v1(filepaths: &Vec<PathBuf>) -> Result<(), Error> {
    
    // type DTInstV1 = InstructionV1<u64, DeterminismTracer<u64>, DeterminismTracer<u64>>;
    let mut obj_files: Vec<ObjectFile<InstructionV1<u64, u64, u64>>> = Vec::new();

    //load assembly in string
    for filepath in filepaths.iter() {
        let statements = AssemblyV1::parse_file(&filepath)?;
        obj_files.push(ObjectFile::new(statements));
    }
    
    //compile assembly to machine code
    let prog = ObjectFile::link(&obj_files)?;

    info!("{}", prog);

    let cfg = static_analysis::cfg::structural(prog);
    // let cfg = static_analysis::cfg::determinism_tracing::<DTInstV1, InstructionV1<u64, u64, u64>>(prog)?;

    println!("CFG:");
    println!("\tNode count: {}", cfg.node_count());
    println!("\tEdge count: {}", cfg.edge_count());

    Ok(())
}

fn analyze_v3(filepaths: &Vec<PathBuf>) -> Result<(), Error> {
    
    let mut obj_files: Vec<ObjectFile<InstructionV3<u64, u64, u64>>> = Vec::new();

    //load assembly in string
    for filepath in filepaths.iter() {
        let statements = AssemblyV3::parse_file(&filepath)?;
        obj_files.push(ObjectFile::new(statements));
    }
    
    //compile assembly to machine code
    let prog = ObjectFile::link(&obj_files)?;

    info!("{}", prog);

    let cfg = static_analysis::cfg::callstack_tracking(prog);

    println!("CFG:");
    println!("\tNode count: {}", cfg.node_count());
    println!("\tEdge count: {}", cfg.edge_count());

    Ok(())
}

fn analyze_v2(filepaths: &Vec<PathBuf>) -> Result<(), Error> {
    
    let mut obj_files: Vec<ObjectFile<InstructionV2<u64, u64, u64>>> = Vec::new();

    //load assembly in string
    for filepath in filepaths.iter() {
        let statements = AssemblyV2::parse_file(&filepath)?;
        obj_files.push(ObjectFile::new(statements));
    }
    

    //compile assembly to machine code
    let prog = ObjectFile::link(&obj_files)?;

    info!("{}", prog);

    let cfg = static_analysis::cfg::structural(prog);

    println!("CFG:");
    println!("\tNode count: {}", cfg.node_count());
    println!("\tEdge count: {}", cfg.edge_count());

    Ok(())
}

fn analyze(filepaths: &Vec<PathBuf>, isa: ISA) -> Result<(), Error> {
    match isa {
        ISA::V1 => analyze_v1(filepaths),
        ISA::V2 => analyze_v2(filepaths),
        ISA::V3 => analyze_v3(filepaths)
    }
}